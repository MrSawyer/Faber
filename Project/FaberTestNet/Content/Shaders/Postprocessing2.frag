#version 330 core

out vec4 FragColor;

in vec2 OutUVs;

uniform sampler2D DLFragColor;

void main()
{
	vec3 Color = texture(DLFragColor, OutUVs).rgb;
	
	vec3 Result = vec3(1.0) - exp(-Color * 1.0);
	Result = pow(Result, vec3(1.0 / 2.2));
	
	FragColor = vec4(1.0, 0.0, 0.0, 1.0);
}