#version 330 core

layout (location = 0) out vec3 DLFragColor;

in vec2 OutUVs;

uniform vec3 CameraV;

uniform sampler2D FGPositionT;
uniform sampler2D FGColorT;
uniform sampler2D FGNormalT;
uniform sampler2D FGMaterialT;

void main()
{
	vec3 PositionT = texture(FGPositionT, OutUVs).rgb;	
	vec3 ColorT = texture(FGColorT, OutUVs).rgb;
	vec3 NormalT = texture(FGNormalT, OutUVs).rgb;
	float SpecularF = texture(FGMaterialT, OutUVs).r;

	vec3 LightDirection = normalize(vec3(20.0, 10.0, 20.0) - PositionT);
	vec3 ViewDirection = normalize(CameraV - PositionT);
	vec3 HalfwayDirection = normalize(ViewDirection + LightDirection);
	
	vec3 AmbientLight = 0.1 * ColorT;
	
	float DiffuseForce = max(dot(LightDirection, NormalT), 0.0);
	vec3 DiffuseLight = DiffuseForce * ColorT;
	
	float SpecularForce = pow(max(dot(HalfwayDirection, NormalT), 0.0), 32.0);
	vec3 SpecularLight = SpecularF * SpecularForce * vec3(1.0);
	
	DLFragColor = AmbientLight + DiffuseLight + SpecularLight;
}