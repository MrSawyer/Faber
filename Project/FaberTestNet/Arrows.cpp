#include "Arrows.h"

cArrows::cArrows() {}

cArrows::~cArrows() {}

bool cArrows::initialize(Faber::Graphics::cRenderDevice *Device, Faber::Content::cScene *Scene, Faber::Content::Native::sMeshRawData *Cone, Faber::Content::Native::sMeshRawData *Cylinder)
{
	if (Device == nullptr)
	{
		return false;
	}

	if (Scene == nullptr)
	{
		return false;
	}

	this->Device = Device;
	this->Scene = Scene;

	Faber::Content::Native::cMesh *ArrowXMeshTop = nullptr;
	Faber::Content::Native::cMesh *ArrowXMeshBottom = nullptr;
	Faber::Content::Native::cMesh *ArrowYMeshTop = nullptr;
	Faber::Content::Native::cMesh *ArrowYMeshBottom = nullptr;
	Faber::Content::Native::cMesh *ArrowZMeshTop = nullptr;
	Faber::Content::Native::cMesh *ArrowZMeshBottom = nullptr;

	Faber::Content::sMaterialTexture *RedColor = new Faber::Content::sMaterialTexture();
	RedColor->Color = glm::vec4(1.0f, 0.0f, 0.0f, 1.0f);

	Faber::Content::sMaterialTexture *RedSpecular = new Faber::Content::sMaterialTexture();
	RedSpecular->Color = glm::vec4(0.0f, 0.0f, 0.0f, 0.0f);

	Faber::Content::sMaterialTexture *GreenColor = new Faber::Content::sMaterialTexture();
	GreenColor->Color = glm::vec4(0.0f, 1.0f, 0.0f, 1.0f);

	Faber::Content::sMaterialTexture *GreenSpecular = new Faber::Content::sMaterialTexture();
	GreenSpecular->Color = glm::vec4(0.0f, 0.0f, 0.0f, 0.0f);

	Faber::Content::sMaterialTexture *BlueColor = new Faber::Content::sMaterialTexture();
	BlueColor->Color = glm::vec4(0.0f, 0.0f, 1.0f, 1.0f);

	Faber::Content::sMaterialTexture *BlueSpecular = new Faber::Content::sMaterialTexture();
	BlueSpecular->Color = glm::vec4(0.0f, 0.0f, 0.0f, 0.0f);

	Faber::Content::cMaterial *ArrowXMaterial = nullptr;
	Faber::Content::cMaterial *ArrowYMaterial = nullptr;
	Faber::Content::cMaterial *ArrowZMaterial = nullptr;

	Faber::Content::createMaterial((void**)&ArrowXMaterial);
	ArrowXMaterial->addColor(RedColor);
	ArrowXMaterial->addSpecular(RedSpecular);

	Faber::Content::createMaterial((void**)&ArrowYMaterial);
	ArrowYMaterial->addColor(GreenColor);
	ArrowYMaterial->addSpecular(GreenSpecular);

	Faber::Content::createMaterial((void**)&ArrowZMaterial);
	ArrowZMaterial->addColor(BlueColor);
	ArrowZMaterial->addSpecular(BlueSpecular);

	Faber::Content::createMesh(this->Device, (void**)&ArrowXMeshTop, Cone, ArrowXMaterial);
	Faber::Content::createMesh(this->Device, (void**)&ArrowXMeshBottom, Cylinder, ArrowXMaterial);

	Faber::Content::createMesh(this->Device, (void**)&ArrowYMeshTop, Cone, ArrowYMaterial);
	Faber::Content::createMesh(this->Device, (void**)&ArrowYMeshBottom, Cylinder, ArrowYMaterial);

	Faber::Content::createMesh(this->Device, (void**)&ArrowZMeshTop, Cone, ArrowZMaterial);
	Faber::Content::createMesh(this->Device, (void**)&ArrowZMeshBottom, Cylinder, ArrowZMaterial);

	Faber::Content::createStaticObject((void**)&this->ArrowX);
	ArrowX->addMesh(ArrowXMeshTop);
	ArrowX->addMesh(ArrowXMeshBottom);
	ArrowXMeshTop->setPosition(0.6f, 0.0f, 0.0f);
	ArrowXMeshTop->setRotation(glm::quat(glm::vec3(0.0f, 0.0f, glm::radians(-90.0f))));
	ArrowXMeshTop->setScale(0.2f, 0.5f, 0.2f);
	ArrowXMeshBottom->setPosition(0.1f, 0.0f, 0.0f);
	ArrowXMeshBottom->setRotation(glm::quat(glm::vec3(0.0f, 0.0f, glm::radians(-90.0f))));
	ArrowXMeshBottom->setScale(0.08f, 0.6f, 0.08f);

	Faber::Content::createStaticObject((void**)&this->ArrowY);
	ArrowY->addMesh(ArrowYMeshTop);
	ArrowY->addMesh(ArrowYMeshBottom);
	ArrowYMeshTop->setPosition(0.0f, 0.6f, 0.0f);
	ArrowYMeshTop->setScale(0.2f, 0.5f, 0.2f);
	ArrowYMeshBottom->setPosition(0.0f, 0.1f, 0.0f);
	ArrowYMeshBottom->setScale(0.08f, 0.6f, 0.08f);

	Faber::Content::createStaticObject((void**)&this->ArrowZ);
	ArrowZ->addMesh(ArrowZMeshTop);
	ArrowZ->addMesh(ArrowZMeshBottom);
	ArrowZMeshTop->setPosition(0.0f, 0.0f, 0.6f);
	ArrowZMeshTop->setRotation(glm::quat(glm::vec3(glm::radians(90.0f), 0.0f, 0.0f)));
	ArrowZMeshTop->setScale(0.2f, 0.5f, 0.2f);
	ArrowZMeshBottom->setPosition(0.0f, 0.0f, 0.1f);
	ArrowZMeshBottom->setRotation(glm::quat(glm::vec3(glm::radians(90.0f), 0.0f, 0.0f)));
	ArrowZMeshBottom->setScale(0.08f, 0.6f, 0.08f);

	return true;
}

void cArrows::show()
{
	Scene->addStaticObject(this->ArrowX);
	Scene->addStaticObject(this->ArrowY);
	Scene->addStaticObject(this->ArrowZ);
}

void cArrows::hide()
{
	Scene->removeStaticObject(this->ArrowX);
	Scene->removeStaticObject(this->ArrowY);
	Scene->removeStaticObject(this->ArrowZ);
}

void cArrows::update()
{

}

void cArrows::setPositionScale(glm::vec3 Position, glm::vec3 Scale)
{
	this->ArrowX->setPosition(Position.x + Scale.x, Position.y, Position.z);
	this->ArrowY->setPosition(Position.x, Position.y + Scale.y, Position.z);
	this->ArrowZ->setPosition(Position.x, Position.y, Position.z + Scale.z);
}