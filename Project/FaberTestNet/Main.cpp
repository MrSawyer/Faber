#include <Faber.h>
#include "Main.h"

[STAThreadAttribute]
void Main(array <String^> ^args)
{
	Application::EnableVisualStyles();
	Application::SetCompatibleTextRenderingDefault(false);

	cWindow ^Window = gcnew cWindow();

	Window->initialize();
	Application::Run(Window);
	Window->terminate();
}