#pragma once

#include <Faber.h>
#include <Input/NativeInput.h>

namespace FaberTestNet {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// Podsumowanie informacji o Window
	/// </summary>
	public ref class cWindow : public System::Windows::Forms::Form
	{
	public:
		cWindow(void)
		{
			InitializeComponent();
			//
			//TODO: W tym miejscu dodaj kod konstruktora
			//
		}

	protected:
		/// <summary>
		/// Wyczy�� wszystkie u�ywane zasoby.
		/// </summary>
		~cWindow()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::Timer^  Timer;
	private: System::Windows::Forms::Panel^  Panel;
	private: System::Windows::Forms::GroupBox^  MeshesGroup;
	private: System::Windows::Forms::GroupBox^  StaticObjectsGroup;
	private: System::Windows::Forms::GroupBox^  MaterialsGroup;
	private: System::Windows::Forms::Label^  MeshesLabel;
	private: System::Windows::Forms::ComboBox^  MeshesList;


	private: System::Windows::Forms::Button^  DeleteMeshButton;
	private: System::Windows::Forms::Button^  CreateMeshButton;
	private: System::Windows::Forms::ComboBox^  RawDataListActive;

	private: System::Windows::Forms::ComboBox^  MaterialsListActive;


	private: System::Windows::Forms::Label^  MaterialLabel;
	private: System::Windows::Forms::Label^  RawDataLabel;
	private: System::Windows::Forms::ComboBox^  MaterialsList;

	private: System::Windows::Forms::Label^  MaterialsLabel;
	private: System::Windows::Forms::Button^  CreateMaterialButton;
	private: System::Windows::Forms::Button^  DeleteMaterialButton;
	private: System::Windows::Forms::Label^  BlueLabel;
	private: System::Windows::Forms::Label^  SpecularLabel;







	private: System::Windows::Forms::Label^  ColorLabel;
	private: System::Windows::Forms::TrackBar^  BlueBar;


	private: System::Windows::Forms::TrackBar^  GreenBar;

	private: System::Windows::Forms::TrackBar^  RedBar;

	private: System::Windows::Forms::Label^  GreenLabel;







	private: System::Windows::Forms::Label^  RedLabel;
	private: System::Windows::Forms::TrackBar^  ForceBar;




	private: System::Windows::Forms::Label^  ForceLabel;
	private: System::Windows::Forms::Button^  PropertiesButton;




	private: System::Windows::Forms::Button^  RemoveMeshButton;

	private: System::Windows::Forms::Button^  AddMeshButton;

	private: System::Windows::Forms::Label^  ObjectsMeshesLabel;


	private: System::Windows::Forms::Button^  CreateStaticObjectButton;
	private: System::Windows::Forms::ComboBox^  ObjectsMeshesList;


	private: System::Windows::Forms::Button^  DeleteStaticObjectButton;

	private: System::Windows::Forms::ComboBox^  StaticObjectsList;

	private: System::Windows::Forms::Label^  StaticObjectLabel;
	private: System::Windows::Forms::TextBox^  MaterialName;
	private: System::Windows::Forms::TextBox^  MeshName;
	private: System::Windows::Forms::TextBox^  StaticObjectName;



	protected:

	protected:
	private: System::ComponentModel::IContainer^  components;

	private:
		/// <summary>
		/// Wymagana zmienna projektanta.
		/// </summary>

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Wymagana metoda obs�ugi projektanta � nie nale�y modyfikowa� 
		/// zawarto�� tej metody z edytorem kodu.
		/// </summary>
		void InitializeComponent(void)
		{
			this->components = (gcnew System::ComponentModel::Container());
			this->Timer = (gcnew System::Windows::Forms::Timer(this->components));
			this->Panel = (gcnew System::Windows::Forms::Panel());
			this->MeshesGroup = (gcnew System::Windows::Forms::GroupBox());
			this->RawDataListActive = (gcnew System::Windows::Forms::ComboBox());
			this->MaterialsListActive = (gcnew System::Windows::Forms::ComboBox());
			this->MaterialLabel = (gcnew System::Windows::Forms::Label());
			this->RawDataLabel = (gcnew System::Windows::Forms::Label());
			this->DeleteMeshButton = (gcnew System::Windows::Forms::Button());
			this->CreateMeshButton = (gcnew System::Windows::Forms::Button());
			this->MeshesLabel = (gcnew System::Windows::Forms::Label());
			this->MeshesList = (gcnew System::Windows::Forms::ComboBox());
			this->MeshName = (gcnew System::Windows::Forms::TextBox());
			this->StaticObjectsGroup = (gcnew System::Windows::Forms::GroupBox());
			this->PropertiesButton = (gcnew System::Windows::Forms::Button());
			this->RemoveMeshButton = (gcnew System::Windows::Forms::Button());
			this->AddMeshButton = (gcnew System::Windows::Forms::Button());
			this->ObjectsMeshesLabel = (gcnew System::Windows::Forms::Label());
			this->CreateStaticObjectButton = (gcnew System::Windows::Forms::Button());
			this->ObjectsMeshesList = (gcnew System::Windows::Forms::ComboBox());
			this->DeleteStaticObjectButton = (gcnew System::Windows::Forms::Button());
			this->StaticObjectsList = (gcnew System::Windows::Forms::ComboBox());
			this->StaticObjectLabel = (gcnew System::Windows::Forms::Label());
			this->StaticObjectName = (gcnew System::Windows::Forms::TextBox());
			this->MaterialsGroup = (gcnew System::Windows::Forms::GroupBox());
			this->ForceBar = (gcnew System::Windows::Forms::TrackBar());
			this->ForceLabel = (gcnew System::Windows::Forms::Label());
			this->BlueBar = (gcnew System::Windows::Forms::TrackBar());
			this->GreenBar = (gcnew System::Windows::Forms::TrackBar());
			this->RedBar = (gcnew System::Windows::Forms::TrackBar());
			this->BlueLabel = (gcnew System::Windows::Forms::Label());
			this->GreenLabel = (gcnew System::Windows::Forms::Label());
			this->RedLabel = (gcnew System::Windows::Forms::Label());
			this->SpecularLabel = (gcnew System::Windows::Forms::Label());
			this->ColorLabel = (gcnew System::Windows::Forms::Label());
			this->CreateMaterialButton = (gcnew System::Windows::Forms::Button());
			this->DeleteMaterialButton = (gcnew System::Windows::Forms::Button());
			this->MaterialsLabel = (gcnew System::Windows::Forms::Label());
			this->MaterialsList = (gcnew System::Windows::Forms::ComboBox());
			this->MaterialName = (gcnew System::Windows::Forms::TextBox());
			this->MeshesGroup->SuspendLayout();
			this->StaticObjectsGroup->SuspendLayout();
			this->MaterialsGroup->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->ForceBar))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->BlueBar))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->GreenBar))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->RedBar))->BeginInit();
			this->SuspendLayout();
			// 
			// Timer
			// 
			this->Timer->Enabled = true;
			this->Timer->Interval = 1;
			this->Timer->Tick += gcnew System::EventHandler(this, &cWindow::Timer_Tick);
			// 
			// Panel
			// 
			this->Panel->BackColor = System::Drawing::Color::Black;
			this->Panel->ForeColor = System::Drawing::Color::White;
			this->Panel->Location = System::Drawing::Point(0, 0);
			this->Panel->Name = L"Panel";
			this->Panel->Size = System::Drawing::Size(900, 600);
			this->Panel->TabIndex = 0;
			this->Panel->MouseDown += gcnew System::Windows::Forms::MouseEventHandler(this, &cWindow::Panel_MouseDown);
			this->Panel->MouseUp += gcnew System::Windows::Forms::MouseEventHandler(this, &cWindow::Panel_MouseUp);
			// 
			// MeshesGroup
			// 
			this->MeshesGroup->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Right));
			this->MeshesGroup->Controls->Add(this->RawDataListActive);
			this->MeshesGroup->Controls->Add(this->MaterialsListActive);
			this->MeshesGroup->Controls->Add(this->MaterialLabel);
			this->MeshesGroup->Controls->Add(this->RawDataLabel);
			this->MeshesGroup->Controls->Add(this->DeleteMeshButton);
			this->MeshesGroup->Controls->Add(this->CreateMeshButton);
			this->MeshesGroup->Controls->Add(this->MeshesLabel);
			this->MeshesGroup->Controls->Add(this->MeshesList);
			this->MeshesGroup->Controls->Add(this->MeshName);
			this->MeshesGroup->Location = System::Drawing::Point(906, 275);
			this->MeshesGroup->Name = L"MeshesGroup";
			this->MeshesGroup->Size = System::Drawing::Size(282, 141);
			this->MeshesGroup->TabIndex = 1;
			this->MeshesGroup->TabStop = false;
			this->MeshesGroup->Text = L"Meshes";
			// 
			// RawDataListActive
			// 
			this->RawDataListActive->DropDownStyle = System::Windows::Forms::ComboBoxStyle::DropDownList;
			this->RawDataListActive->Enabled = false;
			this->RawDataListActive->Items->AddRange(gcnew cli::array< System::Object^  >(7) {
				L"Triangle", L"Rectangle", L"Circle", L"Cone",
					L"Cube", L"Cylinder", L"Sphere"
			});
			this->RawDataListActive->Location = System::Drawing::Point(78, 100);
			this->RawDataListActive->Name = L"RawDataListActive";
			this->RawDataListActive->Size = System::Drawing::Size(198, 21);
			this->RawDataListActive->TabIndex = 7;
			// 
			// MaterialsListActive
			// 
			this->MaterialsListActive->DropDownStyle = System::Windows::Forms::ComboBoxStyle::DropDownList;
			this->MaterialsListActive->Enabled = false;
			this->MaterialsListActive->FormattingEnabled = true;
			this->MaterialsListActive->Location = System::Drawing::Point(78, 73);
			this->MaterialsListActive->Name = L"MaterialsListActive";
			this->MaterialsListActive->Size = System::Drawing::Size(198, 21);
			this->MaterialsListActive->TabIndex = 6;
			this->MaterialsListActive->SelectedIndexChanged += gcnew System::EventHandler(this, &cWindow::MaterialsListActive_SelectedIndexChanged);
			// 
			// MaterialLabel
			// 
			this->MaterialLabel->AutoSize = true;
			this->MaterialLabel->Location = System::Drawing::Point(6, 76);
			this->MaterialLabel->Name = L"MaterialLabel";
			this->MaterialLabel->Size = System::Drawing::Size(47, 13);
			this->MaterialLabel->TabIndex = 5;
			this->MaterialLabel->Text = L"Material:";
			// 
			// RawDataLabel
			// 
			this->RawDataLabel->AutoSize = true;
			this->RawDataLabel->Location = System::Drawing::Point(6, 103);
			this->RawDataLabel->Name = L"RawDataLabel";
			this->RawDataLabel->Size = System::Drawing::Size(58, 13);
			this->RawDataLabel->TabIndex = 4;
			this->RawDataLabel->Text = L"Raw Data:";
			// 
			// DeleteMeshButton
			// 
			this->DeleteMeshButton->Location = System::Drawing::Point(146, 44);
			this->DeleteMeshButton->Name = L"DeleteMeshButton";
			this->DeleteMeshButton->Size = System::Drawing::Size(130, 23);
			this->DeleteMeshButton->TabIndex = 3;
			this->DeleteMeshButton->Text = L"Delete Mesh";
			this->DeleteMeshButton->UseVisualStyleBackColor = true;
			this->DeleteMeshButton->Click += gcnew System::EventHandler(this, &cWindow::DeleteMeshButton_Click);
			// 
			// CreateMeshButton
			// 
			this->CreateMeshButton->Location = System::Drawing::Point(10, 44);
			this->CreateMeshButton->Name = L"CreateMeshButton";
			this->CreateMeshButton->Size = System::Drawing::Size(130, 23);
			this->CreateMeshButton->TabIndex = 2;
			this->CreateMeshButton->Text = L"Create Mesh";
			this->CreateMeshButton->UseVisualStyleBackColor = true;
			this->CreateMeshButton->Click += gcnew System::EventHandler(this, &cWindow::CreateMeshButton_Click);
			// 
			// MeshesLabel
			// 
			this->MeshesLabel->AutoSize = true;
			this->MeshesLabel->Location = System::Drawing::Point(6, 20);
			this->MeshesLabel->Name = L"MeshesLabel";
			this->MeshesLabel->Size = System::Drawing::Size(66, 13);
			this->MeshesLabel->TabIndex = 1;
			this->MeshesLabel->Text = L"Meshes List:";
			// 
			// MeshesList
			// 
			this->MeshesList->DropDownStyle = System::Windows::Forms::ComboBoxStyle::DropDownList;
			this->MeshesList->FormattingEnabled = true;
			this->MeshesList->Location = System::Drawing::Point(78, 17);
			this->MeshesList->Name = L"MeshesList";
			this->MeshesList->Size = System::Drawing::Size(198, 21);
			this->MeshesList->TabIndex = 0;
			this->MeshesList->SelectedIndexChanged += gcnew System::EventHandler(this, &cWindow::MeshesList_SelectedIndexChanged);
			// 
			// MeshName
			// 
			this->MeshName->Location = System::Drawing::Point(78, 18);
			this->MeshName->Name = L"MeshName";
			this->MeshName->Size = System::Drawing::Size(198, 20);
			this->MeshName->TabIndex = 26;
			this->MeshName->Visible = false;
			// 
			// StaticObjectsGroup
			// 
			this->StaticObjectsGroup->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Right));
			this->StaticObjectsGroup->Controls->Add(this->PropertiesButton);
			this->StaticObjectsGroup->Controls->Add(this->RemoveMeshButton);
			this->StaticObjectsGroup->Controls->Add(this->AddMeshButton);
			this->StaticObjectsGroup->Controls->Add(this->ObjectsMeshesLabel);
			this->StaticObjectsGroup->Controls->Add(this->CreateStaticObjectButton);
			this->StaticObjectsGroup->Controls->Add(this->ObjectsMeshesList);
			this->StaticObjectsGroup->Controls->Add(this->DeleteStaticObjectButton);
			this->StaticObjectsGroup->Controls->Add(this->StaticObjectsList);
			this->StaticObjectsGroup->Controls->Add(this->StaticObjectLabel);
			this->StaticObjectsGroup->Controls->Add(this->StaticObjectName);
			this->StaticObjectsGroup->Location = System::Drawing::Point(906, 422);
			this->StaticObjectsGroup->Name = L"StaticObjectsGroup";
			this->StaticObjectsGroup->Size = System::Drawing::Size(282, 166);
			this->StaticObjectsGroup->TabIndex = 2;
			this->StaticObjectsGroup->TabStop = false;
			this->StaticObjectsGroup->Text = L"Static Objects";
			// 
			// PropertiesButton
			// 
			this->PropertiesButton->Enabled = false;
			this->PropertiesButton->Location = System::Drawing::Point(10, 131);
			this->PropertiesButton->Name = L"PropertiesButton";
			this->PropertiesButton->Size = System::Drawing::Size(266, 23);
			this->PropertiesButton->TabIndex = 10;
			this->PropertiesButton->Text = L"Properties...";
			this->PropertiesButton->UseVisualStyleBackColor = true;
			// 
			// RemoveMeshButton
			// 
			this->RemoveMeshButton->Enabled = false;
			this->RemoveMeshButton->Location = System::Drawing::Point(146, 102);
			this->RemoveMeshButton->Name = L"RemoveMeshButton";
			this->RemoveMeshButton->Size = System::Drawing::Size(130, 23);
			this->RemoveMeshButton->TabIndex = 9;
			this->RemoveMeshButton->Text = L"Remove Mesh";
			this->RemoveMeshButton->UseVisualStyleBackColor = true;
			this->RemoveMeshButton->Click += gcnew System::EventHandler(this, &cWindow::RemoveMeshButton_Click);
			// 
			// AddMeshButton
			// 
			this->AddMeshButton->Enabled = false;
			this->AddMeshButton->Location = System::Drawing::Point(10, 102);
			this->AddMeshButton->Name = L"AddMeshButton";
			this->AddMeshButton->Size = System::Drawing::Size(130, 23);
			this->AddMeshButton->TabIndex = 8;
			this->AddMeshButton->Text = L"Add Mesh";
			this->AddMeshButton->UseVisualStyleBackColor = true;
			this->AddMeshButton->Click += gcnew System::EventHandler(this, &cWindow::AddMeshButton_Click);
			// 
			// ObjectsMeshesLabel
			// 
			this->ObjectsMeshesLabel->AutoSize = true;
			this->ObjectsMeshesLabel->Location = System::Drawing::Point(6, 78);
			this->ObjectsMeshesLabel->Name = L"ObjectsMeshesLabel";
			this->ObjectsMeshesLabel->Size = System::Drawing::Size(66, 13);
			this->ObjectsMeshesLabel->TabIndex = 9;
			this->ObjectsMeshesLabel->Text = L"Meshes List:";
			// 
			// CreateStaticObjectButton
			// 
			this->CreateStaticObjectButton->Location = System::Drawing::Point(9, 46);
			this->CreateStaticObjectButton->Name = L"CreateStaticObjectButton";
			this->CreateStaticObjectButton->Size = System::Drawing::Size(130, 23);
			this->CreateStaticObjectButton->TabIndex = 8;
			this->CreateStaticObjectButton->Text = L"Create Static Object";
			this->CreateStaticObjectButton->UseVisualStyleBackColor = true;
			this->CreateStaticObjectButton->Click += gcnew System::EventHandler(this, &cWindow::CreateStaticObjectButton_Click);
			// 
			// ObjectsMeshesList
			// 
			this->ObjectsMeshesList->DropDownStyle = System::Windows::Forms::ComboBoxStyle::DropDownList;
			this->ObjectsMeshesList->Enabled = false;
			this->ObjectsMeshesList->FormattingEnabled = true;
			this->ObjectsMeshesList->Location = System::Drawing::Point(78, 75);
			this->ObjectsMeshesList->Name = L"ObjectsMeshesList";
			this->ObjectsMeshesList->Size = System::Drawing::Size(198, 21);
			this->ObjectsMeshesList->TabIndex = 8;
			this->ObjectsMeshesList->SelectedIndexChanged += gcnew System::EventHandler(this, &cWindow::ObjectsMeshesList_SelectedIndexChanged);
			// 
			// DeleteStaticObjectButton
			// 
			this->DeleteStaticObjectButton->Location = System::Drawing::Point(146, 46);
			this->DeleteStaticObjectButton->Name = L"DeleteStaticObjectButton";
			this->DeleteStaticObjectButton->Size = System::Drawing::Size(130, 23);
			this->DeleteStaticObjectButton->TabIndex = 8;
			this->DeleteStaticObjectButton->Text = L"Delete Static Object";
			this->DeleteStaticObjectButton->UseVisualStyleBackColor = true;
			this->DeleteStaticObjectButton->Click += gcnew System::EventHandler(this, &cWindow::DeleteStaticObjectButton_Click);
			// 
			// StaticObjectsList
			// 
			this->StaticObjectsList->DropDownStyle = System::Windows::Forms::ComboBoxStyle::DropDownList;
			this->StaticObjectsList->FormattingEnabled = true;
			this->StaticObjectsList->Location = System::Drawing::Point(107, 19);
			this->StaticObjectsList->Name = L"StaticObjectsList";
			this->StaticObjectsList->Size = System::Drawing::Size(169, 21);
			this->StaticObjectsList->TabIndex = 8;
			this->StaticObjectsList->SelectedIndexChanged += gcnew System::EventHandler(this, &cWindow::StaticObjectsList_SelectedIndexChanged);
			// 
			// StaticObjectLabel
			// 
			this->StaticObjectLabel->AutoSize = true;
			this->StaticObjectLabel->Location = System::Drawing::Point(6, 22);
			this->StaticObjectLabel->Name = L"StaticObjectLabel";
			this->StaticObjectLabel->Size = System::Drawing::Size(95, 13);
			this->StaticObjectLabel->TabIndex = 0;
			this->StaticObjectLabel->Text = L"Static Objects List:";
			// 
			// StaticObjectName
			// 
			this->StaticObjectName->Location = System::Drawing::Point(107, 20);
			this->StaticObjectName->Name = L"StaticObjectName";
			this->StaticObjectName->Size = System::Drawing::Size(169, 20);
			this->StaticObjectName->TabIndex = 27;
			this->StaticObjectName->Visible = false;
			// 
			// MaterialsGroup
			// 
			this->MaterialsGroup->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Right));
			this->MaterialsGroup->Controls->Add(this->ForceBar);
			this->MaterialsGroup->Controls->Add(this->ForceLabel);
			this->MaterialsGroup->Controls->Add(this->BlueBar);
			this->MaterialsGroup->Controls->Add(this->GreenBar);
			this->MaterialsGroup->Controls->Add(this->RedBar);
			this->MaterialsGroup->Controls->Add(this->BlueLabel);
			this->MaterialsGroup->Controls->Add(this->GreenLabel);
			this->MaterialsGroup->Controls->Add(this->RedLabel);
			this->MaterialsGroup->Controls->Add(this->SpecularLabel);
			this->MaterialsGroup->Controls->Add(this->ColorLabel);
			this->MaterialsGroup->Controls->Add(this->CreateMaterialButton);
			this->MaterialsGroup->Controls->Add(this->DeleteMaterialButton);
			this->MaterialsGroup->Controls->Add(this->MaterialsLabel);
			this->MaterialsGroup->Controls->Add(this->MaterialsList);
			this->MaterialsGroup->Controls->Add(this->MaterialName);
			this->MaterialsGroup->Location = System::Drawing::Point(906, 12);
			this->MaterialsGroup->Name = L"MaterialsGroup";
			this->MaterialsGroup->Size = System::Drawing::Size(282, 257);
			this->MaterialsGroup->TabIndex = 3;
			this->MaterialsGroup->TabStop = false;
			this->MaterialsGroup->Text = L"Materials";
			// 
			// ForceBar
			// 
			this->ForceBar->Enabled = false;
			this->ForceBar->Location = System::Drawing::Point(59, 208);
			this->ForceBar->Maximum = 100;
			this->ForceBar->Name = L"ForceBar";
			this->ForceBar->Size = System::Drawing::Size(215, 45);
			this->ForceBar->TabIndex = 24;
			this->ForceBar->TickStyle = System::Windows::Forms::TickStyle::None;
			this->ForceBar->Scroll += gcnew System::EventHandler(this, &cWindow::ForceBar_Scroll);
			// 
			// ForceLabel
			// 
			this->ForceLabel->AutoSize = true;
			this->ForceLabel->Location = System::Drawing::Point(22, 208);
			this->ForceLabel->Name = L"ForceLabel";
			this->ForceLabel->Size = System::Drawing::Size(37, 13);
			this->ForceLabel->TabIndex = 21;
			this->ForceLabel->Text = L"Force:";
			// 
			// BlueBar
			// 
			this->BlueBar->Enabled = false;
			this->BlueBar->Location = System::Drawing::Point(59, 158);
			this->BlueBar->Maximum = 255;
			this->BlueBar->Name = L"BlueBar";
			this->BlueBar->Size = System::Drawing::Size(215, 45);
			this->BlueBar->TabIndex = 20;
			this->BlueBar->TickStyle = System::Windows::Forms::TickStyle::None;
			this->BlueBar->Scroll += gcnew System::EventHandler(this, &cWindow::BlueBar_Scroll);
			// 
			// GreenBar
			// 
			this->GreenBar->Enabled = false;
			this->GreenBar->Location = System::Drawing::Point(59, 129);
			this->GreenBar->Maximum = 255;
			this->GreenBar->Name = L"GreenBar";
			this->GreenBar->Size = System::Drawing::Size(215, 45);
			this->GreenBar->TabIndex = 19;
			this->GreenBar->TickStyle = System::Windows::Forms::TickStyle::None;
			this->GreenBar->Scroll += gcnew System::EventHandler(this, &cWindow::GreenBar_Scroll);
			// 
			// RedBar
			// 
			this->RedBar->Enabled = false;
			this->RedBar->Location = System::Drawing::Point(59, 100);
			this->RedBar->Maximum = 255;
			this->RedBar->Name = L"RedBar";
			this->RedBar->Size = System::Drawing::Size(215, 45);
			this->RedBar->TabIndex = 18;
			this->RedBar->TickStyle = System::Windows::Forms::TickStyle::None;
			this->RedBar->Scroll += gcnew System::EventHandler(this, &cWindow::RedBar_Scroll);
			// 
			// BlueLabel
			// 
			this->BlueLabel->AutoSize = true;
			this->BlueLabel->Location = System::Drawing::Point(22, 161);
			this->BlueLabel->Name = L"BlueLabel";
			this->BlueLabel->Size = System::Drawing::Size(31, 13);
			this->BlueLabel->TabIndex = 9;
			this->BlueLabel->Text = L"Blue:";
			// 
			// GreenLabel
			// 
			this->GreenLabel->AutoSize = true;
			this->GreenLabel->Location = System::Drawing::Point(22, 129);
			this->GreenLabel->Name = L"GreenLabel";
			this->GreenLabel->Size = System::Drawing::Size(39, 13);
			this->GreenLabel->TabIndex = 8;
			this->GreenLabel->Text = L"Green:";
			// 
			// RedLabel
			// 
			this->RedLabel->AutoSize = true;
			this->RedLabel->Location = System::Drawing::Point(22, 100);
			this->RedLabel->Name = L"RedLabel";
			this->RedLabel->Size = System::Drawing::Size(30, 13);
			this->RedLabel->TabIndex = 7;
			this->RedLabel->Text = L"Red:";
			// 
			// SpecularLabel
			// 
			this->SpecularLabel->AutoSize = true;
			this->SpecularLabel->Location = System::Drawing::Point(6, 190);
			this->SpecularLabel->Name = L"SpecularLabel";
			this->SpecularLabel->Size = System::Drawing::Size(52, 13);
			this->SpecularLabel->TabIndex = 5;
			this->SpecularLabel->Text = L"Specular:";
			// 
			// ColorLabel
			// 
			this->ColorLabel->AutoSize = true;
			this->ColorLabel->Location = System::Drawing::Point(7, 82);
			this->ColorLabel->Name = L"ColorLabel";
			this->ColorLabel->Size = System::Drawing::Size(34, 13);
			this->ColorLabel->TabIndex = 4;
			this->ColorLabel->Text = L"Color:";
			// 
			// CreateMaterialButton
			// 
			this->CreateMaterialButton->Location = System::Drawing::Point(10, 46);
			this->CreateMaterialButton->Name = L"CreateMaterialButton";
			this->CreateMaterialButton->Size = System::Drawing::Size(130, 23);
			this->CreateMaterialButton->TabIndex = 3;
			this->CreateMaterialButton->Text = L"Create Material";
			this->CreateMaterialButton->UseVisualStyleBackColor = true;
			this->CreateMaterialButton->Click += gcnew System::EventHandler(this, &cWindow::CreateMaterialButton_Click);
			// 
			// DeleteMaterialButton
			// 
			this->DeleteMaterialButton->Location = System::Drawing::Point(146, 46);
			this->DeleteMaterialButton->Name = L"DeleteMaterialButton";
			this->DeleteMaterialButton->Size = System::Drawing::Size(130, 23);
			this->DeleteMaterialButton->TabIndex = 2;
			this->DeleteMaterialButton->Text = L"Delete Material";
			this->DeleteMaterialButton->UseVisualStyleBackColor = true;
			this->DeleteMaterialButton->Click += gcnew System::EventHandler(this, &cWindow::DeleteMaterialButton_Click);
			// 
			// MaterialsLabel
			// 
			this->MaterialsLabel->AutoSize = true;
			this->MaterialsLabel->Location = System::Drawing::Point(6, 22);
			this->MaterialsLabel->Name = L"MaterialsLabel";
			this->MaterialsLabel->Size = System::Drawing::Size(71, 13);
			this->MaterialsLabel->TabIndex = 0;
			this->MaterialsLabel->Text = L"Materials List:";
			// 
			// MaterialsList
			// 
			this->MaterialsList->DropDownStyle = System::Windows::Forms::ComboBoxStyle::DropDownList;
			this->MaterialsList->FormattingEnabled = true;
			this->MaterialsList->Location = System::Drawing::Point(83, 19);
			this->MaterialsList->Name = L"MaterialsList";
			this->MaterialsList->Size = System::Drawing::Size(193, 21);
			this->MaterialsList->TabIndex = 1;
			this->MaterialsList->SelectedIndexChanged += gcnew System::EventHandler(this, &cWindow::MaterialsList_SelectedIndexChanged);
			// 
			// MaterialName
			// 
			this->MaterialName->Location = System::Drawing::Point(83, 20);
			this->MaterialName->Name = L"MaterialName";
			this->MaterialName->Size = System::Drawing::Size(193, 20);
			this->MaterialName->TabIndex = 25;
			this->MaterialName->Visible = false;
			// 
			// cWindow
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->BackColor = System::Drawing::SystemColors::Control;
			this->ClientSize = System::Drawing::Size(1200, 600);
			this->Controls->Add(this->MaterialsGroup);
			this->Controls->Add(this->StaticObjectsGroup);
			this->Controls->Add(this->MeshesGroup);
			this->Controls->Add(this->Panel);
			this->ForeColor = System::Drawing::SystemColors::ControlText;
			this->FormBorderStyle = System::Windows::Forms::FormBorderStyle::FixedSingle;
			this->Name = L"cWindow";
			this->Text = L"Scene";
			this->Resize += gcnew System::EventHandler(this, &cWindow::cWindow_Resize);
			this->MeshesGroup->ResumeLayout(false);
			this->MeshesGroup->PerformLayout();
			this->StaticObjectsGroup->ResumeLayout(false);
			this->StaticObjectsGroup->PerformLayout();
			this->MaterialsGroup->ResumeLayout(false);
			this->MaterialsGroup->PerformLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->ForceBar))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->BlueBar))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->GreenBar))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->RedBar))->EndInit();
			this->ResumeLayout(false);

		}
#pragma endregion
	private: System::Void Timer_Tick(System::Object^  sender, System::EventArgs^  e);

	public:
		void initialize();
		void terminate();

	private: System::Void cWindow_Resize(System::Object^  sender, System::EventArgs^  e);

private: System::Void CreateMaterialButton_Click(System::Object^  sender, System::EventArgs^  e);
private: System::Void DeleteMaterialButton_Click(System::Object^  sender, System::EventArgs^  e);
private: System::Void MaterialsList_SelectedIndexChanged(System::Object^  sender, System::EventArgs^  e);
private: System::Void RedBar_Scroll(System::Object^  sender, System::EventArgs^  e);
private: System::Void GreenBar_Scroll(System::Object^  sender, System::EventArgs^  e);
private: System::Void BlueBar_Scroll(System::Object^  sender, System::EventArgs^  e);
private: System::Void ForceBar_Scroll(System::Object^  sender, System::EventArgs^  e);

private: System::Void CreateMeshButton_Click(System::Object^  sender, System::EventArgs^  e);
private: System::Void DeleteMeshButton_Click(System::Object^  sender, System::EventArgs^  e);
private: System::Void MeshesList_SelectedIndexChanged(System::Object^  sender, System::EventArgs^  e);
private: System::Void MaterialsListActive_SelectedIndexChanged(System::Object^  sender, System::EventArgs^  e);

private: System::Void CreateStaticObjectButton_Click(System::Object^  sender, System::EventArgs^  e);
private: System::Void DeleteStaticObjectButton_Click(System::Object^  sender, System::EventArgs^  e);
private: System::Void StaticObjectsList_SelectedIndexChanged(System::Object^  sender, System::EventArgs^  e);
private: System::Void AddMeshButton_Click(System::Object^  sender, System::EventArgs^  e);
private: System::Void RemoveMeshButton_Click(System::Object^  sender, System::EventArgs^  e);
private: System::Void ObjectsMeshesList_SelectedIndexChanged(System::Object^  sender, System::EventArgs^  e);

private: System::Void Panel_MouseDown(System::Object^  sender, System::Windows::Forms::MouseEventArgs^  e);
private: System::Void Panel_MouseUp(System::Object^  sender, System::Windows::Forms::MouseEventArgs^  e);
};
}
