#pragma once

#include <Faber.h>

class cArrows
{
private:
	Faber::Graphics::cRenderDevice	*Device = nullptr;
	Faber::Content::cScene			*Scene = nullptr;

	Faber::Content::cStaticObject *ArrowX = nullptr;
	Faber::Content::cStaticObject *ArrowY = nullptr;
	Faber::Content::cStaticObject *ArrowZ = nullptr;

public:
	cArrows();
	~cArrows();

	bool initialize(Faber::Graphics::cRenderDevice *Device, Faber::Content::cScene *Scene, Faber::Content::Native::sMeshRawData *Cone, Faber::Content::Native::sMeshRawData *Cylinder);

	void show();
	void hide();

	void update();

	void setPositionScale(glm::vec3 Position, glm::vec3 Scale);
};