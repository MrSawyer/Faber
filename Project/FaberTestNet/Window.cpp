#include "Window.h"
#include "Arrows.h"

using namespace FaberTestNet;

// ============= INPUT =============
bool MouseInput[2];
bool KeyboardInput[256];

// ============= MAIN WINDOW CONTENT =============
Faber::Graphics::cRenderDevice *MainDev = nullptr;

Faber::Content::cViewport *MainViewport = nullptr;
Faber::Content::cCamera	*MainCamera = nullptr;
Faber::Content::cScene *MainScene = nullptr;

Faber::Content::cStaticObject *MainGrid = nullptr;

std::vector <Faber::Content::cStaticObject*>	Objects;
std::vector <Faber::Content::Native::cMesh*>	MainMeshes;
std::vector <Faber::Content::cMaterial*>		MainMaterials;

cArrows *MainArrows = nullptr;

// ============= RAW PRIMITIVES DATA =============
Faber::Content::Native::sMeshRawData *TriangleData = nullptr;
Faber::Content::Native::sMeshRawData *RectangleData = nullptr;
Faber::Content::Native::sMeshRawData *CircleData = nullptr;
Faber::Content::Native::sMeshRawData *ConeData = nullptr;
Faber::Content::Native::sMeshRawData *CubeData = nullptr;
Faber::Content::Native::sMeshRawData *CylinderData = nullptr;
Faber::Content::Native::sMeshRawData *SphereData = nullptr;

// ============= GRID DATA =============
Faber::Content::Native::sMeshRawData *GridData = nullptr;

System::Void cWindow::Timer_Tick(System::Object^  sender, System::EventArgs^  e)
{


	Faber::Graphics::render();
}

System::Void cWindow::cWindow_Resize(System::Object^  sender, System::EventArgs^  e)
{
	this->Panel->Width = this->Width - 316;
	this->Panel->Height = this->Height - 39;

	MainDev->setWindowClientSize((float)this->Panel->Width, (float)this->Panel->Height);
}

void cWindow::initialize()
{
	// ===== PREPARING INPUT =====
	for (unsigned int i = 0; i < 2; i++)
	{
		MouseInput[i] = false;
	}
	for (unsigned int i = 0; i < 256; i++)
	{
		KeyboardInput[i] = false;
	}

	// ===== PREPARING ENGINE =====
	Faber::Graphics::sContextDescription RDContextDesc;
	RDContextDesc.TargetWindow = (HWND)(this->Panel->Handle.ToPointer());
	RDContextDesc.ResolutionX = this->Panel->Width;
	RDContextDesc.ResolutionY = this->Panel->Height;
	RDContextDesc.ColorBits = 24;
	RDContextDesc.AlphaBits = 8;
	RDContextDesc.DepthBits = 24;
	RDContextDesc.StencilBits = 8;

	Faber::Graphics::createRenderDevice(RDContextDesc, &MainDev);

	Faber::Content::createCamera((void**)&MainCamera);
	MainCamera->setPosition(4.0f, 2.0f, 4.0f);
	MainCamera->setFront(-glm::normalize(MainCamera->getPosition()));
	MainCamera->setUp(0.0f, 1.0f, 0.0f);

	Faber::Content::createScene((void**)&MainScene);

	Faber::Content::createViewport((void**)&MainViewport);
	MainViewport->setActiveCamera(MainCamera);
	MainViewport->setViewedScene(MainScene);

	MainDev->addViewport(MainViewport);
	MainDev->setWindowClientSize((float)this->Panel->Width, (float)this->Panel->Height);

	// ===== GENERATING GRID DATA =====
	Faber::Content::createMeshRawData((void**)&GridData);
	GridData->Name = "Grid";
	for (unsigned int i = 0; i < 40; i++)
	{
		GridData->Vertices.push_back(glm::vec3(-20.0f, 0.0f, -20.0f + (float)i));
		GridData->Normals.push_back(glm::vec3(0.0f, 1.0f, 0.0f));
		GridData->UVs.push_back(glm::vec2(0.0f, 0.0f));
	}
	for (unsigned int i = 0; i < 40; i++)
	{
		GridData->Vertices.push_back(glm::vec3(20.0f, 0.0f, -20.0f + (float)i));
		GridData->Normals.push_back(glm::vec3(0.0f, 1.0f, 0.0f));
		GridData->UVs.push_back(glm::vec2(0.0f, 0.0f));
	}
	for (unsigned int i = 0; i < 40; i++)
	{
		GridData->Indices.push_back(i);
		GridData->Indices.push_back(i + 40);
	}
	for (unsigned int i = 0; i < 40; i++)
	{
		GridData->Vertices.push_back(glm::vec3(-20.0f + (float)i, 0.0f, -20.0f));
		GridData->Normals.push_back(glm::vec3(0.0f, 1.0f, 0.0f));
		GridData->UVs.push_back(glm::vec2(0.0f, 0.0f));
	}
	for (unsigned int i = 0; i < 40; i++)
	{
		GridData->Vertices.push_back(glm::vec3(-20.0f + (float)i, 0.0f, 20.0f));
		GridData->Normals.push_back(glm::vec3(0.0f, 1.0f, 0.0f));
		GridData->UVs.push_back(glm::vec2(0.0f, 0.0f));
	}
	for (unsigned int i = 80; i < 120; i++)
	{
		GridData->Indices.push_back(i);
		GridData->Indices.push_back(i + 40);
	}

	// ===== CREATING MAIN GRID =====
	Faber::Content::sMaterialTexture *ColorMat = new Faber::Content::sMaterialTexture();
	ColorMat->Color = glm::vec4(0.6f, 0.6f, 0.6f, 1.0f);

	Faber::Content::sMaterialTexture *SpecularMat = new Faber::Content::sMaterialTexture();
	SpecularMat->Color = glm::vec4(0.0f, 0.0f, 0.0f, 0.0f);

	Faber::Content::cMaterial *GridMaterial = nullptr;
	Faber::Content::createMaterial((void**)&GridMaterial);
	GridMaterial->addColor(ColorMat);
	GridMaterial->addSpecular(SpecularMat);

	Faber::Content::Native::cMesh *MainGridMesh = nullptr;
	Faber::Content::createMesh(MainDev, (void**)&MainGridMesh, GridData, GridMaterial);
	MainGridMesh->setRenderMode(Faber::Graphics::RENDER_MODE_LINES);

	Faber::Content::createStaticObject((void**)&MainGrid);
	MainGrid->addMesh(MainGridMesh);

	MainScene->addStaticObject(MainGrid);

	// ===== LOADING PRIMITIVES DATA =====
	Faber::Content::createMeshRawData((void**)&TriangleData, Faber::Content::Predefined::PREDEFINED_PRIMITIVE_TRIANGLE);
	Faber::Content::createMeshRawData((void**)&RectangleData, Faber::Content::Predefined::PREDEFINED_PRIMITIVE_RECTANGLE);
	Faber::Content::createMeshRawData((void**)&CircleData, Faber::Content::Predefined::PREDEFINED_PRIMITIVE_CIRCLE);
	Faber::Content::createMeshRawData((void**)&ConeData, Faber::Content::Predefined::PREDEFINED_PRIMITIVE_CONE);
	Faber::Content::createMeshRawData((void**)&CubeData, Faber::Content::Predefined::PREDEFINED_PRIMITIVE_CUBE);
	Faber::Content::createMeshRawData((void**)&CylinderData, Faber::Content::Predefined::PREDEFINED_PRIMITIVE_CYLINDER);
	Faber::Content::createMeshRawData((void**)&SphereData, Faber::Content::Predefined::PREDEFINED_PRIMITIVE_SPHERE);

	// ===== CREATING MAIN ARROWS =====
	MainArrows = new cArrows();
	MainArrows->initialize(MainDev, MainScene, ConeData, CylinderData);
}

void cWindow::terminate()
{
	if (MainArrows != nullptr)
	{
		delete MainArrows;
		MainArrows = nullptr;
	}

	Faber::Content::terminate();
	Faber::Graphics::terminate();
}

bool MaterialCreatingMode = false;

System::Void cWindow::CreateMaterialButton_Click(System::Object ^sender, System::EventArgs ^e)
{
	if (MaterialCreatingMode)
	{
		Faber::Content::sMaterialTexture *ColorMat = new Faber::Content::sMaterialTexture();
		Faber::Content::sMaterialTexture *SpecularMat = new Faber::Content::sMaterialTexture();

		Faber::Content::cMaterial *Material = nullptr;
		Faber::Content::createMaterial((void**)&Material);
		Material->addColor(ColorMat);
		Material->addSpecular(SpecularMat);

		MainMaterials.push_back(Material);

		if (this->MaterialsList->Items->Count <= 0)
		{
			this->RedBar->Enabled = true;
			this->GreenBar->Enabled = true;
			this->BlueBar->Enabled = true;
			this->ForceBar->Enabled = true;
		}

		this->MaterialsList->Items->Add(this->MaterialName->Text);
		this->MaterialsList->SelectedIndex = this->MaterialsList->Items->Count - 1;
		this->MaterialName->Text = "";

		this->MaterialsListActive->Items->Add(this->MaterialsList->SelectedItem);

		this->CreateMaterialButton->Text = "Create Material";
		this->DeleteMaterialButton->Text = "Delete Material";
		this->MaterialsLabel->Text = "Materials List:";
		this->MaterialsList->Visible = true;
		this->MaterialName->Visible = false;
		MaterialCreatingMode = false;
	}
	else
	{
		this->CreateMaterialButton->Text = "Accept";
		this->DeleteMaterialButton->Text = "Cancel";
		this->MaterialsLabel->Text = "New Name:";
		this->MaterialName->Visible = true;
		this->MaterialName->Focus();
		this->MaterialsList->Visible = false;
		MaterialCreatingMode = true;
	}
}

System::Void cWindow::DeleteMaterialButton_Click(System::Object ^sender, System::EventArgs ^e)
{
	if (MaterialCreatingMode)
	{
		this->CreateMaterialButton->Text = "Create Material";
		this->DeleteMaterialButton->Text = "Delete Material";
		this->MaterialsLabel->Text = "Materials List:";
		this->MaterialsList->Visible = true;
		this->MaterialName->Visible = false;
		MaterialCreatingMode = false;
	}
	else
	{
		if (this->MaterialsList->Items->Count > 0)
		{
			Faber::Content::destroyMaterial((void**)&MainMaterials[(unsigned int)this->MaterialsList->SelectedIndex]);
			MainMaterials.erase(MainMaterials.begin() + this->MaterialsList->SelectedIndex);
		}

		this->MaterialsListActive->Items->Remove(this->MaterialsList->SelectedItem);

		this->MaterialsList->Items->Remove(this->MaterialsList->SelectedItem);
		this->MaterialsList->Text = "";

		this->RedBar->Value = 0;
		this->GreenBar->Value = 0;
		this->BlueBar->Value = 0;
		this->ForceBar->Value = 0;

		this->RedBar->Enabled = false;
		this->GreenBar->Enabled = false;
		this->BlueBar->Enabled = false;
		this->ForceBar->Enabled = false;
	}
}

System::Void cWindow::MaterialsList_SelectedIndexChanged(System::Object ^sender, System::EventArgs ^e)
{
	if (this->MaterialsList->Items->Count > 0)
	{
		this->RedBar->Value = (int)(MainMaterials[this->MaterialsList->SelectedIndex]->getColor(0)->Color.r * 255.0f);
		this->GreenBar->Value = (int)(MainMaterials[this->MaterialsList->SelectedIndex]->getColor(0)->Color.g * 255.0f);
		this->BlueBar->Value = (int)(MainMaterials[this->MaterialsList->SelectedIndex]->getColor(0)->Color.b * 255.0f);
		this->ForceBar->Value = (int)(MainMaterials[this->MaterialsList->SelectedIndex]->getSpecular(0)->Color.r * 100.0f);

		this->RedBar->Enabled = true;
		this->GreenBar->Enabled = true;
		this->BlueBar->Enabled = true;
		this->ForceBar->Enabled = true;
	}
}

System::Void cWindow::RedBar_Scroll(System::Object ^sender, System::EventArgs ^e)
{
	if (this->MaterialsList->Items->Count > 0)
	{
		MainMaterials[this->MaterialsList->SelectedIndex]->getColor(0)->Color.r = (float)this->RedBar->Value / 255.0f;
	}
}

System::Void cWindow::GreenBar_Scroll(System::Object ^sender, System::EventArgs ^e)
{
	if (this->MaterialsList->Items->Count > 0)
	{
		MainMaterials[this->MaterialsList->SelectedIndex]->getColor(0)->Color.g = (float)this->GreenBar->Value / 255.0f;
	}
}

System::Void cWindow::BlueBar_Scroll(System::Object ^sender, System::EventArgs ^e)
{
	if (this->MaterialsList->Items->Count > 0)
	{
		MainMaterials[this->MaterialsList->SelectedIndex]->getColor(0)->Color.b = (float)this->BlueBar->Value / 255.0f;
	}
}

System::Void cWindow::ForceBar_Scroll(System::Object ^sender, System::EventArgs ^e)
{
	if (this->MaterialsList->Items->Count > 0)
	{
		MainMaterials[this->MaterialsList->SelectedIndex]->getSpecular(0)->Color.r = (float)this->ForceBar->Value / 255.0f;
	}
}

bool MeshCreatingMode = false;

System::Void cWindow::CreateMeshButton_Click(System::Object ^sender, System::EventArgs ^e)
{
	if (MeshCreatingMode)
	{
		if (this->RawDataListActive->SelectedIndex < 0)
		{
			MessageBox::Show(this, "Raw data is not set!", "Mesh creating failed!", MessageBoxButtons::OK, MessageBoxIcon::Error);
			return;
		}

		Faber::Content::Native::sMeshRawData *RawData = nullptr;
		switch (this->RawDataListActive->SelectedIndex)
		{
		case 0:
			RawData = TriangleData;
			break;

		case 1:
			RawData = RectangleData;
			break;

		case 2:
			RawData = CircleData;
			break;

		case 3:
			RawData = ConeData;
			break;

		case 4:
			RawData = CubeData;
			break;

		case 5:
			RawData = CylinderData;
			break;

		case 6:
			RawData = SphereData;
			break;

		default:
			return;
		}

		if (this->MaterialsList->Items->Count <= 0)
		{
			MessageBox::Show(this, "You have to create Material first!", "Mesh creating failed!", MessageBoxButtons::OK, MessageBoxIcon::Error);
			return;
		}

		if (this->MaterialsListActive->SelectedIndex < 0)
		{
			MessageBox::Show(this, "Material is not set!", "Mesh creating failed!", MessageBoxButtons::OK, MessageBoxIcon::Error);
			return;
		}

		Faber::Content::cMaterial *Material = MainMaterials[(unsigned int)this->MaterialsListActive->SelectedIndex];

		Faber::Content::Native::cMesh *Mesh = nullptr;
		Faber::Content::createMesh(MainDev, (void**)&Mesh, RawData, Material);

		MainMeshes.push_back(Mesh);

		this->MeshesList->Items->Add(this->MeshName->Text);
		this->MeshesList->SelectedIndex = this->MeshesList->Items->Count - 1;
		this->MeshName->Text = "";

		this->RawDataListActive->Enabled = false;
		this->RawDataListActive->SelectedIndex = -1;

		this->CreateMeshButton->Text = "Create Mesh";
		this->DeleteMeshButton->Text = "Delete Mesh";
		this->MeshesLabel->Text = "Meshes List:";
		this->MeshesList->Visible = true;
		this->MeshName->Visible = false;
		MeshCreatingMode = false;
	}
	else
	{
		this->MaterialsListActive->Enabled = true;
		this->MaterialsListActive->SelectedIndex = -1;

		this->RawDataListActive->Enabled = true;
		this->RawDataListActive->SelectedIndex = -1;

		this->CreateMeshButton->Text = "Accept";
		this->DeleteMeshButton->Text = "Cancel";
		this->MeshesLabel->Text = "New Name:";
		this->MeshName->Visible = true;
		this->MeshName->Focus();
		this->MeshesList->Visible = false;
		MeshCreatingMode = true;
	}
}

System::Void cWindow::DeleteMeshButton_Click(System::Object ^sender, System::EventArgs ^e)
{
	if (MeshCreatingMode)
	{
		if (this->MeshesList->Items->Count > 0)
		{
			int Index = 0;
			for (Index; Index < this->MaterialsList->Items->Count; Index++)
			{
				if (MainMeshes[(unsigned int)this->MeshesList->SelectedIndex]->getMaterial() == MainMaterials[Index])
					break;
			}
			this->MaterialsListActive->SelectedIndex = Index;
		}
		else
		{
			this->MaterialsListActive->Enabled = false;
			this->MaterialsListActive->SelectedIndex = -1;
		}

		this->RawDataListActive->Enabled = false;
		this->RawDataListActive->SelectedIndex = -1;

		this->CreateMeshButton->Text = "Create Mesh";
		this->DeleteMeshButton->Text = "Delete Mesh";
		this->MeshesLabel->Text = "Meshes List:";
		this->MeshesList->Visible = true;
		this->MeshName->Visible = false;
		MeshCreatingMode = false;
	}
	else
	{
		if (this->MeshesList->Items->Count > 0)
		{
			Faber::Content::destroyMesh(MainDev, (void**)&MainMeshes[(unsigned int)this->MeshesList->SelectedIndex]);
			MainMeshes.erase(MainMeshes.begin() + this->MeshesList->SelectedIndex);
		}

		this->MeshesList->Items->Remove(this->MeshesList->SelectedItem);
		this->MeshesList->Text = "";

		this->MaterialsListActive->Enabled = false;
		this->MaterialsListActive->SelectedIndex = -1;
	}
}

System::Void cWindow::MeshesList_SelectedIndexChanged(System::Object ^sender, System::EventArgs ^e)
{
	if (this->MeshesList->Items->Count > 0)
	{
		Faber::Content::cMaterial *Material = MainMeshes[(unsigned int)this->MeshesList->SelectedIndex]->getMaterial();
		for (unsigned int i = 0; i < (unsigned int)this->MaterialsList->Items->Count; i++)
		{
			if (MainMaterials[i] == Material)
			{
				this->MaterialsListActive->Enabled = true;
				this->MaterialsListActive->SelectedIndex = i;
			}
		}
	}
}

System::Void cWindow::MaterialsListActive_SelectedIndexChanged(System::Object ^sender, System::EventArgs ^e)
{
	if (!MeshCreatingMode)
	{
		if (this->MeshesList->SelectedIndex >= 0 && this->MaterialsListActive->SelectedIndex >= 0)
		{
			MainMeshes[(unsigned int)this->MeshesList->SelectedIndex]->setMaterial(MainMaterials[(unsigned int)this->MaterialsListActive->SelectedIndex]);
		}
	}
}

bool StaticObjectCreatingMode = false;

void ConvertToContainedList(System::Windows::Forms::ComboBox ^ParentList, System::Windows::Forms::ComboBox ^ChildList, Faber::Content::cStaticObject *StaticObject)
{
	ChildList->Items->Clear();

	std::set <Faber::Content::Native::cMesh*> Meshes = StaticObject->getMeshes();

	for (int i = 0; i < ParentList->Items->Count; i++)
	{
		for (std::set <Faber::Content::Native::cMesh*>::iterator it = Meshes.begin(); it != Meshes.end(); ++it)
		{
			if (MainMeshes[i] == it._Ptr->_Myval)
			{
				ChildList->Items->Add(ParentList->Items[i]);
			}
		}
	}
}

void ConvertToMeshList(System::Windows::Forms::ComboBox ^ParentList, System::Windows::Forms::ComboBox ^ChildList)
{
	ChildList->Items->Clear();

	for (int i = 0; i < ParentList->Items->Count; i++)
	{
		ChildList->Items->Add(ParentList->Items[i]);
	}
}

System::Void cWindow::CreateStaticObjectButton_Click(System::Object ^sender, System::EventArgs ^e)
{
	if (StaticObjectCreatingMode)
	{
		Faber::Content::cStaticObject *StaticObject = nullptr;
		Faber::Content::createStaticObject((void**)&StaticObject);

		Objects.push_back(StaticObject);
		MainScene->addStaticObject(StaticObject);

		if (this->StaticObjectsList->Items->Count <= 0)
		{
			this->ObjectsMeshesList->Enabled = true;
			this->AddMeshButton->Enabled = true;
			this->RemoveMeshButton->Enabled = true;
		}

		this->StaticObjectsList->Items->Add(this->StaticObjectName->Text);
		this->StaticObjectsList->SelectedIndex = this->StaticObjectsList->Items->Count - 1;
		this->StaticObjectName->Text = "";

		this->CreateStaticObjectButton->Text = "Create Static Object";
		this->DeleteStaticObjectButton->Text = "Delete Static Object";
		this->StaticObjectLabel->Text = "Static Objects List:";
		this->StaticObjectsList->Visible = true;
		this->StaticObjectName->Visible = false;
		StaticObjectCreatingMode = false;
	}
	else
	{
		this->CreateStaticObjectButton->Text = "Accept";
		this->DeleteStaticObjectButton->Text = "Cancel";
		this->StaticObjectLabel->Text = "New Name:";
		this->StaticObjectName->Visible = true;
		this->StaticObjectName->Focus();
		this->StaticObjectsList->Visible = false;
		StaticObjectCreatingMode = true;

		this->ObjectsMeshesList->Enabled = false;
		this->AddMeshButton->Enabled = false;
		this->RemoveMeshButton->Enabled = false;
	}
}

System::Void cWindow::DeleteStaticObjectButton_Click(System::Object ^sender, System::EventArgs ^e)
{
	if (StaticObjectCreatingMode)
	{
		this->CreateStaticObjectButton->Text = "Create Static Object";
		this->DeleteStaticObjectButton->Text = "Delete Static Object";
		this->StaticObjectLabel->Text = "Static Objects List:";
		this->StaticObjectsList->Visible = true;
		this->StaticObjectName->Visible = false;
		StaticObjectCreatingMode = false;

		if (this->StaticObjectsList->Items->Count > 0)
		{
			this->ObjectsMeshesList->Enabled = true;
			this->AddMeshButton->Enabled = true;
			this->RemoveMeshButton->Enabled = true;
		}
	}
	else
	{
		if (this->StaticObjectsList->Items->Count > 0)
		{
			MainScene->removeStaticObject(Objects[(unsigned int)this->StaticObjectsList->SelectedIndex]);
			Faber::Content::destroyStaticObject((void**)&Objects[(unsigned int)this->StaticObjectsList->SelectedIndex]);
			Objects.erase(Objects.begin() + this->StaticObjectsList->SelectedIndex);
		}

		this->StaticObjectsList->Items->Remove(this->StaticObjectsList->SelectedItem);
		this->StaticObjectsList->Text = "";

		this->ObjectsMeshesList->Enabled = false;
		this->AddMeshButton->Enabled = false;
		this->RemoveMeshButton->Enabled = false;
	}
}

System::Void cWindow::StaticObjectsList_SelectedIndexChanged(System::Object ^sender, System::EventArgs ^e)
{
	if (this->StaticObjectsList->Items->Count > 0)
	{
		this->ObjectsMeshesList->Enabled = true;
		this->AddMeshButton->Enabled = true;
		this->RemoveMeshButton->Enabled = true;

		ConvertToContainedList(this->MeshesList, this->ObjectsMeshesList, Objects[(unsigned int)this->StaticObjectsList->SelectedIndex]);

		//MainArrows->setPositionScale(Objects[(unsigned int)this->StaticObjectsList->SelectedIndex]->getPosition(), Objects[(unsigned int)this->StaticObjectsList->SelectedIndex]->getScale());
		//MainArrows->show();
	}
}

bool AddingMeshMode = false;

System::Void cWindow::AddMeshButton_Click(System::Object ^sender, System::EventArgs ^e)
{
	if(!AddingMeshMode)
	{
		this->RemoveMeshButton->Text = "Cancel";

		ConvertToMeshList(this->MeshesList, this->ObjectsMeshesList);
		this->ObjectsMeshesList->DroppedDown = true;

		this->StaticObjectsList->Enabled = false;
		this->CreateStaticObjectButton->Enabled = false;
		this->DeleteStaticObjectButton->Enabled = false;

		AddingMeshMode = true;
	}
}

System::Void cWindow::RemoveMeshButton_Click(System::Object ^sender, System::EventArgs ^e)
{
	if (AddingMeshMode)
	{
		ConvertToContainedList(this->MeshesList, this->ObjectsMeshesList, Objects[(unsigned int)this->StaticObjectsList->SelectedIndex]);

		this->RemoveMeshButton->Text = "Remove Mesh";

		this->StaticObjectsList->Enabled = true;
		this->CreateStaticObjectButton->Enabled = true;
		this->DeleteStaticObjectButton->Enabled = true;

		AddingMeshMode = false;
	}
	else
	{
		if (this->ObjectsMeshesList->Items->Count > 0)
		{
			Objects[(unsigned int)this->StaticObjectsList->SelectedIndex]->removeMesh(MainMeshes[(unsigned int)this->ObjectsMeshesList->SelectedIndex]);
		}

		this->ObjectsMeshesList->Items->Remove(this->ObjectsMeshesList->SelectedItem);
		this->ObjectsMeshesList->Text = "";
	}
}

System::Void cWindow::ObjectsMeshesList_SelectedIndexChanged(System::Object ^sender, System::EventArgs ^e)
{
	if (AddingMeshMode)
	{
		Objects[(unsigned int)this->StaticObjectsList->SelectedIndex]->addMesh(MainMeshes[(unsigned int)this->ObjectsMeshesList->SelectedIndex]);

		ConvertToContainedList(this->MeshesList, this->ObjectsMeshesList, Objects[(unsigned int)this->StaticObjectsList->SelectedIndex]);

		this->RemoveMeshButton->Text = "Remove Mesh";

		this->StaticObjectsList->Enabled = true;
		this->CreateStaticObjectButton->Enabled = true;
		this->DeleteStaticObjectButton->Enabled = true;

		AddingMeshMode = false;
	}
}

System::Void cWindow::Panel_MouseDown(System::Object ^sender, System::Windows::Forms::MouseEventArgs ^e)
{
	if (e->Button == System::Windows::Forms::MouseButtons::Left)
	{
		MouseInput[0] = true;
	}
	if (e->Button == System::Windows::Forms::MouseButtons::Right)
	{
		MouseInput[1] = true;
	}
}

System::Void cWindow::Panel_MouseUp(System::Object ^sender, System::Windows::Forms::MouseEventArgs ^e)
{
	if (e->Button == System::Windows::Forms::MouseButtons::Left)
	{
		MouseInput[0] = false;
	}
	if (e->Button == System::Windows::Forms::MouseButtons::Right)
	{
		MouseInput[1] = false;
	}
}