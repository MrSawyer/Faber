#pragma once

#ifdef BUILD_LIBRARY
	#define FaberEngine __declspec(dllexport)
#else
	#define FaberEngine __declspec(dllimport)
#endif