#include "ErrorHandle.h"
#include <iostream>
#include <iomanip>
#include <algorithm>

namespace Faber::ErrorHandler {
	static cErrorLogs ErrorLogs;

	void cErrorLogs::addError(sLOG NEW_LOG) {
		Logs.push_back(NEW_LOG);
		std::sort(Logs.begin(), Logs.end());
	}


	sLOG::sLOG() {}
	sLOG::sLOG(std::string Fnc, std::string F, unsigned int Lin, std::string Err, bool W) {
		Function = Fnc;
		File = F;
		Line = Lin;
		Error = Err;
		Warning = W;
	}
	sLOG::sLOG(std::string Fnc, std::string F, unsigned int Lin, std::string Err, unsigned int	T, bool	W) {
		Function = Fnc;
		File = F;
		Line = Lin;
		Error = Err;
		Warning = W;
		Time = T;
	}

	bool sLOG::operator < (const sLOG & Right) {
		if (File < Right.File)return true;
		if (File > Right.File)return false;


		if (Function < Right.Function)return true;
		if (Function > Right.Function)return false;

		if (Warning < Right.Warning)return true;
		if (Warning > Right.Warning)return false;

		if (Line < Right.Line)return true;
		if (Line > Right.Line)return false;

		if (Time < Right.Time)return true;
		if (Time > Right.Time)return false;


		if (Error < Right.Error)return true;
		return false;
	}


	void printLogs() {
		std::string File;
		std::cout << std::endl;

		std::cout << "__________________________________________________________________________" << '\n';
		std::cout << "Logs captured at: " << clock() << std::endl;

		unsigned int LONGEST_ERROR = 0;
		unsigned int LONGEST_FUNCTION = 0;
		unsigned int LONGEST_TIME = 0;
		unsigned int LONGEST_LINE = 0;

		for (unsigned int i = 0; i < (unsigned int)ErrorLogs.Logs.size(); ++i) {
			if (strlen(std::to_string(ErrorLogs.Logs[i].Time).c_str()) > LONGEST_TIME)	LONGEST_TIME = strlen(std::to_string(ErrorLogs.Logs[i].Time).c_str()) + 2;
			if (strlen(std::to_string(ErrorLogs.Logs[i].Line).c_str()) > LONGEST_LINE)	LONGEST_LINE = strlen(std::to_string(ErrorLogs.Logs[i].Line).c_str()) + 2;
			if (strlen(ErrorLogs.Logs[i].Error.c_str()) > LONGEST_ERROR)				LONGEST_ERROR = strlen(ErrorLogs.Logs[i].Error.c_str()) + 2;
			if (strlen(ErrorLogs.Logs[i].Function.c_str()) > LONGEST_FUNCTION)			LONGEST_FUNCTION = strlen(ErrorLogs.Logs[i].Function.c_str()) + 2;
		}

		for (unsigned int i = 0; i < (unsigned int)ErrorLogs.Logs.size(); ++i) {

			if (ErrorLogs.Logs[i].File != File) {
				std::cout<< std::endl;
				std::cout << "__________________________________________________________________________" << '\n';
				std::cout << std::left << "IN FILE: " << ErrorLogs.Logs[i].File << '\n';
				std::cout << "__________________________________________________________________________" << '\n';
			}
			if (!ErrorLogs.Logs[i].Warning) {
				std::cout << std::left << std::setw(9) << "ERROR: ";
			}else std::cout << std::left << std::setw(9) << "WARNING: ";

			std::cout << std::left << std::setw(LONGEST_TIME) << ErrorLogs.Logs[i].Time<< std::setw(LONGEST_ERROR) << ErrorLogs.Logs[i].Error << std::setw(LONGEST_FUNCTION) << ErrorLogs.Logs[i].Function << std::setw(LONGEST_LINE) << ErrorLogs.Logs[i].Line << '\n';
			File = ErrorLogs.Logs[i].File;
		}
		std::cout << "__________________________________________________________________________" << '\n';
	}


	void addErrorToLog(std::string Error, LPCTSTR FunctionName, LPCTSTR File, unsigned int Line, bool Warning) {
		if (!Warning)MessageBox(NULL, (LPCTSTR)("Error: " + Error + " in file " + File + " on line "+ std::to_string(Line)).c_str(), "Error", MB_ICONERROR | MB_OK);
		else
			MessageBox(NULL, (LPCTSTR)("Warning: " + Error + " in file " + File + " on line " + std::to_string(Line)).c_str(), "Warning", MB_ICONWARNING | MB_OK);

		sLOG NewLog;
		NewLog.Error = Error;
		NewLog.File = File;
		NewLog.Function = FunctionName;
		NewLog.Line = Line;
		NewLog.Time = clock();
		NewLog.Warning = Warning;
		ErrorLogs.addError(NewLog);
	}
	void addErrorToLog(std::string Error, HWND hWnd, LPCTSTR FunctionName, LPCTSTR File, unsigned int Line, bool Warning) {
		if(!Warning)MessageBox(hWnd, (LPCTSTR)Error.c_str(), FunctionName, MB_ICONERROR | MB_OK);
		else
			MessageBox(hWnd, (LPCTSTR)("Warning: " + Error + " in file " + File + " on line " + std::to_string(Line)).c_str(), "Warning", MB_ICONWARNING | MB_OK);

		sLOG NewLog;
		NewLog.Error = Error;
		NewLog.File = File;
		NewLog.Function = FunctionName;
		NewLog.Line = Line;
		NewLog.Time = clock();
		NewLog.Warning = Warning;
		ErrorLogs.addError(NewLog);
	}

	void addErrorToLog(sLOG Log) {
		if (!Log.Warning)MessageBox(NULL, (LPCTSTR)("Error: " + Log.Error + " in file " + Log.File + " on line " + std::to_string(Log.Line)).c_str(), "Error", MB_ICONERROR | MB_OK);
		else
			MessageBox(NULL, (LPCTSTR)("Warning: " + Log.Error + " in file " + Log.File + " on line " + std::to_string(Log.Line)).c_str(), "Warning", MB_ICONWARNING | MB_OK);
	
		ErrorLogs.addError(Log);
	}

	bool errorLogicANDCheck(unsigned int n_args, ...) {
		va_list args;
		va_start(args, n_args);

		for (unsigned int i = 0; i < n_args; i++) {
			if (!va_arg(args, bool))return false;
		}
		return true;
	}
	FaberEngine bool  errorLogicORCheck(unsigned int n_args, ...) {
		va_list args;
		va_start(args, n_args);

		for (unsigned int i = 0; i < n_args; i++) {
			if (va_arg(args, bool))return true;
		}
		return false;
	}

}