#pragma once
#include "../Config.h"
#include <vector>
#include <string>
#include <time.h> 
#include <Windows.h>
#include<stdarg.h>

namespace Faber::ErrorHandler {
	class cWatchListObjectInterface;
	struct sLOG {
		std::string		Function;
		std::string		File;
		unsigned int	Line;
		std::string		Error;
		unsigned int	Time;
		bool			Warning;
		FaberEngine sLOG();
		FaberEngine sLOG(std::string Fnc, std::string File, unsigned int Lin, std::string Err, bool	Warning = false);
		FaberEngine sLOG(std::string Fnc, std::string File, unsigned int Lin, std::string Err, unsigned int	Time, bool	Warning = false);

		FaberEngine bool operator < (const sLOG & Right);
	};
	/*
	* Dodaje log do spisu
	*/
	FaberEngine void	addErrorToLog(sLOG Log);
	/*
	* Dodaje b��d do spisu
	*/
	FaberEngine void	addErrorToLog(std::string Error, LPCTSTR FunctionName, LPCTSTR File, unsigned int Line, bool Warning = false);
	/*
	* Dodaje b��d do spisu uwzgl�dniaj�c uchwyt okna
	*/
	FaberEngine void	addErrorToLog(std::string Error, HWND hWnd, LPCTSTR FunctionName, LPCTSTR File, unsigned int Line, bool Warning = false);
	/*
	* Sprawdza czy prawdziwa jest koniunkcja logiczna pomi�dzy argumentami
	*/
	FaberEngine bool	errorLogicANDCheck(unsigned int n_args, ...);
	/*
	* Sprawdza czy prawdziwa jest alternatywa logiczna pomi�dzy argumentami
	*/
	FaberEngine bool	errorLogicORCheck(unsigned int n_args, ...);
	/*
	* Tworzy pusty log
	*/
	#define createLog(Error) Faber::ErrorHandler::sLOG(__FUNCTION__, __FILE__,__LINE__, Error);
	/*
	* Dodaje log do spisu b��d�w
	*/
	#define throwErrorLog(Log) Faber::ErrorHandler::addErrorToLog (Log);
	/*
	* Wy�wietla okno o b��dzie i dodaje b��d do spisu
	*/
	#define throwError(Error) Faber::ErrorHandler::addErrorToLog (Error ,__FUNCTION__, __FILE__, __LINE__);
	/*
	* Wy�wietla okno o b��dzie, uwzgl�dniaj�c uchwyt okna, i dodaje b��d do spisu
	*/
	#define throwErrorHWND(Error, hWnd) Faber::ErrorHandler::addErrorToLog (Error, hWnd ,__FUNCTION__, __FILE__, __LINE__);
	/*
	* Wy�wietla okno o ostrze�eniu i dodaje ostrze�enie do spisu
	*/
	#define throwWarning(Exception) Faber::ErrorHandler::addErrorToLog (Exception ,__FUNCTION__, __FILE__, __LINE__, true);
	/*
	* Wy�wietla okno o ostrze�eniu, uwzgl�dniaj�c uchwyt okna, i dodaje ostrze�enie do spisu
	*/
	#define throwWarningHWND(Exception, hWnd) Faber::ErrorHandler::addErrorToLog (Exception, hWnd ,__FUNCTION__, __FILE__, __LINE__, true);
	/*
	* Wy�wietla okno o b��dzie, i zwraca false w funkcji w kt�rej b��d wyst�pi� oraz dodaje b��d do spisu
	*/
	#define exitWithError(Error, ReturnValue) {Faber::ErrorHandler::addErrorToLog (Error,__FUNCTION__, __FILE__, __LINE__); return ReturnValue;};
	/*
	* Wy�wietla okno o b��dzie, uwzgl�dniaj�c uchwyt okna, i zwraca false w funkcji w kt�rej b��d wyst�pi� oraz dodaje b��d do spisu
	*/
	#define exitWithErrorHWND(Error, hWnd, ReturnValue) {Faber::ErrorHandler::addErrorToLog (Error, hWnd ,__FUNCTION__, __FILE__, __LINE__), return ReturnValue;};
	/*
	* Wy�wietla okno o ostrze�eniu, i zwraca false w funkcji w kt�rej ostrze�enie wyst�pi�o oraz dodaje ostrze�enie do spisu
	*/
	#define exitWithWarning(Error, ReturnValue) {Faber::ErrorHandler::addErrorToLog (Error,__FUNCTION__, __FILE__, __LINE__, true); return ReturnValue;};
	/*
	* Wy�wietla okno o ostrze�eniu, uwzgl�dniaj�c uchwyt okna, i zwraca false w funkcji w kt�rej ostrze�enie wyst�pi�o oraz dodaje ostrze�enie do spisu
	*/
	#define exitWithWarningHWND(Error, hWnd, ReturnValue) {Faber::ErrorHandler::addErrorToLog (Error, hWnd ,__FUNCTION__, __FILE__, __LINE__, true), return ReturnValue;};
	/*
	* Wy�wietla okno o b��dzie je�li wszystkie warunki zadane na argumencie s� spe�nione, oraz dodaje b��d do spisu
	*/
	#define ANDlogicCheckError(Error, n_args, ...) {if(Faber::ErrorHandler::errorLogicANDCheck(n_args, ##__VA_ARGS__))throwError(Error);};
	/*
	* Wy�wietla okno o b��dzie, uwzgl�dniaj�c uchwyt okna, je�li wszystkie warunki zadane na argumencie s� spe�nione, oraz dodaje b��d do spisu
	*/
	#define ANDlogicCheckErrorHWND(Error, hWnd , n_args, ...) {if(Faber::ErrorHandler::errorLogicANDCheck(n_args, ##__VA_ARGS__))throwErrorHWND(Error, hWnd);};
	/*
	* Wy�wietla okno o b��dzie je�li wszystkie warunki zadane na argumencie s� spe�nione, zwraca fa�sz w funkcji w kt�rej b��d wyst�pi� oraz dodaje b��d do spisu
	*/
	#define ANDlogicCheckErrorExit(Error, n_args, ...) {if(Faber::ErrorHandler::errorLogicANDCheck(n_args, ##__VA_ARGS__))exitWithError(Error, ReturnValue);};
	/*
	* Wy�wietla okno o b��dzie, uwzgl�dniaj�c uchwyt okna, je�li wszystkie warunki zadane na argumencie s� spe�nione, zwraca fa�sz w funkcji w kt�rej b��d wyst�pi� oraz dodaje b��d do spisu
	*/
	#define ANDlogicCheckErrorExitHWND(Error, hWnd, n_args, ...) {if(Faber::ErrorHandler::errorLogicANDCheck(n_args, ##__VA_ARGS__))exitWithErrorHWND(Error, hWnd, ReturnValue);};
	/*
	* Wy�wietla okno o b��dzie je�li jakikolwiek warunek zadany na argumencie jest spe�niony oraz dodaje b��d do spisu
	*/
	#define ORlogicCheckError(Error, n_args, ...) {if(Faber::ErrorHandler::errorLogicORCheck(n_args, ##__VA_ARGS__))throwError(Error);};
	/*
	* Wy�wietla okno o b��dzie, uwzgl�dniaj�c uchwyt okna, je�li jakikolwiek warunek zadany na argumencie jest spe�niony oraz dodaje b��d do spisu
	*/
	#define ORlogicCheckErrorHWND(Error, hWnd, n_args, ...) {if(Faber::ErrorHandler::errorLogicORCheck(n_args, ##__VA_ARGS__))throwErrorHWND(Error, hWnd);};
	/*
	* Wy�wietla okno o b��dzie je�li jakikolwiek warunek zadany na argumencie jest spe�niony, zwraca fa�sz w funkcji w kt�rej b��d wyst�pi� oraz dodaje b��d do spisu
	*/
	#define ORlogicCheckErrorExit(Error, ReturnValue, n_args, ...) {if(Faber::ErrorHandler::errorLogicORCheck(n_args, ##__VA_ARGS__))exitWithError(Error, ReturnValue);};
	/*
	* Wy�wietla okno o b��dzie, uwzgl�dniaj�c uchwyt okna, je�li jakikolwiek warunek zadany na argumencie jest spe�niony, zwraca fa�sz w funkcji w kt�rej b��d wyst�pi� oraz dodaje b��d do spisu
	*/
	#define ORlogicCheckErrorExitHWND(Error, hWnd, ReturnValue, n_args, ...) {if(Faber::ErrorHandler::errorLogicORCheck(n_args, ##__VA_ARGS__))exitWithErrorHWND(Error, hWnd, ReturnValue);};
	/*
	* Wy�wietla okno o ostrze�eniu je�li wszystkie warunki zadane na argumencie s� spe�nione oraz dodaje ostrze�enie do spisu
	*/
	#define ANDlogicCheckWarning(Error, n_args, ...) {if(Faber::ErrorHandler::errorLogicANDCheck(n_args, ##__VA_ARGS__))throwWarning(Error);};
	/*
	* Wy�wietla okno o ostrze�eniu, uwzgl�dniaj�c uchwyt okna, je�li wszystkie warunki zadane na argumencie s� spe�nione oraz dodaje ostrze�enie do spisu
	*/
	#define ANDlogicCheckWarningHWND(Error,hWnd , n_args, ...) {if(Faber::ErrorHandler::errorLogicANDCheck(n_args, ##__VA_ARGS__))throwWarningHWND(Error, hWnd);};
	/*
	* Wy�wietla okno o ostrze�eniu je�li wszystkie warunki zadane na argumencie s� spe�nione, zwraca fa�sz w funkcji w kt�rej ostrze�enie wyst�pi�o oraz dodaje ostrze�enie do spisu
	*/
	#define ANDlogicCheckWarningExit(Error, ReturnValue, n_args, ...) {if(Faber::ErrorHandler::errorLogicANDCheck(n_args, ##__VA_ARGS__))exitWithWarning(Error, ReturnValue);};
	/*
	* Wy�wietla okno o ostrze�eniu, uwzgl�dniaj�c uchwyt okna, je�li wszystkie warunki zadane na argumencie s� spe�nione, zwraca fa�sz w funkcji w kt�rej ostrze�enie wyst�pi�o oraz dodaje ostrze�enie do spisu
	*/
	#define ANDlogicCheckWarningExitHWND(Error, hWnd, ReturnValue, n_args, ...) {if(Faber::ErrorHandler::errorLogicANDCheck(n_args, ##__VA_ARGS__))exitWithWarningHWND(Error, hWnd, ReturnValue);};
	/*
	* Wy�wietla okno o ostrze�eniu je�li jakikolwiek warunek zadany na argumencie jest spe�niony oraz dodaje ostrze�enie do spisu
	*/
	#define ORlogicCheckWarning(Error, n_args, ...) {if(Faber::ErrorHandler::errorLogicORCheck(n_args, ##__VA_ARGS__))throwWarning(Error);};
	/*
	* Wy�wietla okno o ostrze�eniu, uwzgl�dniaj�c uchwyt okna, je�li jakikolwiek warunek zadany na argumencie jest spe�niony oraz dodaje ostrze�enie do spisu 
	*/
	#define ORlogicCheckWarningHWND(Error, hWnd, n_args, ...) {if(Faber::ErrorHandler::errorLogicORCheck(n_args, ##__VA_ARGS__))throwWarningHWND(Error, hWnd);};
	/*
	* Wy�wietla okno o ostrze�eniu je�li jakikolwiek warunek zadany na argumencie jest spe�niony, zwraca fa�sz w funkcji w kt�rej ostrze�enie wyst�pi�o oraz dodaje ostrze�enie do spisu
	*/
	#define ORlogicCheckWarningExit(Error, ReturnValue, n_args, ...) {if(Faber::ErrorHandler::errorLogicORCheck(n_args, ##__VA_ARGS__))exitWithWarning(Error, ReturnValue);};
	/*
	* Wy�wietla okno o ostrze�eniu, uwzgl�dniaj�c uchwyt okna, je�li jakikolwiek warunek zadany na argumencie jest spe�niony, zwraca fa�sz w funkcji w kt�rej ostrze�enie wyst�pi�o oraz dodaje ostrze�enie do spisu
	*/
	#define ORlogicCheckWarningExitHWND(Error, hWnd, ReturnValue, n_args, ...) {if(Faber::ErrorHandler::errorLogicORCheck(n_args, ##__VA_ARGS__))exitWithWarningHWND(Error, hWnd, ReturnValue);};



	class cErrorLogs {
		std::vector<sLOG>		Logs;
		friend void FaberEngine	printLogs();
	public:
		void					addError(sLOG NEW_LOG);
	};
	/*
	* Wy�wietla logi w konsoli
	*/
	void FaberEngine printLogs();
}