#pragma once

#include <Windows.h>
#include <GLM/glm.hpp>
#include "../Config.h"

namespace Faber
{
	namespace Input
	{
		FaberEngine void		zeroParameters();
		FaberEngine void		setMousePosition(int PositionX, int PositionY);
		FaberEngine void		setMouseButtonState(unsigned int ID, bool Value);
		FaberEngine void		setKeyboardButtonState(unsigned int ID, bool Value);
		FaberEngine glm::ivec2	getNativeMousePosition();
		FaberEngine bool		getMouseButtonState(unsigned int ID);
		FaberEngine bool		getKeyboardButtonState(unsigned int ID);
	}
}