#include "Input.h"
#include "NativeInput.h"

namespace Faber
{
	namespace Input
	{
		bool keyPressed(eMouseInputTypes Type)
		{
			return getMouseButtonState(Type);
		}

		bool keyPressed(eKeyboardInputTypes Type)
		{
			return getKeyboardButtonState(Type);
		}

		glm::ivec2 getMousePosition()
		{
			return getNativeMousePosition();
		}
	}
}