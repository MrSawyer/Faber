#include "NativeInput.h"

namespace Faber
{
	namespace Input
	{
		glm::ivec2	MousePosition;
		bool		MouseButtonState[2];
		bool		KeyboardButtonState[256];

		void zeroParameters()
		{
			MousePosition = glm::ivec2(0, 0);
			for (unsigned int i = 0; i < 2; i++)
			{
				MouseButtonState[i] = false;
			}
			for (unsigned int i = 0; i < 256; i++)
			{
				KeyboardButtonState[i] = false;
			}
		}

		void setMousePosition(int PositionX, int PositionY)
		{
			MousePosition.x = PositionX;
			MousePosition.y = PositionY;
		}

		void setMouseButtonState(unsigned int ID, bool Value)
		{
			MouseButtonState[ID] = Value;
		}

		void setKeyboardButtonState(unsigned int ID, bool Value)
		{
			KeyboardButtonState[ID] = Value;
		}

		glm::ivec2 getNativeMousePosition()
		{
			return MousePosition;
		}

		bool getMouseButtonState(unsigned int ID)
		{
			return MouseButtonState[ID];
		}

		bool getKeyboardButtonState(unsigned int ID)
		{
			return KeyboardButtonState[ID];
		}
	}
}