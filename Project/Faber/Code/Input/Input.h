#pragma once

#include <Windows.h>
#include <GLM/glm.hpp>
#include "InputTypes.h"
#include "../Config.h"

namespace Faber
{
	namespace Input
	{
		FaberEngine bool keyPressed(eMouseInputTypes Type);
		FaberEngine bool keyPressed(eKeyboardInputTypes Type);
		FaberEngine glm::ivec2 getMousePosition();
	}
}