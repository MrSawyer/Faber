#pragma once

#include "../Config.h"
#include <set>
#include <memory>
#include "PhysicObjects.h"

namespace Faber::Physics {
	typedef std::weak_ptr<Faber::Physics::cPhysicBody> BodyWatch;
	typedef std::pair<BodyWatch, BodyWatch> CollisionPair;

	using namespace std;
	class cCollisionAlgorithm {
		set<CollisionPair> CollisionPairs;
	public:
		FaberEngine virtual void			addObject	(weak_ptr<cPhysicBody> Obj) = 0;
		FaberEngine virtual void			removeObject(weak_ptr<cPhysicBody> Obj) = 0;
		FaberEngine	virtual void			updateObject(weak_ptr<cPhysicBody> Obj) = 0;
		FaberEngine set<CollisionPair> &	getCollisionPairs();
	};
}