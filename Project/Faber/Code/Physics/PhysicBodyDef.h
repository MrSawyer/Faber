#pragma once
#include "../Config.h"
#include <GLM/glm.hpp>
#include <GLM/gtc/quaternion.hpp>

#include "BodyTypes.h"

namespace Faber::Physics {

	struct sPhysicBodyDef {
		glm::vec3	MoveVector;
		glm::vec3	RotationVector;
		eBODY_TYPE	BodyType;
		glm::vec3	Position;
		glm::quat	Rotation;
		glm::vec3	Scale;
		FaberEngine sPhysicBodyDef() {}
		FaberEngine ~sPhysicBodyDef() {}
	};
}