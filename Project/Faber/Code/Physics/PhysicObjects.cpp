#include "PhysicObjects.h"

namespace Faber {
	namespace Physics
	{
		cPhysicBody::cPhysicBody() { 
			initialize();
		}

		cPhysicBody::~cPhysicBody() {
			terminate();
		}

		void cPhysicBody::setMoveVector(glm::vec3 Vec) {
			MoveVector = Vec;
		}

		void cPhysicBody::update() {
			MoveVector.y -= 0.01f;

			if (getPosition().x < 0)MoveVector.x *= -0.9;
			if (getPosition().x > 20)MoveVector.x *= -0.9;

			if (getPosition().y < 0)MoveVector.y *= -0.9;
			if (getPosition().y > 20)MoveVector.y *= -0.9;

			if (getPosition().z < 0)MoveVector.z *= -0.9;
			if (getPosition().z > 20)MoveVector.z *= -0.9;


			this->setPosition(getPosition() + MoveVector);
			glm::quat(1, glm::vec3(1,1,1));
			glm::quat QuatAroundX = glm::quat(RotationVector.x, glm::vec3(1.0, 0.0, 0.0));
			glm::quat QuatAroundY = glm::quat(RotationVector.y, glm::vec3(0.0, 1.0, 0.0));
			glm::quat QuatAroundZ = glm::quat(RotationVector.z, glm::vec3(0.0, 0.0, 1.0));
			glm::quat finalOrientation = QuatAroundX * QuatAroundY * QuatAroundZ;
			this->setRotation(getRotation() + finalOrientation);
		}
		void cPhysicBody::handleCollision(cPhysicBody & Obj) {

			if (glm::distance(this->getPosition(), Obj.getPosition()) < this->getScale().x + Obj.getScale().x) {
				glm::vec3 RelativeVelocity = this->MoveVector - Obj.MoveVector;
				glm::vec3 CollisionVector = glm::normalize(this->getPosition() - Obj.getPosition());

				this->MoveVector += CollisionVector/100.0f;
				Obj.MoveVector += -CollisionVector / 100.0f;
			}
		}

	}
}