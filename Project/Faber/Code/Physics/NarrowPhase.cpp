#include "NarrowPhase.h"
#include <math.h>
namespace Faber::Physics {
	#define PHY_EPSILON 0.0001


	//BOX, CAPSULE,  SPHERE
	bool BOX_BOX(cCollider & BOX1, cCollider & BOX2) {
		return false;
	}
	bool SPHERE_SPHERE(cCollider & SPHERE1, cCollider & SPHERE2) {
		if (glm::distance(SPHERE1.getPosition(), SPHERE2.getPosition()) < SPHERE1.getScale().x + SPHERE2.getScale().y)return true; //already touching
		glm::vec3 OriginVector = SPHERE2.getPosition() - SPHERE1.getPosition();
		glm::vec3 RelativeMovementVector = SPHERE1.getMoveVector() - SPHERE2.getMoveVector();
		
		if (glm::dot(OriginVector, RelativeMovementVector) <= 0)return false; // they are not moving to each other 

		glm::vec3 RelativeMovementVectorNormalized = glm::normalize(RelativeMovementVector);

		float ClosestDistance = glm::dot(RelativeMovementVectorNormalized, OriginVector);
		float Distance2Power = pow((float)OriginVector.length(), 2) - pow(ClosestDistance, 2);

		if (Distance2Power < pow(SPHERE1.getScale().x + SPHERE2.getScale().y, 2))return false;

		float DistanceToShift = pow(SPHERE1.getScale().x + SPHERE2.getScale().x, 2) - Distance2Power;

		glm::vec3  ShortedVector = glm::normalize(SPHERE1.getMoveVector()) * (ClosestDistance - sqrt(DistanceToShift));
		float Ratio = 0;
		if (RelativeMovementVector.length() != 0) {
			Ratio = (float)ShortedVector.length() / (float)RelativeMovementVector.length();
		}
		SPHERE1.setMoveVector(SPHERE1.getMoveVector() * Ratio);
		SPHERE2.setMoveVector(SPHERE2.getMoveVector() * Ratio);
		return true;
	}
	bool CAPSULE_CAPSULE(cCollider & CAPSULE1, cCollider & CAPSULE2) {
		return false;
	}


	/*
	_____________________________
	*/
	void				cCollider::setType(eSHAPE_TYPE T) {
		Type = T;
	}
	void				cCollider::setMass(float M) {
		Mass = M;
	}
	void				cCollider::setElascity(float E) {
		Elascity = E;
	}
	void				cCollider::setMoveVector(glm::vec3 Vec) {
		MoveVector = Vec;
	}
	void				cCollider::setRotationVector(glm::vec3 Vec) {
		RotationVector = Vec;
	}


	glm::vec3 cCollider::getMoveVector() {
		return MoveVector;
	}

}