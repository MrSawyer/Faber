#pragma once
#include "../Config.h"
#include <GLM/glm.hpp>


namespace Faber::Physics {
	/**
	* Typ wyliczeniowy opisujacy jak dane cialo ma sie zachowywac.
	* DYNAMIC - reaguje z kazdym innym cialem.
	* KINEMATIC - koliduje z obiektami typu DYNAMIC,
	*			posiada wlasne funkcje przesuwania w czasie,
	*			jednak nie jest wrazliwe na przesuniecie zwiazane z kolizja. (np platforma).
	* STATIC - koliduje z obiektami typu DYNAMIC, jednak niemozliwa jest jego zmiana polozenia (np. teren).
	*/
	enum eBODY_TYPE {
		DYNAMIC, STATIC, KINEMATIC
	};
}