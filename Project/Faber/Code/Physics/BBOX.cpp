#include <vector>
#include "PhysicObjects.h"
namespace Faber::Physics {
	float sBBOX::getWidth() {
		return XMax - XMin;
	}

	float sBBOX::getHeight() {
		return YMax - YMin;
	}

	float sBBOX::getDepth() {
		return ZMax - ZMin;
	}

	glm::vec2 sBBOX::getXAxisPoints() {
		return glm::vec2(XMax, XMin);
	}

	glm::vec2 sBBOX::getYAxisPoints() {
		return glm::vec2(YMax, YMin);
	}

	glm::vec2 sBBOX::getZAxisPoints() {
		return glm::vec2(ZMax, ZMin);
	}

	sBBOX cPhysicBody::getAABB() {
		calculateBBOX();
		return AABB;
	}

	void cPhysicBody::calculateBBOX() {

		calculateMatrix();

		std::vector<glm::vec3> Verticles;
		Verticles.push_back(glm::vec3(-0.5f, -0.5f, 0.5f));
		Verticles.push_back(glm::vec3(0.5f, -0.5, 0.5f));
		Verticles.push_back(glm::vec3(0.5f, 0.5f, 0.5f));
		Verticles.push_back(glm::vec3(-0.5f, 0.5f, 0.5f));

		Verticles.push_back(glm::vec3(-0.5f, -0.5f, -0.5f));
		Verticles.push_back(glm::vec3(0.5f, -0.5f, -0.5f));
		Verticles.push_back(glm::vec3(0.5f, 0.5f, -0.5f));
		Verticles.push_back(glm::vec3(-0.5f, 0.5f, -0.5f));

		sBBOX NewBBOX;
		glm::vec3 TransformedFirst = getMatrix() * glm::vec4(Verticles[0], 1);
		NewBBOX.XMax = TransformedFirst.x;
		NewBBOX.XMin = TransformedFirst.x;

		NewBBOX.YMax = TransformedFirst.y;
		NewBBOX.YMin = TransformedFirst.y;

		NewBBOX.ZMax = TransformedFirst.z;
		NewBBOX.ZMin = TransformedFirst.z;

		for (int i = 1; i < (int)Verticles.size(); ++i) {
			glm::vec3 Transformed = getMatrix() * glm::vec4(Verticles[i], 1);

			if (Transformed.x > NewBBOX.XMax)NewBBOX.XMax = Transformed.x;
			if (Transformed.x < NewBBOX.XMin)NewBBOX.XMin = Transformed.x;

			if (Transformed.y > NewBBOX.YMax)NewBBOX.YMax = Transformed.y;
			if (Transformed.y < NewBBOX.YMin)NewBBOX.YMin = Transformed.y;

			if (Transformed.z > NewBBOX.ZMax)NewBBOX.ZMax = Transformed.z;
			if (Transformed.z < NewBBOX.ZMin)NewBBOX.ZMin = Transformed.z;
		}
		AABB = NewBBOX;
	}
}