#pragma once

#include "../Config.h"
#include <GLM/glm.hpp>


namespace Faber::Physics {
	FaberEngine bool			areLineSegmentsIntersect(glm::vec3 a1, glm::vec3 a2, glm::vec3 b1, glm::vec3 b2);

	FaberEngine glm::vec3		pointOfIntersection(glm::vec3 a1, glm::vec3 a2, glm::vec3 b1, glm::vec3 b2);

	FaberEngine float			closestDistanceBetweenLineSegments(glm::vec3 a1, glm::vec3 a2, glm::vec3 b1, glm::vec3 b2);
}