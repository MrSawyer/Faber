#include "Physics.h"

namespace Faber::Physics {
	typedef std::weak_ptr<Faber::Physics::cPhysicBody> BodyWatch;
	typedef std::pair<BodyWatch, BodyWatch> CollisionPair;


	void cWorld::handleCollision() {
		if (!CollisionAlgorithm.operator bool())return;
		
		set<CollisionPair> Pairs  = CollisionAlgorithm->getCollisionPairs();
		
		for (std::set<CollisionPair>::iterator it = Pairs.begin(); it != Pairs.end(); ++it) {
			BodyWatch A = it._Ptr->_Myval.first;
			BodyWatch B = it._Ptr->_Myval.second;
			
			//A->handleCollision(*B);
			
		}
	}
}