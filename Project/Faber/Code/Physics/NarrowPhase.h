#pragma once
#include "../Config.h"
#include "../Content/Native/Transformable.h"
#include <GLM/glm.hpp>
#include <vector>
#include <set>

namespace Faber::Physics {
	enum eSHAPE_TYPE {
			BOX, CAPSULE, SPHERE, MESH
	};

	class cCollider : public Content::Native::cTransformable {
		eSHAPE_TYPE			Type;
		float				Mass;
		float				Elascity;
		glm::vec3			MoveVector;
		glm::vec3			RotationVector;

	public:

		FaberEngine glm::vec3			getMoveVector();
		FaberEngine glm::vec3			getRotationVector();
		FaberEngine eSHAPE_TYPE			getType();
		FaberEngine float				getMass();
		FaberEngine float				getElascity();


		FaberEngine void				setElascity(float Elascity);
		FaberEngine void				setMass(float Mass);
		FaberEngine void				setType(eSHAPE_TYPE Type);
		FaberEngine void				setMoveVector(glm::vec3 Vec);
		FaberEngine void				setRotationVector(glm::vec3 Vec);

	};
}