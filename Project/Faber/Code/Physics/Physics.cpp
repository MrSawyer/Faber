#include "Physics.h"
#include "../ErrorHandler/ErrorHandle.h"
#include<string>
#include <iostream>

namespace Faber::Physics {

		void initialize() {
			
		}

		void terminate() {
		}

		cWorld::~cWorld() {
			BodiesOnScene.clear();
		}

		BodyWatch cWorld::createBody(sPhysicBodyDef & Def) {
			std::shared_ptr<cPhysicBody> NewBody(new cPhysicBody);
			BodiesOnScene.insert(NewBody);

					NewBody->BodyType = Def.BodyType;
					NewBody->MoveVector = Def.MoveVector;
					NewBody->setPosition(Def.Position);
					NewBody->setRotation(Def.Rotation);
					NewBody->RotationVector = Def.RotationVector;
					NewBody->setScale(Def.Scale);
					NewBody->calculateBBOX();

					if (CollisionAlgorithm.operator bool())CollisionAlgorithm->addObject(NewBody);
			return NewBody;
		}

		bool cWorld::deleteBody(BodyWatch Body) {
			if (Body.expired())return false;
			
			if (CollisionAlgorithm.operator bool())CollisionAlgorithm->removeObject(Body);
			
			BodiesOnScene.erase(Body.lock());
			return true;
		}

		void cWorld::updatePhysics() {
			if (CollisionAlgorithm.operator bool())handleCollision();
			for (auto Body :  BodiesOnScene) {
				Body->update();
				if (!Body->isUpdated()) {
					Body->calculateBBOX();
					if(CollisionAlgorithm.operator bool())CollisionAlgorithm->updateObject(Body);
				}
			}
			//for (auto PAIR : CollisionAlgorithm->getCollisionPairs()) {
				//PAIR.first->handleCollision(*PAIR.second);
			//}
		}
}
