#pragma once

#include "../Config.h"
#include <set>

#include "../Content/Native/Transformable.h"
#include "BodyTypes.h"
#include "BBOX.h"
#include "NarrowPhase.h"

namespace Faber::Physics{
		/**
		* Wlasciwa klasa ciala fizycznego.
		*/
		class cPhysicBody 
			: public Content::Native::cTransformable {
		private:
			friend class cWorld;
			
			bool						ToUpdate;
			sBBOX						AABB;
			eBODY_TYPE					BodyType;

			glm::vec3					MoveVector;
			glm::vec3					RotationVector;

			std::set<cCollider>			Shape;
			/**
			* Liczy szescian mogacy objac dane cialo fizyczne,
			* robi to na podstawie macierzy rotacji translacji oraz skali.
			*/
			FaberEngine void			calculateBBOX();

			FaberEngine void			handleCollision(cPhysicBody & Obj);

		public:
			/**
			* Konstruktor inicjalizuje obiekt Content::cTransformable
			*/
			FaberEngine					cPhysicBody();
			/**
			* Destruktor wywoluje funkcje terminate dla obiektu Content::cTransformable.
			*/
			FaberEngine					~cPhysicBody();
			/**
			* Zwraca szescian mogacy objac dane cialo fizyczne.
			*/
			FaberEngine sBBOX			getAABB();
			/*
			* Funkcja wykonywana raz na klatke 
			*/
			FaberEngine void			update();

			FaberEngine void			setRotationVector(glm::vec3);

			FaberEngine void			setMoveVector(glm::vec3);

			//FaberEngine void			applyForce(glm::vec3, eFORCE_TYPE);

		};
}