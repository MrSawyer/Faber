#pragma once

#include "../Config.h"
#include "../Content/Native/Transformable.h"
#include <GLM/glm.hpp>
#include <set>

namespace Faber {
	namespace Physics
	{
		enum AXIS {
			X, Y, Z
		};
		enum BODY_TYPE {
			DYNAMIC, STATIC, KINEMATIC
		};

		struct sBBOX {
			float XMin, XMax;
			float YMin, YMax;
			float ZMin, ZMax;

			FaberEngine float		getHeight();
			FaberEngine float		getWidth();
			FaberEngine float		getDepth();
			FaberEngine glm::vec2	getXAxisPoints();
			FaberEngine glm::vec2	getYAxisPoints();
			FaberEngine glm::vec2	getZAxisPoints();
		};

		struct sPhysicBodyDef
			: public Content::Native::cTransformable {
			
			glm::vec3	MoveVector;
			glm::vec3	RotationVector;
			float		Mass;
			float		Elasticity;
			BODY_TYPE	BodyType;

			FaberEngine sPhysicBodyDef();
			FaberEngine ~sPhysicBodyDef();
		};

		class cPhysicBody 
			: public Content::Native::cTransformable {
		private:
			friend class cWorld;
			
			bool						ToUpdate;
			sBBOX						AABB;
			BODY_TYPE					BodyType;

			glm::vec3					MoveVector;
			glm::vec3					RotationVector;

			float						Mass;
			float						Elasticity;

			void						calculateBBOX();

			std::set<cPhysicBody *>		CollidingList;
			FaberEngine sBBOX			getAABB();
		public:
			FaberEngine					cPhysicBody();
			FaberEngine					~cPhysicBody();
		};
	}
}