#pragma once
#include "../Config.h"


namespace Faber::Physics {
	/**
	* Struktura opisujaca najmniejszy szescian ulozony w kartezjanskich osiach.
	* Axis Align Bounding Box.
	*/
	struct sBBOX {
		float XMin, XMax;
		float YMin, YMax;
		float ZMin, ZMax;

		FaberEngine float		getHeight();
		FaberEngine float		getWidth();
		FaberEngine float		getDepth();
		FaberEngine glm::vec2	getXAxisPoints();
		FaberEngine glm::vec2	getYAxisPoints();
		FaberEngine glm::vec2	getZAxisPoints();
	};
}