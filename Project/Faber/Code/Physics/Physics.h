#pragma once
#include "../Config.h"

#include <memory>
#include <vector>
#include <algorithm>
#include <set>
#include <GLM/glm.hpp>

#include "PhysicObjects.h"
#include "PhysicBodyDef.h"
#include "BroadPhase.h"

namespace Faber::Physics{
	typedef std::weak_ptr<Faber::Physics::cPhysicBody> BodyWatch;
	typedef std::pair<BodyWatch, BodyWatch> CollisionPair;

		class cWorld {
		private:
			std::set<std::shared_ptr<cPhysicBody>>							BodiesOnScene;

			std::unique_ptr<cCollisionAlgorithm>							CollisionAlgorithm;

			FaberEngine														cWorld(const cWorld &) {}

			FaberEngine void												handleCollision();
		public: 
			FaberEngine														cWorld() {}
			/**
			* Destruktor usuwa wszystkie obiekty ze sceny.
			*/
			FaberEngine														~cWorld();
			/**
			* Funkcja tworzy nowy obiekt fizyczny na podstawie definicji danej w argumencie, a nastepnie
			* zwraca na niego wskaznik
			*/
			FaberEngine BodyWatch											createBody(sPhysicBodyDef & Def);
			/**
			* Funkcja usuwa dany na argumencie obiekt fizyczny i ustawia wskaznik na NULL.
			*/
			FaberEngine bool												deleteBody(BodyWatch);
			/**
			* Funkcja odswieza pary kolizji i zajmuje sie ruchem obiektow.
			*/
			FaberEngine void												updatePhysics();
			/**
			* Funkcja ustawia algorytm ktory bedzie uzywany przy broad phase collision.
			*/
			FaberEngine void useSAP();
			FaberEngine void useQuadTree();
			
		};
		/**
		* Funkcja wywolywana przy wlaczaniu modulu fizyki.
		*/
		FaberEngine void			initialize();

		/**
		* Funkcja wywolywana przy wylaczaniu modulu fizyki.
		*/
		FaberEngine void			terminate();
}