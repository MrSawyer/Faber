#include "SAP.h"
#include "Physics.h"
namespace Faber::Physics {

		static cSAP SAP_ALGORITHM;
		cCollisionAlgorithm & getSAP() {
			return SAP_ALGORITHM;
		}

		sSAPBox::sSAPBox() {}

		sSAPBox::~sSAPBox() {}

		std::set<std::pair<cPhysicBody *, cPhysicBody*>> & cSAP::getCollisionPairs() {
			return CollisionPairs;
		}

		bool operator < (const sSAPBox & L, const sSAPBox & R) {
			return L.Max[0].Value < R.Max[0].Value ? true : false;
		}

		sSAPBox*	cSAP::calculatesSAPBox(cPhysicBody & Obj) {
			sSAPBox *NewSapBox = new sSAPBox;
			NewSapBox->Max[0].Value = Obj.getAABB().XMax;
			NewSapBox->Max[1].Value = Obj.getAABB().YMax;
			NewSapBox->Max[2].Value = Obj.getAABB().ZMax;

			NewSapBox->Min[0].Value = Obj.getAABB().XMin;
			NewSapBox->Min[1].Value = Obj.getAABB().YMin;
			NewSapBox->Min[2].Value = Obj.getAABB().ZMin;

			NewSapBox->Max[0].End = true;
			NewSapBox->Max[1].End = true;
			NewSapBox->Max[2].End = true;

			NewSapBox->Min[0].End = false;
			NewSapBox->Min[1].End = false;
			NewSapBox->Min[2].End = false;

			NewSapBox->Max[0].Owner = NewSapBox;
			NewSapBox->Max[1].Owner = NewSapBox;
			NewSapBox->Max[2].Owner = NewSapBox;

			NewSapBox->Min[0].Owner = NewSapBox;
			NewSapBox->Min[1].Owner = NewSapBox;
			NewSapBox->Min[2].Owner = NewSapBox;


			NewSapBox->Owner = &Obj;
			return NewSapBox;
		}

		bool cSAP::checkOverlaping(sSAPBox * A, sSAPBox * B, eAXIS Axis) {
			unsigned int AVal[2];
			unsigned int BVal[2];
			
			switch (Axis) {
				case X_AXIS: 
					AVal[0] = A->Min[0].Iterator; 
					AVal[1] = A->Max[0].Iterator;
					BVal[0] = B->Min[0].Iterator; 
					BVal[1] = B->Max[0].Iterator;
					break; 
				
				case Y_AXIS : 
					AVal[0] = A->Min[1].Iterator; 
					AVal[1] = A->Max[1].Iterator;
					BVal[0] = B->Min[1].Iterator; 
					BVal[1] = B->Max[1].Iterator;
					break; 
				
				case Z_AXIS : 
					AVal[0] = A->Min[2].Iterator; 
					AVal[1] = A->Max[2].Iterator;
					BVal[0] = B->Min[2].Iterator; 
					BVal[1] = B->Max[2].Iterator;
					break;
			}

			if (AVal[0] < BVal[0] && BVal[0] < AVal[1])return true;
			if (AVal[0] < BVal[1] && BVal[1] < AVal[1])return true;
			
			if (BVal[0] < AVal[0] && AVal[0] < BVal[1])return true;
			if (BVal[0] < AVal[1] && AVal[1] < BVal[1])return true;
			
			return false;
		}

		void cSAP::handleCollisionPairs(sSAPBox * AABB) {
			for (auto CollisionPair : AABB->InvolvedCollision) {
				CollisionPairs.erase(CollisionPair);
			}

			AABB->InvolvedCollision.clear();

			std::set<sSAPBox *> FindedCollisionPairs;

			for (unsigned int i = AABB->Min[0].Iterator + 1; i < AABB->Max[0].Iterator; ++i) {

				sSAPBox * SecondBox = XAxisSAP[i]->Owner;
				std::pair<cPhysicBody *, cPhysicBody *> ToCheckIfAlreadyExist(SecondBox->Owner, AABB->Owner);
				if (CollisionPairs.find(ToCheckIfAlreadyExist) != CollisionPairs.end())continue; // already exists
				
				if (checkOverlaping(AABB, SecondBox, X_AXIS)) {
					if (checkOverlaping(AABB, SecondBox, Z_AXIS)) {
						if (checkOverlaping(AABB, SecondBox, Y_AXIS)) {
							FindedCollisionPairs.insert(SecondBox);
						}else continue; // Y CHECK
					}else continue; // Z CHECK
				}else continue; // X CHECK
			}

			for (auto NewCollision : FindedCollisionPairs) {
				std::set<std::pair<cPhysicBody *, cPhysicBody*>>::iterator NewCollisionIterator = CollisionPairs.insert(std::pair<cPhysicBody *, cPhysicBody*>(AABB->Owner, NewCollision->Owner)).first;
				AABB->InvolvedCollision.push_back(NewCollisionIterator);
				
			}
		}

		void cSAP::updateIterators(unsigned int From, std::vector<SAPEndPoint *> &Vec) {
			for (unsigned int i = From; i < (unsigned int)Vec.size(); ++i) {
				Vec[i]->Iterator = i;
			}
		}

		bool cSAP::moveObjectInAxis(std::vector<SAPEndPoint *> & Vec, SAPEndPoint & Point, unsigned int Position) {
			if (Point.Iterator == Position)return false;

			unsigned int OldIterator = Point.Iterator;
			Vec.erase(Vec.begin() + Point.Iterator);
			Point.Iterator = Position;
			Vec.insert(Vec.begin() + Position, &Point);

			unsigned int RangeMin = std::min(OldIterator, Position);
			unsigned int RangeMax = std::max(OldIterator, Position);

			for (unsigned int i = RangeMin; i <= RangeMax; ++i) {
				Vec[i]->Iterator = i;
			}
			return true;
		}

		void cSAP::findProperPlace(std::vector<SAPEndPoint *> & AxisList, sSAPBox * AABB, eAXIS Axis, bool & TO_CHANGE_COLLISION_PAIRS) {
			short ID = 0;
			switch (Axis) {
				default:		ID = 0; break;
				case X_AXIS:	ID = 0; break;
				case Y_AXIS:	ID = 1; break;
				case Z_AXIS:	ID = 2; break;
			}

			for (unsigned int i = 0; i < (unsigned int)AxisList.size(); ++i) {
				if (AxisList[i]->Value >= AABB->Max[ID].Value) {
					if (moveObjectInAxis(AxisList, AABB->Max[ID], i))TO_CHANGE_COLLISION_PAIRS = true;
					break;
				}
			}

			for (unsigned int i = 0; i < (unsigned int)AxisList.size(); ++i) {
				if (AxisList[i]->Value >= AABB->Min[ID].Value) {
					if (moveObjectInAxis(AxisList, AABB->Min[ID], i))TO_CHANGE_COLLISION_PAIRS = true;
					break;
				}
			}
		}

		void cSAP::updateObject(cPhysicBody & Obj) {
			bool TO_CHANGE_COLLISION_PAIRS = false;
			sSAPBox * AABB = Objects[&Obj];

			AABB->Max[0].Value = Obj.getAABB().XMax;
			AABB->Max[1].Value = Obj.getAABB().YMax;
			AABB->Max[2].Value = Obj.getAABB().ZMax;

			AABB->Min[0].Value = Obj.getAABB().XMin;
			AABB->Min[1].Value = Obj.getAABB().YMin;
			AABB->Min[2].Value = Obj.getAABB().ZMin;

			
			findProperPlace(XAxisSAP, AABB, X_AXIS, TO_CHANGE_COLLISION_PAIRS);
			findProperPlace(YAxisSAP, AABB, Y_AXIS, TO_CHANGE_COLLISION_PAIRS);
			findProperPlace(ZAxisSAP, AABB, Z_AXIS, TO_CHANGE_COLLISION_PAIRS);

			if (TO_CHANGE_COLLISION_PAIRS)handleCollisionPairs(AABB);
		}

		void cSAP::addObject(cPhysicBody & Obj) {
			sSAPBox * AABB = calculatesSAPBox(Obj);
			
			Objects[&Obj] = AABB;

			XAxisSAP.push_back(&AABB->Min[0]);
			XAxisSAP.push_back(&AABB->Max[0]);

			YAxisSAP.push_back(&AABB->Min[1]);
			YAxisSAP.push_back(&AABB->Max[1]);

			ZAxisSAP.push_back(&AABB->Min[2]);
			ZAxisSAP.push_back(&AABB->Max[2]);

			updateIterators(0, XAxisSAP);
			updateIterators(0, YAxisSAP);
			updateIterators(0, ZAxisSAP);

			updateObject(Obj);
		}

		void cSAP::removeObject(cPhysicBody & Obj) {
			sSAPBox * AABB = Objects[&Obj];

			for (unsigned int i = 0; i < AABB->InvolvedCollision.size(); ++i) {
				CollisionPairs.erase(AABB->InvolvedCollision[i]);
			}
			AABB->InvolvedCollision.clear();

			XAxisSAP.erase(XAxisSAP.begin() + AABB->Min[0].Iterator);
			updateIterators(AABB->Min[0].Iterator, XAxisSAP);
			XAxisSAP.erase(XAxisSAP.begin() + AABB->Max[0].Iterator);
			updateIterators(AABB->Max[0].Iterator, XAxisSAP);

			YAxisSAP.erase(YAxisSAP.begin() + AABB->Min[1].Iterator);
			updateIterators(AABB->Min[1].Iterator, YAxisSAP);
			YAxisSAP.erase(YAxisSAP.begin() + AABB->Max[1].Iterator);
			updateIterators(AABB->Max[1].Iterator, YAxisSAP);

			ZAxisSAP.erase(ZAxisSAP.begin() + AABB->Min[2].Iterator);
			updateIterators(AABB->Min[2].Iterator, ZAxisSAP);
			ZAxisSAP.erase(ZAxisSAP.begin() + AABB->Max[2].Iterator);
			updateIterators(AABB->Max[2].Iterator, YAxisSAP);

			delete AABB;
		}
}

