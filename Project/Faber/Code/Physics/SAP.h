#pragma once

#include "../Config.h"

#include <vector>
#include <set>
#include <map>
#include <algorithm>

#include "BroadPhase.h"

namespace Faber::Physics{
		/**
		* Typ wyliczeniowy opisujacy osie w systemie kartezjanskim.
		*/
		enum eAXIS {
			X_AXIS, Y_AXIS, Z_AXIS
		};

		struct sSAPBox;
		/**
		* Struktura pozwalajaca zapisac maxymalna oraz minimalna wartosc dla danego szescianu (Owner),
		* rzutowanego na poszczegolne osie systemu kartezjanskiego.
		*/
		struct SAPEndPoint {
			bool			End;// START = 0, END = 1;
			float			Value;
			sSAPBox *		Owner;
			unsigned int	Iterator;
		};

		/**
		* Struktura Szescianu opisujacego dany obiekt fizyczny (Owner).
		* z mozliwoscia rzutowania na poszczegolne osie systemu kartezjanskiego.
		* Informacja o rzutowaniu zapisuje sie w tablicach SAPEndPoint.
		*/
		struct sSAPBox {
			sSAPBox();
			~sSAPBox();
			SAPEndPoint																Min[3]; // Min EndPoint over X,Y,Z
			SAPEndPoint																Max[3]; // Max EndPoint over X,Y,Z
			std::vector<std::set<std::pair<cPhysicBody *, cPhysicBody*>>::iterator>	InvolvedCollision;
			cPhysicBody *															Owner;
		};

		/**
		* Klasa algorytmu Sweep and Prune
		*/
		class cSAP
		: public cCollisionAlgorithm{
			std::set<std::pair<cPhysicBody *, cPhysicBody*>>						CollisionPairs;
			std::map< cPhysicBody * , sSAPBox *>									Objects;

			/**
			* Osie ukladu kartezjanskiego zawierajace posortowane obiekty typu SAPEndPoint *.
			*/
			std::vector<SAPEndPoint *>												XAxisSAP;
			std::vector<SAPEndPoint *>												YAxisSAP;
			std::vector<SAPEndPoint *>												ZAxisSAP;

			/**
			* Zmienia pozycje zadanego punktu na osiach uaktualniajac przy okazji iteratory reszty obiektow.
			*/
			FaberEngine bool														moveObjectInAxis(std::vector<SAPEndPoint *> & AxisList, SAPEndPoint & Point, unsigned int Position);
			/**
			* Sprawdza czy dwa boxy zachodza na siebie na zadanej osi.
			*/
			FaberEngine bool														checkOverlaping(sSAPBox * A, sSAPBox * B, eAXIS Axis);
			/**
			* Obsluguje dodawania oraz usuwanie nowych i starych kolizji z setu CollisionPairs dla danego obiektu A.
			*/
			FaberEngine void														handleCollisionPairs(sSAPBox * A);
			/**
			* Uaktualnia iteratory wszystkim obiektom od zadanej pozycji FROM na zadanej osi.
			*/
			FaberEngine void														updateIterators(unsigned int From, std::vector<SAPEndPoint *> &Vec);
			/**
			* Przemieszcza zadany box w odpowiednie dla niego miejsce posortowanej osi.
			*/
			FaberEngine void														findProperPlace(std::vector<SAPEndPoint *> & AxisList, sSAPBox * AABB, eAXIS Axis, bool & TO_CHANGE_COLLISION_PAIRS);
			FaberEngine sSAPBox*													calculatesSAPBox(cPhysicBody & Obj);
		public:
			FaberEngine virtual void												addObject(cPhysicBody & Obj);
			FaberEngine virtual void												removeObject(cPhysicBody & Obj);
			FaberEngine	virtual void												updateObject(cPhysicBody & Obj);
			FaberEngine virtual std::set<std::pair<cPhysicBody *, cPhysicBody*>>&	getCollisionPairs();		
		};

}

