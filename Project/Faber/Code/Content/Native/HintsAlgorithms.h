#pragma once

#include <Windows.h>
#include <GLM/glm.hpp>
#include <vector>
#include "../../Config.h"

namespace Faber
{
	namespace Content
	{
		namespace Native
		{
			struct sFace {
				glm::vec3 A, B, C;
				sFace() {}
				sFace(glm::vec3 v1, glm::vec3 v2, glm::vec3 v3) { A = v1; B = v2; C = v3; }
			};

			FaberEngine std::vector<sFace> convertToFaces(std::vector<glm::vec3> Points);
		}
	}
}