#include "HintsAlgorithms.h"

namespace Faber
{
	namespace Content
	{
		namespace Native
		{
			std::vector<sFace> convertToFaces(std::vector<glm::vec3> Points) {
				std::vector<sFace> Faces;

				for (unsigned int i = 0; i < Points.size(); i++) {
					for (unsigned ii = i + 1; ii < Points.size(); ii++) {
						for (unsigned int iii = ii + 1; iii < Points.size(); iii++) {
							glm::vec3 AB = glm::vec3(Points[ii].x - Points[i].x, Points[ii].y - Points[i].y, Points[ii].z - Points[i].z);
							glm::vec3 CB = glm::vec3(Points[ii].x - Points[iii].x, Points[ii].y - Points[iii].y, Points[ii].z - Points[iii].z);
							glm::vec3 VecNormalized = glm::normalize(glm::cross(AB, CB));

							float x0 = Points[i].x;
							float y0 = Points[i].y;
							float z0 = Points[i].z;

							bool Greater = false;
							bool FirstShot = true;
							bool IsWall = true;
							for (unsigned int v = 0; v < Points.size(); v++) {
								if (v == i || v == ii || v == iii)continue;
								if (FirstShot) {
									if (
										VecNormalized.x * (Points[v].x - x0) +
										VecNormalized.y * (Points[v].y - y0) +
										VecNormalized.z * (Points[v].z - z0) > 0
										) {
										Greater = true;
										FirstShot = false;
									}

									if (
										VecNormalized.x * (Points[v].x - x0) +
										VecNormalized.y * (Points[v].y - y0) +
										VecNormalized.z * (Points[v].z - z0) < 0
										) {
										Greater = false;
										FirstShot = false;
									}
									continue;
								}
								else {
									if (
										VecNormalized.x * (Points[v].x - x0) +
										VecNormalized.y * (Points[v].y - y0) +
										VecNormalized.z * (Points[v].z - z0) > 0
										&&
										!Greater
										) {
										IsWall = false;
										break;
									}

									if (
										VecNormalized.x * (Points[v].x - x0) +
										VecNormalized.y * (Points[v].y - y0) +
										VecNormalized.z * (Points[v].z - z0) < 0
										&&
										Greater
										) {
										IsWall = false;
										break;
									}
								}

							}
							if (IsWall)Faces.push_back(sFace(Points[i], Points[ii], Points[iii]));

						}
					}
				}
				return Faces;
			}
		}
	}
}