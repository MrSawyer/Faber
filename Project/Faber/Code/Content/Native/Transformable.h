#pragma once

#include <Windows.h>
#include <GL/glew.h>
#include <GLM/glm.hpp>
#include <GLM/gtc/matrix_transform.hpp>
#include <GLM/gtc/type_ptr.hpp>
#include <GLM/gtx/quaternion.hpp>
#include "../../Config.h"

namespace Faber
{
	namespace Content
	{
		namespace Native
		{
			class cTransformable
			{
			private:
				glm::mat4 *MMatrix;
				glm::mat3 *InvTransMMatrix;
				glm::vec3 *Position;
				glm::quat *Rotation;
				glm::vec3 *Scale;

				bool OWN_VARIABLE[3];
				bool CALCULATE_MATRIX;
				bool UPDATED;

			protected:
				FaberEngine cTransformable();
				FaberEngine ~cTransformable();

				FaberEngine bool initialize();
				FaberEngine void terminate();

				FaberEngine void callMatrixCalculations();

			public:
				FaberEngine bool isUpdated();

				FaberEngine void calculateMatrix();

				FaberEngine void setPosition(glm::vec3 Position);
				FaberEngine void setPosition(float PositionX, float PositionY, float PositionZ);
				FaberEngine void setPositionX(float PositionX);
				FaberEngine void setPositionY(float PositionY);
				FaberEngine void setPositionZ(float PositionZ);

				FaberEngine void setRotation(glm::quat Rotation);
				/* Dodaj tu cos tam co chcesz :P --^ */

				FaberEngine void setScale(glm::vec3 Scale);
				FaberEngine void setScale(float ScaleX, float ScaleY, float ScaleZ);
				FaberEngine void setScaleX(float ScaleX);
				FaberEngine void setScaleY(float ScaleY);
				FaberEngine void setScaleZ(float ScaleZ);

				FaberEngine glm::mat4 getMatrix();
				FaberEngine glm::mat3 getInvTransMatrix();
				FaberEngine glm::vec3 getPosition();
				FaberEngine glm::quat getRotation();
				FaberEngine glm::vec3 getScale();

				FaberEngine bool setPositionPtr(glm::vec3 *Position);
				FaberEngine bool setRotationPtr(glm::quat *Rotation);
				FaberEngine bool setScalePtr(glm::vec3 *Scale);

				FaberEngine glm::vec3 *getPositionPtr();
				FaberEngine glm::quat *getRotationPtr();
				FaberEngine glm::vec3 *getScalePtr();
			};
		}
	}
}