#include "Mesh.h"
#include "../../Graphics/Shaders.h"

namespace Faber
{
	namespace Content
	{
		namespace Native
		{
			cMesh::cMesh()
			{
				this->VAO = NULL;
				this->VBO = NULL;
				this->EBO = NULL;
				this->RenderMode = Graphics::RENDER_MODE_TRIANGLES;
				this->IndicesCount = 0;
				this->RawDataPtr = NULL;
				this->OwnRawData = false;
			}

			cMesh::~cMesh() {}

			bool cMesh::create(sMeshRawData *RawDataPtr, cMaterial *Material, Graphics::eBufferTypes VertexBufferType, Graphics::eBufferTypes IndexBufferType)
			{
				if (this->cTransformable::initialize() == false)
				{
					return false;
				}

				this->IndexBufferT = IndexBufferType;
				this->VertexBufferT = VertexBufferType;

				this->RawDataPtr = RawDataPtr;
				if (this->RawDataPtr == NULL)
				{
					return false;
				}
				this->OwnRawData = false;

				this->Material = Material;
				if (this->Material == NULL)
				{
					return false;
				}

				this->IndicesCount = this->RawDataPtr->Indices.size();

				unsigned int MaxID = 0;
				for (unsigned int i = 0; i < this->RawDataPtr->Indices.size(); i++)
				{
					if (this->RawDataPtr->Indices[i] > MaxID)
						MaxID = this->RawDataPtr->Indices[i];
				}

				if (this->RawDataPtr->Vertices.size() - 1 != MaxID)
				{
					return false;
				}

				MaxID = this->RawDataPtr->Vertices.size() - 1;

				if (this->RawDataPtr->Normals.size() - 1 != MaxID)
				{
					this->RawDataPtr->Normals.resize(MaxID + 1);
				}

				if (this->RawDataPtr->UVs.size() - 1 != MaxID)
				{
					this->RawDataPtr->UVs.resize(MaxID + 1);
				}

				glGenVertexArrays(1, &this->VAO);
				if (this->VAO == NULL)
				{
					return false;
				}
				glBindVertexArray(this->VAO);

				glGenBuffers(1, &this->VBO);
				if (this->VBO == NULL)
				{
					return false;
				}
				glBindBuffer(GL_ARRAY_BUFFER, this->VBO);
				glBufferData(GL_ARRAY_BUFFER, this->RawDataPtr->Vertices.size() * sizeof(glm::vec3) + this->RawDataPtr->Normals.size() * sizeof(glm::vec3) + this->RawDataPtr->UVs.size() * sizeof(glm::vec2), (void*)0, this->VertexBufferT);

				glBufferSubData(GL_ARRAY_BUFFER, 0, this->RawDataPtr->Vertices.size() * sizeof(glm::vec3), (void*)&this->RawDataPtr->Vertices[0]);
				glBufferSubData(GL_ARRAY_BUFFER, this->RawDataPtr->Vertices.size() * sizeof(glm::vec3), this->RawDataPtr->Normals.size() * sizeof(glm::vec3), (void*)&this->RawDataPtr->Normals[0]);
				glBufferSubData(GL_ARRAY_BUFFER, this->RawDataPtr->Vertices.size() * sizeof(glm::vec3) + this->RawDataPtr->Normals.size() * sizeof(glm::vec3), this->RawDataPtr->UVs.size() * sizeof(glm::vec2), (void*)&this->RawDataPtr->UVs[0]);

				glGenBuffers(1, &this->EBO);
				if (this->EBO == NULL)
				{
					return false;
				}
				glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, this->EBO);
				glBufferData(GL_ELEMENT_ARRAY_BUFFER, this->RawDataPtr->Indices.size() * sizeof(GLuint), (void*)&this->RawDataPtr->Indices[0], this->IndexBufferT);

				glEnableVertexAttribArray(0);
				glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(GLfloat), (void*)0);

				glEnableVertexAttribArray(1);
				glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(GLfloat), (void*)(this->RawDataPtr->Vertices.size() * sizeof(glm::vec3)));

				glEnableVertexAttribArray(2);
				glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 2 * sizeof(GLfloat), (void*)(this->RawDataPtr->Vertices.size() * sizeof(glm::vec3) + this->RawDataPtr->Normals.size() * sizeof(glm::vec3)));

				glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, NULL);
				glBindBuffer(GL_ARRAY_BUFFER, NULL);

				glBindVertexArray(NULL);

				return true;
			}

			bool cMesh::create(std::vector <GLuint> *Indices, std::vector <glm::vec3> *Vertices, std::vector <glm::vec3> *Normals, std::vector <glm::vec2> *UVs, cMaterial *Material, Graphics::eBufferTypes VertexBufferType, Graphics::eBufferTypes IndexBufferType)
			{
				if (this->cTransformable::initialize() == false)
				{
					return false;
				}

				this->IndexBufferT = IndexBufferType;
				this->VertexBufferT = VertexBufferType;

				this->Material = Material;
				if (this->Material == NULL)
				{
					return false;
				}

				this->RawDataPtr = new sMeshRawData();
				if (this->RawDataPtr == NULL)
				{
					return false;
				}
				ZeroMemory(this->RawDataPtr, sizeof(sMeshRawData));
				this->OwnRawData = true;

				if (Indices != NULL)
				{
					this->RawDataPtr->Indices = *Indices;
					this->IndicesCount = this->RawDataPtr->Indices.size();
				}
				else
				{
					return false;
				}

				unsigned int MaxID = 0;
				for (unsigned int i = 0; i < this->RawDataPtr->Indices.size(); i++)
				{
					if (this->RawDataPtr->Indices[i] > MaxID)
						MaxID = this->RawDataPtr->Indices[i];
				}

				if (Vertices != NULL)
				{
					if (Vertices->size() - 1 != MaxID)
					{
						return false;
					}

					this->RawDataPtr->Vertices = *Vertices;
					MaxID = this->RawDataPtr->Vertices.size() - 1;
				}
				else
				{
					return false;
				}

				if (Normals != NULL)
				{
					this->RawDataPtr->Normals = *Normals;
					if (this->RawDataPtr->Normals.size() - 1 != MaxID)
					{
						this->RawDataPtr->Normals.resize(MaxID + 1);
					}
				}
				else
				{
					this->RawDataPtr->Normals.resize(MaxID + 1);
				}

				if (UVs != NULL)
				{
					this->RawDataPtr->UVs = *UVs;
					if (this->RawDataPtr->UVs.size() - 1 != MaxID)
					{
						this->RawDataPtr->UVs.resize(MaxID + 1);
					}
				}
				else
				{
					this->RawDataPtr->UVs.resize(MaxID + 1);
				}

				glGenVertexArrays(1, &this->VAO);
				if (this->VAO == NULL)
				{
					return false;
				}
				glBindVertexArray(this->VAO);

				glGenBuffers(1, &this->VBO);
				if (this->VBO == NULL)
				{
					return false;
				}
				glBindBuffer(GL_ARRAY_BUFFER, this->VBO);
				glBufferData(GL_ARRAY_BUFFER, this->RawDataPtr->Vertices.size() * sizeof(glm::vec3) + this->RawDataPtr->Normals.size() * sizeof(glm::vec3) + this->RawDataPtr->UVs.size() * sizeof(glm::vec2), (void*)0, this->VertexBufferT);

				glBufferSubData(GL_ARRAY_BUFFER, 0, this->RawDataPtr->Vertices.size() * sizeof(glm::vec3), (void*)&this->RawDataPtr->Vertices[0]);
				glBufferSubData(GL_ARRAY_BUFFER, this->RawDataPtr->Vertices.size() * sizeof(glm::vec3), this->RawDataPtr->Normals.size() * sizeof(glm::vec3), (void*)&this->RawDataPtr->Normals[0]);
				glBufferSubData(GL_ARRAY_BUFFER, this->RawDataPtr->Vertices.size() * sizeof(glm::vec3) + this->RawDataPtr->Normals.size() * sizeof(glm::vec3), this->RawDataPtr->UVs.size() * sizeof(glm::vec2), (void*)&this->RawDataPtr->UVs[0]);

				glGenBuffers(1, &this->EBO);
				if (this->EBO == NULL)
				{
					return false;
				}
				glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, this->EBO);
				glBufferData(GL_ELEMENT_ARRAY_BUFFER, this->RawDataPtr->Indices.size() * sizeof(GLuint), (void*)&this->RawDataPtr->Indices[0], this->IndexBufferT);

				glEnableVertexAttribArray(0);
				glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(GLfloat), (void*)0);

				glEnableVertexAttribArray(1);
				glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(GLfloat), (void*)(this->RawDataPtr->Vertices.size() * sizeof(glm::vec3)));

				glEnableVertexAttribArray(2);
				glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 2 * sizeof(GLfloat), (void*)(this->RawDataPtr->Vertices.size() * sizeof(glm::vec3) + this->RawDataPtr->Normals.size() * sizeof(glm::vec3)));

				glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, NULL);
				glBindBuffer(GL_ARRAY_BUFFER, NULL);

				glBindVertexArray(NULL);

				return true;
			}

			void cMesh::destroy()
			{
				this->cTransformable::terminate();

				if (this->OwnRawData == true)
				{
					this->RawDataPtr->Indices.clear();
					this->RawDataPtr->Vertices.clear();
					this->RawDataPtr->Normals.clear();
					this->RawDataPtr->UVs.clear();

					delete this->RawDataPtr;
					this->RawDataPtr = NULL;
					this->OwnRawData = false;
				}

				if (this->EBO != NULL)
				{
					glDeleteBuffers(1, &this->EBO);
					this->EBO = NULL;
				}

				if (this->VBO != NULL)
				{
					glDeleteBuffers(1, &this->VBO);
					this->VBO = NULL;
				}

				if (this->VAO != NULL)
				{
					glDeleteVertexArrays(1, &this->VAO);
					this->VAO = NULL;
				}
			}

			void cMesh::setRenderMode(Graphics::eRenderModes RenderMode)
			{
				this->RenderMode = RenderMode;
			}

			Graphics::eRenderModes cMesh::getRenderMode()
			{
				return this->RenderMode;
			}

			void cMesh::setMaterial(cMaterial *Material)
			{
				this->Material = Material;
			}

			cMaterial *cMesh::getMaterial()
			{
				return this->Material;
			}

			void cMesh::setupMaterial(Graphics::cShaders *Shaders)
			{
				this->Material->setupMaterial(Shaders);
			}

			void cMesh::render()
			{
				glBindVertexArray(this->VAO);
				glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, this->EBO);
				glDrawElements(this->RenderMode, this->IndicesCount, GL_UNSIGNED_INT, (void*)0);
				glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, NULL);
				glBindVertexArray(NULL);
			}
		}
	}
}