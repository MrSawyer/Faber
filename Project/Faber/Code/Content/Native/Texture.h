#pragma once

#include <string>
#include <Windows.h>
#include <GL/glew.h>
#include "../../Config.h"

namespace Faber
{
	namespace Content
	{
		namespace Native
		{
			class cTexture
			{
			private:
				GLuint		ID;
				int			Width, Height;
				int			Channels;
				std::string Path;

			public:
				FaberEngine cTexture();
				FaberEngine ~cTexture();
			};
		}
	}
}