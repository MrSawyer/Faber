#include "Transformable.h"

namespace Faber
{
	namespace Content
	{
		namespace Native
		{
			cTransformable::cTransformable()
			{
				this->MMatrix = NULL;
				this->InvTransMMatrix = NULL;
				this->Position = NULL;
				this->Rotation = NULL;
				this->Scale = NULL;
			}

			cTransformable::~cTransformable() {}

			bool cTransformable::initialize()
			{
				this->MMatrix = new glm::mat4();
				if (this->MMatrix == NULL)
				{
					return false;
				}

				this->InvTransMMatrix = new glm::mat3();
				if (this->InvTransMMatrix == NULL)
				{
					return false;
				}

				this->Position = new glm::vec3(0.0f);
				if (this->Position == NULL)
				{
					return false;
				}

				this->Rotation = new glm::quat();
				if (this->Rotation == NULL)
				{
					return false;
				}

				this->Scale = new glm::vec3(1.0f);
				if (this->Scale == NULL)
				{
					return false;
				}

				for (unsigned int i = 0; i < 3; i++)
				{
					this->OWN_VARIABLE[i] = true;
				}

				this->CALCULATE_MATRIX = true;

				return true;
			}

			void cTransformable::terminate()
			{
				if (this->MMatrix != NULL)
				{
					delete this->MMatrix;
					this->MMatrix = NULL;
				}

				if (this->InvTransMMatrix != NULL)
				{
					delete this->InvTransMMatrix;
					this->InvTransMMatrix = NULL;
				}

				if (this->Position != NULL && this->OWN_VARIABLE[0] == true)
				{
					delete this->Position;
					this->Position = NULL;
				}

				if (this->Rotation != NULL && this->OWN_VARIABLE[1] == true)
				{
					delete this->Rotation;
					this->Rotation = NULL;
				}

				if (this->Scale != NULL && this->OWN_VARIABLE[2] == true)
				{
					delete this->Scale;
					this->Scale = NULL;
				}
			}

			void cTransformable::callMatrixCalculations()
			{
				this->CALCULATE_MATRIX = true;
			}

			void cTransformable::calculateMatrix()
			{
				if (this->CALCULATE_MATRIX == true)
				{
					*this->MMatrix = glm::translate(glm::mat4(), *this->Position);
					*this->MMatrix = *this->MMatrix * glm::toMat4(*this->Rotation);
					*this->MMatrix = glm::scale(*this->MMatrix, *this->Scale);

					*this->InvTransMMatrix = glm::transpose(glm::inverse(glm::mat3(*this->MMatrix)));

					this->CALCULATE_MATRIX = false;
					UPDATED = true;
				}
			}

			bool cTransformable::isUpdated() {
				if (UPDATED) { UPDATED = false; return true; }
				return false;
			}

			void cTransformable::setPosition(glm::vec3 Position)
			{
				*this->Position = Position;
				this->callMatrixCalculations();
			}

			void cTransformable::setPosition(float PositionX, float PositionY, float PositionZ)
			{
				this->Position->x = PositionX;
				this->Position->y = PositionY;
				this->Position->z = PositionZ;
				this->callMatrixCalculations();
			}

			void cTransformable::setPositionX(float PositionX)
			{
				this->Position->x = PositionX;
				this->callMatrixCalculations();
			}

			void cTransformable::setPositionY(float PositionY)
			{
				this->Position->y = PositionY;
				this->callMatrixCalculations();
			}

			void cTransformable::setPositionZ(float PositionZ)
			{
				this->Position->z = PositionZ;
				this->callMatrixCalculations();
			}

			void cTransformable::setRotation(glm::quat Rotation)
			{
				*this->Rotation = Rotation;
				this->callMatrixCalculations();
			}

			void cTransformable::setScale(glm::vec3 Scale)
			{
				*this->Scale = Scale;
				this->callMatrixCalculations();
			}

			void cTransformable::setScale(float ScaleX, float ScaleY, float ScaleZ)
			{
				this->Scale->x = ScaleX;
				this->Scale->y = ScaleY;
				this->Scale->z = ScaleZ;
				this->callMatrixCalculations();
			}

			void cTransformable::setScaleX(float ScaleX)
			{
				this->Scale->x = ScaleX;
				this->callMatrixCalculations();
			}

			void cTransformable::setScaleY(float ScaleY)
			{
				this->Scale->y = ScaleY;
				this->callMatrixCalculations();
			}

			void cTransformable::setScaleZ(float ScaleZ)
			{
				this->Scale->z = ScaleZ;
				this->callMatrixCalculations();
			}

			glm::mat4 cTransformable::getMatrix()
			{
				return *this->MMatrix;
			}

			glm::mat3 cTransformable::getInvTransMatrix()
			{
				return *this->InvTransMMatrix;
			}

			glm::vec3 cTransformable::getPosition()
			{
				return *this->Position;
			}

			glm::quat cTransformable::getRotation()
			{
				return *this->Rotation;
			}

			glm::vec3 cTransformable::getScale()
			{
				return *this->Scale;
			}

			bool cTransformable::setPositionPtr(glm::vec3 *Position)
			{
				if (Position == NULL)
				{
					if (this->OWN_VARIABLE[0] == true)
					{
						return false;
					}
					else
					{
						this->Position = NULL;
						this->Position = new glm::vec3(0.0f);
						if (this->Position == NULL)
						{
							return false;
						}
						this->OWN_VARIABLE[0] = true;
					}
				}
				else
				{
					if (this->OWN_VARIABLE[0] == true)
					{
						if (this->Position != NULL)
						{
							delete this->Position;
							this->Position = NULL;
						}
						this->Position = Position;
						this->OWN_VARIABLE[0] = false;
					}
					else
					{
						this->Position = NULL;
						this->Position = Position;
					}
				}

				return true;
			}

			bool cTransformable::setRotationPtr(glm::quat *Rotation)
			{
				if (Rotation == NULL)
				{
					if (this->OWN_VARIABLE[1] == true)
					{
						return false;
					}
					else
					{
						this->Rotation = NULL;
						this->Rotation = new glm::quat();
						if (this->Rotation == NULL)
						{
							return false;
						}
						this->OWN_VARIABLE[1] = true;
					}
				}
				else
				{
					if (this->OWN_VARIABLE[1] == true)
					{
						if (this->Rotation != NULL)
						{
							delete this->Rotation;
							this->Rotation = NULL;
						}
						this->Rotation = Rotation;
						this->OWN_VARIABLE[1] = false;
					}
					else
					{
						this->Rotation = NULL;
						this->Rotation = Rotation;
					}
				}

				return true;
			}

			bool cTransformable::setScalePtr(glm::vec3 *Scale)
			{
				if (Scale == NULL)
				{
					if (this->OWN_VARIABLE[2] == true)
					{
						return false;
					}
					else
					{
						this->Scale = NULL;
						this->Scale = new glm::vec3(1.0f);
						if (this->Scale == NULL)
						{
							return false;
						}
						this->OWN_VARIABLE[2] = true;
					}
				}
				else
				{
					if (this->OWN_VARIABLE[2] == true)
					{
						if (this->Scale != NULL)
						{
							delete this->Scale;
							this->Scale = NULL;
						}
						this->Scale = Scale;
						this->OWN_VARIABLE[2] = false;
					}
					else
					{
						this->Scale = NULL;
						this->Scale = Scale;
					}
				}

				return true;
			}

			glm::vec3 *cTransformable::getPositionPtr()
			{
				return this->Position;
			}

			glm::quat *cTransformable::getRotationPtr()
			{
				return this->Rotation;
			}

			glm::vec3 *cTransformable::getScalePtr()
			{
				return this->Scale;
			}
		}
	}
}