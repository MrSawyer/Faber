#pragma once

#include <Windows.h>
#include <GL/glew.h>
#include <GLM/glm.hpp>
#include <vector>
#include "Transformable.h"
#include "../Scene/Material.h"
#include "../../Graphics/RenderTypes.h"
#include "../../Config.h"

namespace Faber
{
	namespace Content
	{
		namespace Native
		{
			struct sMeshRawData
			{
				std::string				Name;
				std::vector <GLuint>	Indices;
				std::vector <glm::vec3> Vertices;
				std::vector <glm::vec3> Normals;
				std::vector <glm::vec2> UVs;
			};

			class cMesh : public cTransformable
			{
			private:
				GLuint VAO, EBO, VBO;
				unsigned int IndicesCount;

			private:
				Graphics::eRenderModes	RenderMode;
				Graphics::eBufferTypes	IndexBufferT;
				Graphics::eBufferTypes	VertexBufferT;

			private:
				sMeshRawData	*RawDataPtr;
				bool			OwnRawData;

			private:
				cMaterial	*Material;

			public:
				FaberEngine cMesh();
				FaberEngine ~cMesh();

				FaberEngine bool create(sMeshRawData *RawDataPtr, cMaterial *Material, Graphics::eBufferTypes VertexBufferType = Graphics::BUFFER_TYPE_UNMODIFIABLE, Graphics::eBufferTypes IndexBufferType = Graphics::BUFFER_TYPE_UNMODIFIABLE);
				FaberEngine bool create(std::vector <GLuint> *Indices, std::vector <glm::vec3> *Vertices, std::vector <glm::vec3> *Normals, std::vector <glm::vec2> *UVs, cMaterial *Material, Graphics::eBufferTypes VertexBufferType = Graphics::BUFFER_TYPE_UNMODIFIABLE, Graphics::eBufferTypes IndexBufferType = Graphics::BUFFER_TYPE_UNMODIFIABLE);
				FaberEngine void destroy();

				FaberEngine void					setRenderMode(Graphics::eRenderModes RenderMode);
				FaberEngine Graphics::eRenderModes	getRenderMode();

				FaberEngine void					setMaterial(cMaterial *Material);
				FaberEngine cMaterial				*getMaterial();

				FaberEngine void setupMaterial(Graphics::cShaders *Shaders);
				FaberEngine void render();
			};
		}
	}
}