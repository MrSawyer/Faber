#include "DependentContent.h"

namespace Faber
{
	namespace Content
	{
		namespace Dependent
		{
			cDependentContent::cDependentContent() {}

			cDependentContent::~cDependentContent() {}

			bool cDependentContent::createMesh(void **Mesh)
			{
				if (Mesh == NULL)
				{
					return false;
				}

				if (*Mesh == NULL)
				{
					*Mesh = new Native::cMesh();

					if (*Mesh == NULL)
					{
						return false;
					}
				}

				std::pair <std::set <Native::cMesh*>::iterator, bool> Result = this->Meshes.emplace((Native::cMesh*)*Mesh);

				if (Result.second == false)
				{
					return false;
				}

				if (Result.first == this->Meshes.end())
				{
					return false;
				}

				return true;
			}

			bool cDependentContent::createMesh(void **Mesh, Native::sMeshRawData *RawDataPtr, cMaterial *Material, Graphics::eBufferTypes VertexBufferType, Graphics::eBufferTypes IndexBufferType)
			{
				if (Mesh == NULL)
				{
					return false;
				}

				if (*Mesh == NULL)
				{
					*Mesh = new Native::cMesh();

					if (*Mesh == NULL)
					{
						return false;
					}
				}

				if (((Native::cMesh*)*Mesh)->create(RawDataPtr, Material, VertexBufferType, IndexBufferType) == false)
				{
					((Native::cMesh*)*Mesh)->destroy();
					delete *Mesh;
					*Mesh = NULL;
					return false;
				}

				std::pair <std::set <Native::cMesh*>::iterator, bool> Result = this->Meshes.emplace((Native::cMesh*)*Mesh);

				if (Result.second == false)
				{
					return false;
				}

				if (Result.first == this->Meshes.end())
				{
					return false;
				}

				return true;
			}

			bool cDependentContent::createMesh(void **Mesh, std::vector <GLuint> *Indices, std::vector <glm::vec3> *Vertices, std::vector <glm::vec3> *Normals, std::vector <glm::vec2> *UVs, cMaterial *Material, Graphics::eBufferTypes VertexBufferType, Graphics::eBufferTypes IndexBufferType)
			{
				if (Mesh == NULL)
				{
					return false;
				}

				if (*Mesh == NULL)
				{
					*Mesh = new Native::cMesh();

					if (*Mesh == NULL)
					{
						return false;
					}
				}

				if (((Native::cMesh*)*Mesh)->create(Indices, Vertices, Normals, UVs, Material, VertexBufferType, IndexBufferType))
				{
					((Native::cMesh*)*Mesh)->destroy();
					delete *Mesh;
					*Mesh = NULL;
					return false;
				}

				std::pair <std::set <Native::cMesh*>::iterator, bool> Result = this->Meshes.emplace((Native::cMesh*)*Mesh);

				if (Result.second == false)
				{
					return false;
				}

				if (Result.first == this->Meshes.end())
				{
					return false;
				}

				return true;
			}

			bool cDependentContent::destroyMesh(void **Mesh)
			{
				if (Mesh == NULL)
				{
					return false;
				}

				if (*Mesh == NULL)
				{
					return false;
				}

				std::set <Native::cMesh*>::iterator it = this->Meshes.find((Native::cMesh*)*Mesh);

				if (it == this->Meshes.end())
				{
					return false;
				}

				((Native::cMesh*)*Mesh)->destroy();
				delete *Mesh;
				*Mesh = NULL;

				this->Meshes.erase(it);

				return true;
			}

			void cDependentContent::clearMeshes()
			{
				for (std::set <Native::cMesh*>::iterator it = this->Meshes.begin(); it != this->Meshes.end(); ++it)
				{
					it._Ptr->_Myval->destroy();
					delete it._Ptr->_Myval;
					it._Ptr->_Myval = NULL;
				}
				this->Meshes.clear();
			}

			void cDependentContent::terminate()
			{
				clearMeshes();
			}
		}
	}
}