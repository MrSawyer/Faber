#include "Scene.h"

namespace Faber
{
	namespace Content
	{
		cScene::cScene() {}

		cScene::~cScene() {}

		bool cScene::addStaticObject(cStaticObject *StaticObject)
		{
			if (StaticObject == NULL)
			{
				return false;
			}

			std::pair <std::set <cStaticObject*>::iterator, bool> Result = this->StaticObjects.emplace(StaticObject);

			if (Result.second == false)
			{
				return false;
			}
			if (Result.first == this->StaticObjects.end())
			{
				return false;
			}

			return true;
		}

		void cScene::removeStaticObject(cStaticObject *StaticObject)
		{
			if (StaticObject == NULL)
			{
				return;
			}

			std::set <cStaticObject*>::iterator it = this->StaticObjects.find(StaticObject);

			if (it == this->StaticObjects.end())
			{
				return;
			}

			this->StaticObjects.erase(it);
		}

		void cScene::renderStaticObjects(Graphics::cShaders *Shaders)
		{
			for (std::set <cStaticObject*>::iterator it = this->StaticObjects.begin(); it != this->StaticObjects.end(); ++it)
			{
				it._Ptr->_Myval->render(Shaders);
			}
		}
	}
}