#include "Camera.h"

namespace Faber
{
	namespace Content
	{
		cCamera::cCamera() {}

		cCamera::~cCamera() {}

		void cCamera::setPosition(glm::vec3 Position)
		{
			this->Position = Position;
		}

		void cCamera::setPosition(float PositionX, float PositionY, float PositionZ)
		{
			this->Position.x = PositionX;
			this->Position.y = PositionY;
			this->Position.z = PositionZ;
		}

		void cCamera::setPositionX(float PositionX)
		{
			this->Position.x = PositionX;
		}

		void cCamera::setPositionY(float PositionY)
		{
			this->Position.y = PositionY;
		}

		void cCamera::setPositionZ(float PositionZ)
		{
			this->Position.z = PositionZ;
		}

		void cCamera::setFront(glm::vec3 Front)
		{
			this->Front = Front;
		}

		void cCamera::setFront(float FrontX, float FrontY, float FrontZ)
		{
			this->Front.x = FrontX;
			this->Front.y = FrontY;
			this->Front.z = FrontZ;
		}

		void cCamera::setFrontX(float FrontX)
		{
			this->Front.x = FrontX;
		}

		void cCamera::setFrontY(float FrontY)
		{
			this->Front.y = FrontY;
		}

		void cCamera::setFrontZ(float FrontZ)
		{
			this->Front.z = FrontZ;
		}

		void cCamera::setUp(glm::vec3 Up)
		{
			this->Up = Up;
		}

		void cCamera::setUp(float UpX, float UpY, float UpZ)
		{
			this->Up.x = UpX;
			this->Up.y = UpY;
			this->Up.z = UpZ;
		}

		void cCamera::setUpX(float UpX)
		{
			this->Up.x = UpX;
		}

		void cCamera::setUpY(float UpY)
		{
			this->Up.y = UpY;
		}

		void cCamera::setUpZ(float UpZ)
		{
			this->Up.z = UpZ;
		}

		glm::vec3 cCamera::getPosition()
		{
			return this->Position;
		}

		glm::vec3 cCamera::getFront()
		{
			return this->Front;
		}

		glm::vec3 cCamera::getUp()
		{
			return this->Up;
		}
	}
}