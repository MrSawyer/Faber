#include "Viewport.h"

namespace Faber
{
	namespace Content
	{
		cViewport::cViewport()
		{
			this->ActiveCamera = NULL;
			this->ViewedScene = NULL;

			this->Scale.x = 1.0f;
			this->Scale.y = 1.0f;
		}

		cViewport::~cViewport() {}

		void cViewport::setActiveCamera(cCamera *Camera)
		{
			this->ActiveCamera = Camera;
		}

		cCamera *cViewport::getActiveCamera()
		{
			return this->ActiveCamera;
		}

		void cViewport::setViewedScene(cScene *Scene)
		{
			this->ViewedScene = Scene;
		}

		cScene *cViewport::getViewedScene()
		{
			return this->ViewedScene;
		}

		void cViewport::setPosition(glm::vec2 PosFraction)
		{
			this->Position = PosFraction;
		}

		void cViewport::setPosition(float PosFractionX, float PosFractionY)
		{
			this->Position.x = PosFractionX;
			this->Position.y = PosFractionY;
		}

		void cViewport::setPositionX(float PosFractionX)
		{
			this->Position.x = PosFractionX;
		}

		void cViewport::setPositionY(float PosFractionY)
		{
			this->Position.y = PosFractionY;
		}

		void cViewport::setScale(glm::vec2 ScaleFraction)
		{
			this->Scale = ScaleFraction;
		}

		void cViewport::setScale(float ScaleFractionX, float ScaleFractionY)
		{
			this->Scale.x = ScaleFractionX;
			this->Scale.y = ScaleFractionY;
		}

		void cViewport::setScaleX(float ScaleFractionX)
		{
			this->Scale.x = ScaleFractionX;
		}

		void cViewport::setScaleY(float ScaleFractionY)
		{
			this->Scale.y = ScaleFractionY;
		}

		glm::vec2 cViewport::getPosition()
		{
			return this->Position;
		}

		glm::vec2 cViewport::getScale()
		{
			return this->Scale;
		}

		void cViewport::setViewport(float X, float Y, float Z, float W)
		{
			this->Viewport.x = X;
			this->Viewport.y = Y;
			this->Viewport.z = Z;
			this->Viewport.w = W;
		}

		void cViewport::setAspect(float Aspect)
		{
			this->Aspect = Aspect;
		}

		float cViewport::getViewportX()
		{
			return this->Viewport.x;
		}

		float cViewport::getViewportY()
		{
			return this->Viewport.y;
		}

		float cViewport::getViewportZ()
		{
			return this->Viewport.z;
		}

		float cViewport::getViewportW()
		{
			return this->Viewport.w;
		}

		float cViewport::getAspect()
		{
			return this->Aspect;
		}
	}
}