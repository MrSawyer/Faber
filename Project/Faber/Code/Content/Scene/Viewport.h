#pragma once

#include <Windows.h>
#include <GL/glew.h>
#include <GLM/glm.hpp>

#include "Camera.h"
#include "Scene.h"

#include "../../Config.h"

namespace Faber
{
	namespace Content
	{
		class cViewport
		{
		private:
			cCamera *ActiveCamera;
			cScene	*ViewedScene;

			glm::vec4 Viewport;
			glm::vec2 Position;
			glm::vec2 Scale;

			float Aspect;

		public:
			FaberEngine cViewport();
			FaberEngine ~cViewport();

			FaberEngine	void		setActiveCamera(cCamera *Camera);
			FaberEngine cCamera		*getActiveCamera();

			FaberEngine void		setViewedScene(cScene *Scene);
			FaberEngine cScene		*getViewedScene();

			FaberEngine void		setPosition(glm::vec2 PosFraction);
			FaberEngine void		setPosition(float PosFractionX, float PosFractionY);
			FaberEngine void		setPositionX(float PosFractionX);
			FaberEngine void		setPositionY(float PosFractionY);

			FaberEngine void		setScale(glm::vec2 ScaleFraction);
			FaberEngine void		setScale(float ScaleFractionX, float ScaleFractionY);
			FaberEngine void		setScaleX(float ScaleFractionX);
			FaberEngine void		setScaleY(float ScaleFractionY);

			FaberEngine	glm::vec2	getPosition();
			FaberEngine glm::vec2	getScale();

			FaberEngine void		setViewport(float X, float Y, float Z, float W);
			FaberEngine void		setAspect(float Aspect);

			FaberEngine float		getViewportX();
			FaberEngine float		getViewportY();
			FaberEngine float		getViewportZ();
			FaberEngine float		getViewportW();
			FaberEngine float		getAspect();
		};
	}
}