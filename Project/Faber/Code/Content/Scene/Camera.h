#pragma once

#include <Windows.h>
#include <GL/glew.h>
#include <GLM/glm.hpp>
#include <GLM/gtc/matrix_transform.hpp>
#include "../../Config.h"

namespace Faber
{
	namespace Content
	{
		class cCamera
		{
		private:
			glm::vec3 Position;
			glm::vec3 Front;
			glm::vec3 Up;

		public:
			FaberEngine cCamera();
			FaberEngine ~cCamera();

			FaberEngine void		setPosition(glm::vec3 Position);
			FaberEngine void		setPosition(float PositionX, float PositionY, float PositionZ);
			FaberEngine void		setPositionX(float PositionX);
			FaberEngine void		setPositionY(float PositionY);
			FaberEngine void		setPositionZ(float PositionZ);

			FaberEngine void		setFront(glm::vec3 Front);
			FaberEngine void		setFront(float FrontX, float FrontY, float FrontZ);
			FaberEngine void		setFrontX(float FrontX);
			FaberEngine void		setFrontY(float FrontY);
			FaberEngine void		setFrontZ(float FrontZ);

			FaberEngine void		setUp(glm::vec3 Up);
			FaberEngine void		setUp(float UpX, float UpY, float UpZ);
			FaberEngine void		setUpX(float UpX);
			FaberEngine void		setUpY(float UpY);
			FaberEngine void		setUpZ(float UpZ);

			FaberEngine glm::vec3	getPosition();
			FaberEngine glm::vec3	getFront();
			FaberEngine glm::vec3	getUp();
		};
	}
}