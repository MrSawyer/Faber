#pragma once

#include <Windows.h>
#include <GL/glew.h>
#include <set>

#include "../Native/Mesh.h"
#include "../Native/Transformable.h"
#include "../../Graphics/RenderTypes.h"
#include "../../Graphics/Shaders.h"

#include "../../Config.h"

namespace Faber
{
	namespace Content
	{
		class cStaticObject : public Native::cTransformable
		{
		private:
			std::set <Native::cMesh*>	Meshes;
			unsigned int				NumMeshes;

		public:
			FaberEngine cStaticObject();
			FaberEngine ~cStaticObject();

			FaberEngine bool	initialize();
			FaberEngine void	terminate();

			FaberEngine bool			addMesh(Native::cMesh *Mesh);
			FaberEngine void			removeMesh(Native::cMesh *Mesh);

			FaberEngine std::set <Native::cMesh*>	getMeshes();
			FaberEngine unsigned int				getNumMeshes();

			FaberEngine virtual void update() {};
			FaberEngine void render(Graphics::cShaders *Shaders);
		};
	}
}