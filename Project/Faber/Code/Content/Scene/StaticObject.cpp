#include "StaticObject.h"
#include "../../Graphics/Framebuffers.h"

namespace Faber
{
	namespace Content
	{
		cStaticObject::cStaticObject() {}

		cStaticObject::~cStaticObject() {}

		bool cStaticObject::initialize()
		{
			if (!this->cTransformable::initialize())
			{
				return false;
			}

			return true;
		}

		void cStaticObject::terminate()
		{
			this->cTransformable::terminate();
		}

		bool cStaticObject::addMesh(Native::cMesh *Mesh)
		{
			if (Mesh == NULL)
			{
				return false;
			}

			std::pair <std::set <Native::cMesh*>::iterator, bool> Result = this->Meshes.emplace(Mesh);
			
			if (Result.second == false)
			{
				return false;
			}
			if (Result.first == this->Meshes.end())
			{
				return false;
			}

			this->NumMeshes = this->Meshes.size();

			return true;
		}

		void cStaticObject::removeMesh(Native::cMesh *Mesh)
		{
			if (Mesh == NULL)
				return;

			std::set <Native::cMesh*>::iterator it = this->Meshes.find(Mesh);

			if (it == this->Meshes.end())
			{
				return;
			}

			this->Meshes.erase(it);

			this->NumMeshes = this->Meshes.size();
		}

		std::set <Native::cMesh*> cStaticObject::getMeshes()
		{
			return this->Meshes;
		}

		unsigned int cStaticObject::getNumMeshes()
		{
			return this->NumMeshes;
		}

		void cStaticObject::render(Graphics::cShaders *Shaders)
		{
			for (std::set <Native::cMesh*>::iterator it = this->Meshes.begin(); it != this->Meshes.end(); ++it)
			{
				this->calculateMatrix();
				it._Ptr->_Myval->calculateMatrix();
				
				glm::mat4 MMatrix = this->getMatrix() * it._Ptr->_Myval->getMatrix();
				glm::mat3 InvTransMMatrix = this->getInvTransMatrix() * it._Ptr->_Myval->getInvTransMatrix();

				glUniformMatrix4fv(Shaders->getFGPS_MMatrixLoc(), 1, GL_FALSE, glm::value_ptr(MMatrix));
				glUniformMatrix3fv(Shaders->getFGPS_InvTransMMatrixLoc(), 1, GL_FALSE, glm::value_ptr(InvTransMMatrix));

				it._Ptr->_Myval->setupMaterial(Shaders);
				it._Ptr->_Myval->render();
			}
		}
	}
}