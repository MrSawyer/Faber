#pragma once

#include <GLM/glm.hpp>
#include <vector>

#include "../Native/Texture.h"
#include "../../Graphics/Shaders.h"

#include "../../Config.h"

namespace Faber
{
	namespace Content
	{
		struct sMaterialTexture
		{
			glm::vec4			Color;
			Native::cTexture	*Texture;
			unsigned int		UVsArrayID;

			sMaterialTexture()
			{
				this->Color = glm::vec4(1.0f);
				this->Texture = NULL;
				this->UVsArrayID = 0;
			}
		};

		class cMaterial
		{
		private:
			std::vector <sMaterialTexture*>	Colors;
			unsigned int					NumColors;

			std::vector <sMaterialTexture*>	Normals;
			unsigned int					NumNormals;

			std::vector <sMaterialTexture*>	Speculars;
			unsigned int					NumSpeculars;

		public:
			FaberEngine cMaterial();
			FaberEngine ~cMaterial();

			FaberEngine void				addColor(sMaterialTexture *MaterialColor);
			FaberEngine void				removeColor(sMaterialTexture **MaterialColor);
			FaberEngine sMaterialTexture	*getColor(unsigned int ID);
			FaberEngine unsigned int		getNumColors();

			FaberEngine void				addNormal(sMaterialTexture *MaterialNormal);
			FaberEngine void				removeNormal(sMaterialTexture **MaterialNormal);
			FaberEngine sMaterialTexture	*getNormal(unsigned int ID);
			FaberEngine unsigned int		getNumNormals();

			FaberEngine void				addSpecular(sMaterialTexture *MaterialSpecular);
			FaberEngine void				removeSpecular(sMaterialTexture **MaterialSpecular);
			FaberEngine sMaterialTexture	*getSpecular(unsigned int ID);
			FaberEngine unsigned int		getNumSpeculars();

			FaberEngine void	setupMaterial(Graphics::cShaders *Shaders);
		};
	}
}