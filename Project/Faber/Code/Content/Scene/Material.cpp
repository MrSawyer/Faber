#include "Material.h"

namespace Faber
{
	namespace Content
	{
		cMaterial::cMaterial()
		{
			this->NumColors = 0;
			this->NumNormals = 0;
			this->NumSpeculars = 0;
		}

		cMaterial::~cMaterial()
		{
			for (unsigned int i = 0; i < this->NumColors; i++)
			{
				delete this->Colors[i];
				this->Colors[i] = NULL;
			}
			this->Colors.clear();
			this->NumColors = 0;

			for (unsigned int i = 0; i < this->NumNormals; i++)
			{
				delete this->Normals[i];
				this->Normals[i] = NULL;
			}
			this->Normals.clear();
			this->NumNormals = 0;

			for (unsigned int i = 0; i < this->NumSpeculars; i++)
			{
				delete this->Speculars[i];
				this->Speculars[i] = NULL;
			}
			this->Speculars.clear();
			this->NumSpeculars = 0;
		}

		void cMaterial::addColor(sMaterialTexture *MaterialColor)
		{
			if (MaterialColor == NULL)
				return;

			this->Colors.push_back(MaterialColor);
			this->NumColors = this->Colors.size();
		}

		void cMaterial::removeColor(sMaterialTexture **MaterialColor)
		{
			if (MaterialColor == NULL)
			{
				return;

				if (*MaterialColor == NULL)
				{
					return;
				}
			}

			for (unsigned int i = 0; i < this->NumColors; i++)
			{
				if (this->Colors[i] == *MaterialColor)
				{
					delete this->Colors[i];
					this->Colors[i] = NULL;
					*MaterialColor = NULL;
					this->Colors.erase(this->Colors.begin() + i);
					return;
				}
			}
		}

		sMaterialTexture *cMaterial::getColor(unsigned int ID)
		{
			return this->Colors[ID];
		}

		unsigned int cMaterial::getNumColors()
		{
			return this->NumColors;
		}

		void cMaterial::addNormal(sMaterialTexture *MaterialNormal)
		{
			if (MaterialNormal == NULL)
				return;

			this->Normals.push_back(MaterialNormal);
			this->NumNormals = this->Normals.size();
		}

		void cMaterial::removeNormal(sMaterialTexture **MaterialNormal)
		{
			if (MaterialNormal == NULL)
			{
				return;

				if (*MaterialNormal == NULL)
				{
					return;
				}
			}

			for (unsigned int i = 0; i < this->NumNormals; i++)
			{
				if (this->Normals[i] == *MaterialNormal)
				{
					delete this->Normals[i];
					this->Normals[i] = NULL;
					*MaterialNormal = NULL;
					this->Normals.erase(this->Normals.begin() + i);
					return;
				}
			}
		}

		sMaterialTexture *cMaterial::getNormal(unsigned int ID)
		{
			return this->Normals[ID];
		}

		unsigned int cMaterial::getNumNormals()
		{
			return this->NumNormals;
		}

		void cMaterial::addSpecular(sMaterialTexture *MaterialSpecular)
		{
			if (MaterialSpecular == NULL)
				return;

			this->Speculars.push_back(MaterialSpecular);
			this->NumSpeculars = this->Speculars.size();
		}

		void cMaterial::removeSpecular(sMaterialTexture **MaterialSpecular)
		{
			if (MaterialSpecular == NULL)
			{
				return;

				if (*MaterialSpecular == NULL)
				{
					return;
				}
			}

			for (unsigned int i = 0; i < this->NumSpeculars; i++)
			{
				if (this->Speculars[i] == *MaterialSpecular)
				{
					delete this->Speculars[i];
					this->Speculars[i] = NULL;
					*MaterialSpecular = NULL;
					this->Speculars.erase(this->Speculars.begin() + i);
					return;
				}
			}
		}

		sMaterialTexture *cMaterial::getSpecular(unsigned int ID)
		{
			return this->Speculars[ID];
		}

		unsigned int cMaterial::getNumSpeculars()
		{
			return this->NumSpeculars;
		}

		void cMaterial::setupMaterial(Graphics::cShaders *Shaders)
		{
			int SamplerID = 0;

			for (unsigned int i = 0; i < this->NumColors; i++, SamplerID++)
			{
				glUniform4f
				(
					Shaders->getFGPS_ColorVLoc(),
					this->Colors[i]->Color.x,
					this->Colors[i]->Color.y,
					this->Colors[i]->Color.z,
					this->Colors[i]->Color.w
				);

				//glActiveTexture(GL_TEXTURE0 + i);
				//glBindTexture(GL_TEXTURE_2D, this->Colors[i]->Texture->getID());
				//glUniform1i(Shader->getFGPS_ColorTLoc(), i);

				glUniform1i(Shaders->getFGPS_UVsColorArrayIDLoc(), i);
			}
			glUniform1i(Shaders->getFGPS_NumColorsLoc(), SamplerID);

			for (unsigned int i = 0; i < this->NumNormals; i++, SamplerID++)
			{
				glUniform1i(Shaders->getFGPS_UVsNormalArrayIDLoc(), i);
			}
			glUniform1i(Shaders->getFGPS_NumNormalsLoc(), SamplerID);

			for (unsigned int i = 0; i < this->NumSpeculars; i++, SamplerID++)
			{
				glUniform4f
				(
					Shaders->getFGPS_SpecularVLoc(),
					this->Speculars[i]->Color.x,
					this->Speculars[i]->Color.y,
					this->Speculars[i]->Color.z,
					this->Speculars[i]->Color.w
				);

				glUniform1i(Shaders->getFGPS_UVsSpecularArrayIDLoc(), i);
			}
			glUniform1i(Shaders->getFGPS_NumSpecularsLoc(), SamplerID);
		}
	}
}