#pragma once

#include <set>

#include "StaticObject.h"

#include "../../Graphics/Shaders.h"

#include "../../Config.h"

namespace Faber
{
	namespace Content
	{
		class cScene
		{
		private:
			std::set <cStaticObject*> StaticObjects;

		public:
			FaberEngine cScene();
			FaberEngine ~cScene();

			FaberEngine bool addStaticObject(cStaticObject *StaticObject);
			FaberEngine void removeStaticObject(cStaticObject *StaticObject);

			FaberEngine void renderStaticObjects(Graphics::cShaders *Shaders);
		};
	}
}