#pragma once
#include "../Config.h"
#include <GLM/glm.hpp>

namespace Faber::Methods {
	template<class Type, class name> using setPtr = void(name::*)(Type, Type*);
	template<class Type, class name> using getPtr =  Type(name::*)(Type);
	template<class Type> using acc_get = const Type &;
	template<class Type> using acc_set = const Type &;
}