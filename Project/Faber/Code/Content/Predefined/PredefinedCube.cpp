#include "PredefinedCube.h"

namespace Faber
{
	namespace Content
	{
		namespace Predefined
		{
			Native::sMeshRawData getPredefinedCube()
			{
				Native::sMeshRawData Result;

				std::vector <GLuint> Indices;
				std::vector <glm::vec3> Vertices;
				std::vector <glm::vec3> Normals;
				std::vector <glm::vec2> UVs;

				Vertices.push_back(glm::vec3(-0.500000, -0.500000, 0.500000));
				Vertices.push_back(glm::vec3(-0.500000, 0.500000, 0.500000));
				Vertices.push_back(glm::vec3(-0.500000, 0.500000, -0.500000));
				Vertices.push_back(glm::vec3(-0.500000, -0.500000, -0.500000));
				Vertices.push_back(glm::vec3(-0.500000, -0.500000, -0.500000));
				Vertices.push_back(glm::vec3(-0.500000, 0.500000, -0.500000));
				Vertices.push_back(glm::vec3(0.500000, 0.500000, -0.500000));
				Vertices.push_back(glm::vec3(0.500000, -0.500000, -0.500000));
				Vertices.push_back(glm::vec3(0.500000, -0.500000, -0.500000));
				Vertices.push_back(glm::vec3(0.500000, 0.500000, -0.500000));
				Vertices.push_back(glm::vec3(0.500000, 0.500000, 0.500000));
				Vertices.push_back(glm::vec3(0.500000, -0.500000, 0.500000));
				Vertices.push_back(glm::vec3(0.500000, -0.500000, 0.500000));
				Vertices.push_back(glm::vec3(0.500000, 0.500000, 0.500000));
				Vertices.push_back(glm::vec3(-0.500000, 0.500000, 0.500000));
				Vertices.push_back(glm::vec3(-0.500000, -0.500000, 0.500000));
				Vertices.push_back(glm::vec3(-0.500000, -0.500000, -0.500000));
				Vertices.push_back(glm::vec3(0.500000, -0.500000, -0.500000));
				Vertices.push_back(glm::vec3(0.500000, -0.500000, 0.500000));
				Vertices.push_back(glm::vec3(-0.500000, -0.500000, 0.500000));
				Vertices.push_back(glm::vec3(0.500000, 0.500000, -0.500000));
				Vertices.push_back(glm::vec3(-0.500000, 0.500000, -0.500000));
				Vertices.push_back(glm::vec3(-0.500000, 0.500000, 0.500000));
				Vertices.push_back(glm::vec3(0.500000, 0.500000, 0.500000));

				Normals.push_back(glm::vec3(-1.000000, 0.000000, 0.000000));
				Normals.push_back(glm::vec3(-1.000000, 0.000000, 0.000000));
				Normals.push_back(glm::vec3(-1.000000, 0.000000, 0.000000));
				Normals.push_back(glm::vec3(-1.000000, 0.000000, 0.000000));
				Normals.push_back(glm::vec3(0.000000, 0.000000, -1.000000));
				Normals.push_back(glm::vec3(0.000000, 0.000000, -1.000000));
				Normals.push_back(glm::vec3(0.000000, 0.000000, -1.000000));
				Normals.push_back(glm::vec3(0.000000, 0.000000, -1.000000));
				Normals.push_back(glm::vec3(1.000000, 0.000000, 0.000000));
				Normals.push_back(glm::vec3(1.000000, 0.000000, 0.000000));
				Normals.push_back(glm::vec3(1.000000, 0.000000, 0.000000));
				Normals.push_back(glm::vec3(1.000000, 0.000000, 0.000000));
				Normals.push_back(glm::vec3(0.000000, 0.000000, 1.000000));
				Normals.push_back(glm::vec3(0.000000, 0.000000, 1.000000));
				Normals.push_back(glm::vec3(0.000000, 0.000000, 1.000000));
				Normals.push_back(glm::vec3(0.000000, 0.000000, 1.000000));
				Normals.push_back(glm::vec3(0.000000, -1.000000, 0.000000));
				Normals.push_back(glm::vec3(0.000000, -1.000000, 0.000000));
				Normals.push_back(glm::vec3(0.000000, -1.000000, 0.000000));
				Normals.push_back(glm::vec3(0.000000, -1.000000, 0.000000));
				Normals.push_back(glm::vec3(0.000000, 1.000000, 0.000000));
				Normals.push_back(glm::vec3(0.000000, 1.000000, 0.000000));
				Normals.push_back(glm::vec3(0.000000, 1.000000, 0.000000));
				Normals.push_back(glm::vec3(0.000000, 1.000000, 0.000000));

				UVs.push_back(glm::vec2(0.000000, 0.000000));
				UVs.push_back(glm::vec2(1.000000, 0.000000));
				UVs.push_back(glm::vec2(1.000000, 1.000000));
				UVs.push_back(glm::vec2(0.000000, 1.000000));
				UVs.push_back(glm::vec2(0.000000, 0.000000));
				UVs.push_back(glm::vec2(1.000000, 0.000000));
				UVs.push_back(glm::vec2(1.000000, 1.000000));
				UVs.push_back(glm::vec2(0.000000, 1.000000));
				UVs.push_back(glm::vec2(0.000000, 0.000000));
				UVs.push_back(glm::vec2(1.000000, 0.000000));
				UVs.push_back(glm::vec2(1.000000, 1.000000));
				UVs.push_back(glm::vec2(0.000000, 1.000000));
				UVs.push_back(glm::vec2(0.000000, 0.000000));
				UVs.push_back(glm::vec2(1.000000, 0.000000));
				UVs.push_back(glm::vec2(1.000000, 1.000000));
				UVs.push_back(glm::vec2(0.000000, 1.000000));
				UVs.push_back(glm::vec2(0.000000, 0.000000));
				UVs.push_back(glm::vec2(1.000000, 0.000000));
				UVs.push_back(glm::vec2(1.000000, 1.000000));
				UVs.push_back(glm::vec2(0.000000, 1.000000));
				UVs.push_back(glm::vec2(0.000000, 0.000000));
				UVs.push_back(glm::vec2(1.000000, 0.000000));
				UVs.push_back(glm::vec2(1.000000, 1.000000));
				UVs.push_back(glm::vec2(0.000000, 1.000000));

				Indices.push_back(0);
				Indices.push_back(1);
				Indices.push_back(2);
				Indices.push_back(0);
				Indices.push_back(2);
				Indices.push_back(3);
				Indices.push_back(4);
				Indices.push_back(5);
				Indices.push_back(6);
				Indices.push_back(4);
				Indices.push_back(6);
				Indices.push_back(7);
				Indices.push_back(8);
				Indices.push_back(9);
				Indices.push_back(10);
				Indices.push_back(8);
				Indices.push_back(10);
				Indices.push_back(11);
				Indices.push_back(12);
				Indices.push_back(13);
				Indices.push_back(14);
				Indices.push_back(12);
				Indices.push_back(14);
				Indices.push_back(15);
				Indices.push_back(16);
				Indices.push_back(17);
				Indices.push_back(18);
				Indices.push_back(16);
				Indices.push_back(18);
				Indices.push_back(19);
				Indices.push_back(20);
				Indices.push_back(21);
				Indices.push_back(22);
				Indices.push_back(20);
				Indices.push_back(22);
				Indices.push_back(23);

				Result.Name = "PredefinedCube";
				Result.Indices = Indices;
				Result.Vertices = Vertices;
				Result.Normals = Normals;
				Result.UVs = UVs;

				return Result;
			}
		}
	}
}