#pragma once

#include "../Native/Mesh.h"
#include "../../Config.h"

namespace Faber
{
	namespace Content
	{
		namespace Predefined
		{
			FaberEngine Native::sMeshRawData getPredefinedCube();
		}
	}
}