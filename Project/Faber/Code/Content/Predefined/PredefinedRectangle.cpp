#include "PredefinedRectangle.h"

namespace Faber
{
	namespace Content
	{
		namespace Predefined
		{
			Native::sMeshRawData getPredefinedRectangle()
			{
				Native::sMeshRawData Result;

				std::vector <GLuint> Indices;
				std::vector <glm::vec3> Vertices;
				std::vector <glm::vec3> Normals;
				std::vector <glm::vec2> UVs;

				Vertices.push_back(glm::vec3(-0.500000, -0.500000, -0.000000));
				Vertices.push_back(glm::vec3(0.500000, -0.500000, -0.000000));
				Vertices.push_back(glm::vec3(0.500000, 0.500000, 0.000000));
				Vertices.push_back(glm::vec3(-0.500000, 0.500000, 0.000000));

				Normals.push_back(glm::vec3(0.000000, -0.000000, 1.000000));
				Normals.push_back(glm::vec3(0.000000, -0.000000, 1.000000));
				Normals.push_back(glm::vec3(0.000000, -0.000000, 1.000000));
				Normals.push_back(glm::vec3(0.000000, -0.000000, 1.000000));

				UVs.push_back(glm::vec2(0.000100, 0.000100));
				UVs.push_back(glm::vec2(0.999900, 0.000100));
				UVs.push_back(glm::vec2(0.999900, 0.999900));
				UVs.push_back(glm::vec2(0.000100, 0.999900));

				Indices.push_back(0);
				Indices.push_back(1);
				Indices.push_back(2);
				Indices.push_back(0);
				Indices.push_back(2);
				Indices.push_back(3);

				Result.Name = "PredefinedRectangle";
				Result.Indices = Indices;
				Result.Vertices = Vertices;
				Result.Normals = Normals;
				Result.UVs = UVs;

				return Result;
			}
		}
	}
}