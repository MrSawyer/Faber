#include "PredefinedTriangle.h"

namespace Faber
{
	namespace Content
	{
		namespace Predefined
		{
			Native::sMeshRawData getPredefinedTriangle()
			{
				Native::sMeshRawData Result;

				std::vector <GLuint> Indices;
				std::vector <glm::vec3> Vertices;
				std::vector <glm::vec3> Normals;
				std::vector <glm::vec2> UVs;

				Vertices.push_back(glm::vec3(-0.5f, -0.5f, 0.0f));
				Vertices.push_back(glm::vec3(0.0f, 0.5f, 0.0f));
				Vertices.push_back(glm::vec3(0.5f, -0.5f, 0.0f));

				Normals.push_back(glm::vec3(0.0f, 0.0f, 1.0f));
				Normals.push_back(glm::vec3(0.0f, 0.0f, 1.0f));
				Normals.push_back(glm::vec3(0.0f, 0.0f, 1.0f));

				UVs.push_back(glm::vec2(0.0f, 0.0f));
				UVs.push_back(glm::vec2(0.5f, 1.0f));
				UVs.push_back(glm::vec2(1.0f, 0.0f));

				Indices.push_back(0);
				Indices.push_back(1);
				Indices.push_back(2);

				Result.Name = "PredefinedTriangle";
				Result.Indices = Indices;
				Result.Vertices = Vertices;
				Result.Normals = Normals;
				Result.UVs = UVs;

				return Result;
			}
		}
	}
}