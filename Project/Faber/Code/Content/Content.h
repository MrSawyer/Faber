#pragma once

#include "PredefinedContent.h"
#include "../Graphics/RenderDevice.h"

#include "Native/Mesh.h"
#include "Scene/Scene.h"
#include "Scene/Viewport.h"
#include "Scene/Camera.h"
#include "Scene/Material.h"
#include "Scene/StaticObject.h"

#include "../Config.h"

namespace Faber
{
	namespace Content
	{
		FaberEngine bool createMesh(Graphics::cRenderDevice *pRenderDevice, void **Mesh);
		FaberEngine bool createMesh(Graphics::cRenderDevice *pRenderDevice, void **Mesh, Native::sMeshRawData *RawDataPtr, cMaterial *Material, Graphics::eBufferTypes VertexBufferType = Graphics::BUFFER_TYPE_UNMODIFIABLE, Graphics::eBufferTypes IndexBufferType = Graphics::BUFFER_TYPE_UNMODIFIABLE);
		FaberEngine bool createMesh(Graphics::cRenderDevice *pRenderDevice, void **Mesh, std::vector <GLuint> *Indices, std::vector <glm::vec3> *Vertices, std::vector <glm::vec3> *Normals, std::vector <glm::vec2> *UVs, cMaterial *Material, Graphics::eBufferTypes VertexBufferType = Graphics::BUFFER_TYPE_UNMODIFIABLE, Graphics::eBufferTypes IndexBufferType = Graphics::BUFFER_TYPE_UNMODIFIABLE);
		FaberEngine bool destroyMesh(Graphics::cRenderDevice *pRenderDevice, void **Mesh);
		FaberEngine bool clearMeshes(Graphics::cRenderDevice *pRenderDevice);

		FaberEngine bool createMeshRawData(void **RawData, Predefined::ePredefinedPrimitiveTypes PredefinedPrimitive = Predefined::PREDEFINED_PRIMITIVE_NONE);
		FaberEngine bool destroyMeshRawData(void **RawData);
		FaberEngine void clearMeshRawData();

		FaberEngine bool createScene(void **Scene);
		FaberEngine bool destroyScene(void **Scene);
		FaberEngine void clearScenes();

		FaberEngine bool createViewport(void **Viewport);
		FaberEngine bool destroyViewport(void **Viewport);
		FaberEngine void clearViewports();

		FaberEngine bool createCamera(void **Camera);
		FaberEngine bool destroyCamera(void **Camera);
		FaberEngine void clearCameras();

		FaberEngine bool createMaterial(void **Material);
		FaberEngine bool destroyMaterial(void **Material);
		FaberEngine void clearMaterials();

		FaberEngine bool createStaticObject(void **StaticObject);
		FaberEngine bool destroyStaticObject(void **StaticObject);
		FaberEngine void clearStaticObjects();

		FaberEngine void terminate();
		FaberEngine bool terminate(Graphics::cRenderDevice *pRenderDevice);
	}
}