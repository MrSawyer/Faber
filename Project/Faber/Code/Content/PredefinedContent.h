#pragma once

#include "../Config.h"

namespace Faber
{
	namespace Content
	{
		namespace Predefined
		{
			enum ePredefinedPrimitiveTypes
			{
				PREDEFINED_PRIMITIVE_NONE = 0,
				PREDEFINED_PRIMITIVE_CIRCLE = 1,
				PREDEFINED_PRIMITIVE_CONE = 2,
				PREDEFINED_PRIMITIVE_CUBE = 3,
				PREDEFINED_PRIMITIVE_CYLINDER = 4,
				PREDEFINED_PRIMITIVE_RECTANGLE = 5,
				PREDEFINED_PRIMITIVE_SPHERE = 6,
				PREDEFINED_PRIMITIVE_TRIANGLE = 7
			};
		}
	}
}