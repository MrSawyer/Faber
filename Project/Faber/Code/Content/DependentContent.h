#pragma once

#include <set>

#include "Native/Mesh.h"
#include "Native/Texture.h"

#include "../Config.h"

namespace Faber
{
	namespace Content
	{
		namespace Dependent
		{
			class cDependentContent
			{
			private:
				std::set <Native::cMesh*> Meshes;
				std::set <Native::cTexture*> Textures;

			public:
				FaberEngine cDependentContent();
				FaberEngine ~cDependentContent();

				FaberEngine bool createMesh(void **Mesh);
				FaberEngine bool createMesh(void **Mesh, Native::sMeshRawData *RawDataPtr, cMaterial *Material, Graphics::eBufferTypes VertexBufferType = Graphics::BUFFER_TYPE_UNMODIFIABLE, Graphics::eBufferTypes IndexBufferType = Graphics::BUFFER_TYPE_UNMODIFIABLE);
				FaberEngine bool createMesh(void **Mesh, std::vector <GLuint> *Indices, std::vector <glm::vec3> *Vertices, std::vector <glm::vec3> *Normals, std::vector <glm::vec2> *UVs, cMaterial *Material, Graphics::eBufferTypes VertexBufferType = Graphics::BUFFER_TYPE_UNMODIFIABLE, Graphics::eBufferTypes IndexBufferType = Graphics::BUFFER_TYPE_UNMODIFIABLE);
				FaberEngine bool destroyMesh(void **Mesh);
				FaberEngine void clearMeshes();

				FaberEngine void terminate();
			};
		}
	}
}