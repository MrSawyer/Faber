#include "Content.h"

#include "Predefined/PredefinedCircle.h"
#include "Predefined/PredefinedCone.h"
#include "Predefined/PredefinedCube.h"
#include "Predefined/PredefinedCylinder.h"
#include "Predefined/PredefinedRectangle.h"
#include "Predefined/PredefinedSphere.h"
#include "Predefined/PredefinedTriangle.h"

#include <set>

namespace Faber
{
	namespace Content
	{
		std::set <Native::sMeshRawData*>	MeshRawData;
		std::set <cScene*>					Scenes;
		std::set <cViewport*>				Viewports;
		std::set <cCamera*>					Cameras;
		std::set <cMaterial*>				Materials;
		std::set <cStaticObject*>			StaticObjects;

		bool createMesh(Graphics::cRenderDevice *pRenderDevice, void **Mesh)
		{
			if (pRenderDevice == NULL)
			{
				return false;
			}

			HDC CurrentDC = wglGetCurrentDC();
			HGLRC CurrentRC = wglGetCurrentContext();

			if (pRenderDevice->makeContextCurrent() == false)
			{
				return false;
			}

			if (pRenderDevice->getDependentContent()->createMesh(Mesh) == false)
			{
				pRenderDevice->getDependentContent()->destroyMesh(Mesh);
				return false;
			}

			if (wglMakeCurrent(CurrentDC, CurrentRC) == false)
			{
				return false;
			}

			return true;
		}

		bool createMesh(Graphics::cRenderDevice *pRenderDevice, void **Mesh, Native::sMeshRawData *RawDataPtr, cMaterial *Material, Graphics::eBufferTypes VertexBufferType, Graphics::eBufferTypes IndexBufferType)
		{
			if (pRenderDevice == NULL)
			{
				return false;
			}

			HDC CurrentDC = wglGetCurrentDC();
			HGLRC CurrentRC = wglGetCurrentContext();

			if (pRenderDevice->makeContextCurrent() == false)
			{
				return false;
			}

			if (pRenderDevice->getDependentContent()->createMesh(Mesh, RawDataPtr, Material, VertexBufferType, IndexBufferType) == false)
			{
				pRenderDevice->getDependentContent()->destroyMesh(Mesh);
				return false;
			}

			if (wglMakeCurrent(CurrentDC, CurrentRC) == false)
			{
				return false;
			}

			return true;
		}

		bool createMesh(Graphics::cRenderDevice *pRenderDevice, void **Mesh, std::vector <GLuint> *Indices, std::vector <glm::vec3> *Vertices, std::vector <glm::vec3> *Normals, std::vector <glm::vec2> *UVs, cMaterial *Material, Graphics::eBufferTypes VertexBufferType, Graphics::eBufferTypes IndexBufferType)
		{
			if (pRenderDevice == NULL)
			{
				return false;
			}

			HDC CurrentDC = wglGetCurrentDC();
			HGLRC CurrentRC = wglGetCurrentContext();

			if (pRenderDevice->makeContextCurrent() == false)
			{
				return false;
			}

			if (pRenderDevice->getDependentContent()->createMesh(Mesh, Indices, Vertices, Normals, UVs, Material, VertexBufferType, IndexBufferType) == false)
			{
				pRenderDevice->getDependentContent()->destroyMesh(Mesh);
				return false;
			}

			if (wglMakeCurrent(CurrentDC, CurrentRC) == false)
			{
				return false;
			}

			return true;
		}

		bool destroyMesh(Graphics::cRenderDevice *pRenderDevice, void **Mesh)
		{
			if (pRenderDevice == NULL)
			{
				return false;
			}

			HDC CurrentDC = wglGetCurrentDC();
			HGLRC CurrentRC = wglGetCurrentContext();

			if (pRenderDevice->makeContextCurrent() == false)
			{
				return false;
			}

			if (pRenderDevice->getDependentContent()->destroyMesh(Mesh) == false)
			{
				return false;
			}

			if (wglMakeCurrent(CurrentDC, CurrentRC) == false)
			{
				return false;
			}

			return true;
		}

		bool clearMeshes(Graphics::cRenderDevice *pRenderDevice)
		{
			if (pRenderDevice == NULL)
			{
				return false;
			}

			pRenderDevice->getDependentContent()->clearMeshes();

			return true;
		}

		bool createMeshRawData(void **RawData, Predefined::ePredefinedPrimitiveTypes PredefinedPrimitive)
		{
			if (RawData == NULL)
			{
				return false;
			}

			if (*RawData == NULL)
			{
				*RawData = new Native::sMeshRawData();

				if (*RawData == NULL)
				{
					return false;
				}
			}

			std::pair <std::set <Native::sMeshRawData*>::iterator, bool> Result = MeshRawData.emplace((Native::sMeshRawData*)*RawData);

			if (Result.second == false)
			{
				return false;
			}

			if (Result.first == MeshRawData.end())
			{
				return false;
			}

			if (PredefinedPrimitive == Predefined::PREDEFINED_PRIMITIVE_NONE)
			{
				return true;
			}

			Native::sMeshRawData Data;

			switch (PredefinedPrimitive)
			{
			case Predefined::PREDEFINED_PRIMITIVE_CIRCLE:
				Data = Predefined::getPredefinedCircle();
				break;

			case Predefined::PREDEFINED_PRIMITIVE_CONE:
				Data = Predefined::getPredefinedCone();
				break;

			case Predefined::PREDEFINED_PRIMITIVE_CUBE:
				Data = Predefined::getPredefinedCube();
				break;

			case Predefined::PREDEFINED_PRIMITIVE_CYLINDER:
				Data = Predefined::getPredefinedCylinder();
				break;

			case Predefined::PREDEFINED_PRIMITIVE_RECTANGLE:
				Data = Predefined::getPredefinedRectangle();
				break;

			case Predefined::PREDEFINED_PRIMITIVE_SPHERE:
				Data = Predefined::getPredefinedSphere();
				break;

			case Predefined::PREDEFINED_PRIMITIVE_TRIANGLE:
				Data = Predefined::getPredefinedTriangle();
				break;

			default:
				return true;
			}

			Result.first._Ptr->_Myval->Name = Data.Name;
			Result.first._Ptr->_Myval->Indices = Data.Indices;
			Result.first._Ptr->_Myval->Vertices = Data.Vertices;
			Result.first._Ptr->_Myval->Normals = Data.Normals;
			Result.first._Ptr->_Myval->UVs = Data.UVs;

			return true;
		}

		bool destroyMeshRawData(void **RawData)
		{
			if (RawData == NULL)
			{
				return false;
			}

			if (*RawData == NULL)
			{
				return false;
			}

			std::set <Native::sMeshRawData*>::iterator it = MeshRawData.find((Native::sMeshRawData*)*RawData);

			if (it == MeshRawData.end())
			{
				return false;
			}

			delete *RawData;
			*RawData = NULL;

			MeshRawData.erase(it);

			return true;
		}

		void clearMeshRawData()
		{
			for (std::set <Native::sMeshRawData*>::iterator it = MeshRawData.begin(); it != MeshRawData.end(); ++it)
			{
				delete it._Ptr->_Myval;
				it._Ptr->_Myval = NULL;
			}
			MeshRawData.clear();
		}

		bool createScene(void **Scene)
		{
			if (Scene == NULL)
			{
				return false;
			}

			if (*Scene == NULL)
			{
				*Scene = new cScene();

				if (*Scene == NULL)
				{
					return false;
				}
			}

			std::pair <std::set <cScene*>::iterator, bool> Result = Scenes.emplace((cScene*)*Scene);

			if (Result.second == false)
			{
				return false;
			}

			if (Result.first == Scenes.end())
			{
				return false;
			}

			return true;
		}

		bool destroyScene(void **Scene)
		{
			if (Scene == NULL)
			{
				return false;
			}

			if (*Scene == NULL)
			{
				return false;
			}

			std::set <cScene*>::iterator it = Scenes.find((cScene*)*Scene);

			if (it == Scenes.end())
			{
				return false;
			}

			delete *Scene;
			*Scene = NULL;

			Scenes.erase(it);

			return true;
		}

		void clearScenes()
		{
			for (std::set <cScene*>::iterator it = Scenes.begin(); it != Scenes.end(); ++it)
			{
				delete it._Ptr->_Myval;
				it._Ptr->_Myval = NULL;
			}
			Scenes.clear();
		}

		bool createViewport(void **Viewport)
		{
			if (Viewport == NULL)
			{
				return false;
			}

			if (*Viewport == NULL)
			{
				*Viewport = new cViewport();

				if (*Viewport == NULL)
				{
					return false;
				}
			}

			std::pair <std::set <cViewport*>::iterator, bool> Result = Viewports.emplace((cViewport*)*Viewport);
			
			if (Result.second == false)
			{
				return false;
			}

			if (Result.first == Viewports.end())
			{
				return false;
			}

			return true;
		}

		bool destroyViewport(void **Viewport)
		{
			if (Viewport == NULL)
			{
				return false;
			}

			if (*Viewport == NULL)
			{
				return false;
			}

			std::set <cViewport*>::iterator it = Viewports.find((cViewport*)*Viewport);

			if (it == Viewports.end())
			{
				return false;
			}

			delete *Viewport;
			*Viewport = NULL;

			Viewports.erase(it);

			return true;
		}

		void clearViewports()
		{
			for (std::set <cViewport*>::iterator it = Viewports.begin(); it != Viewports.end(); ++it)
			{
				delete it._Ptr->_Myval;
				it._Ptr->_Myval = NULL;
			}
			Viewports.clear();
		}

		bool createCamera(void **Camera)
		{
			if (Camera == NULL)
			{
				return false;
			}

			if (*Camera == NULL)
			{
				*Camera = new cCamera();

				if (*Camera == NULL)
				{
					return false;
				}
			}

			std::pair <std::set <cCamera*>::iterator, bool> Result = Cameras.emplace((cCamera*)*Camera);

			if (Result.second == false)
			{
				return false;
			}

			if (Result.first == Cameras.end())
			{
				return false;
			}

			return true;
		}

		bool destroyCamera(void **Camera)
		{
			if (Camera == NULL)
			{
				return false;
			}

			if (*Camera == NULL)
			{
				return false;
			}

			std::set <cCamera*>::iterator it = Cameras.find((cCamera*)*Camera);

			if (it == Cameras.end())
			{
				return false;
			}

			delete *Camera;
			*Camera = NULL;

			Cameras.erase(it);

			return true;
		}

		void clearCameras()
		{
			for (std::set <cCamera*>::iterator it = Cameras.begin(); it != Cameras.end(); ++it)
			{
				delete it._Ptr->_Myval;
				it._Ptr->_Myval = NULL;
			}
			Cameras.clear();
		}

		bool createMaterial(void **Material)
		{
			if (Material == NULL)
			{
				return false;
			}

			if (*Material == NULL)
			{
				*Material = new cMaterial();

				if (*Material == NULL)
				{
					return false;
				}
			}

			std::pair <std::set <cMaterial*>::iterator, bool> Result = Materials.emplace((cMaterial*)*Material);

			if (Result.second == false)
			{
				return false;
			}

			if (Result.first == Materials.end())
			{
				return false;
			}

			return true;
		}

		bool destroyMaterial(void **Material)
		{
			if (Material == NULL)
			{
				return false;
			}

			if (*Material == NULL)
			{
				return false;
			}

			std::set <cMaterial*>::iterator it = Materials.find((cMaterial*)*Material);

			if (it == Materials.end())
			{
				return false;
			}

			delete *Material;
			*Material = NULL;

			Materials.erase(it);

			return true;
		}

		void clearMaterials()
		{
			for (std::set <cMaterial*>::iterator it = Materials.begin(); it != Materials.end(); ++it)
			{
				delete it._Ptr->_Myval;
				it._Ptr->_Myval = NULL;
			}
			Materials.clear();
		}

		bool createStaticObject(void **StaticObject)
		{
			if (StaticObject == NULL)
			{
				return false;
			}

			if (*StaticObject == NULL)
			{
				*StaticObject = new cStaticObject();

				if (*StaticObject == NULL)
				{
					return false;
				}
			}

			if (((cStaticObject*)*StaticObject)->initialize() == false)
			{
				((cStaticObject*)*StaticObject)->terminate();
				delete *StaticObject;
				*StaticObject = NULL;
				return false;
			}

			std::pair <std::set <cStaticObject*>::iterator, bool> Result = StaticObjects.emplace((cStaticObject*)*StaticObject);

			if (Result.second == false)
			{
				return false;
			}

			if (Result.first == StaticObjects.end())
			{
				return false;
			}

			return true;
		}

		bool destroyStaticObject(void **StaticObject)
		{
			if (StaticObject == NULL)
			{
				return false;
			}

			if (*StaticObject == NULL)
			{
				return false;
			}

			std::set <cStaticObject*>::iterator it = StaticObjects.find((cStaticObject*)*StaticObject);

			if (it == StaticObjects.end())
			{
				return false;
			}

			((cStaticObject*)*StaticObject)->terminate();
			delete *StaticObject;
			*StaticObject = NULL;

			StaticObjects.erase(it);

			return true;
		}

		void clearStaticObjects()
		{
			for (std::set <cStaticObject*>::iterator it = StaticObjects.begin(); it != StaticObjects.end(); ++it)
			{
				it._Ptr->_Myval->terminate();
				delete it._Ptr->_Myval;
				it._Ptr->_Myval = NULL;
			}
			StaticObjects.clear();
		}

		void terminate()
		{
			clearMeshRawData();
			clearScenes();
			clearViewports();
			clearCameras();
			clearMaterials();
			clearStaticObjects();
		}

		bool terminate(Graphics::cRenderDevice *pRenderDevice)
		{
			if (pRenderDevice == NULL)
			{
				return false;
			}

			pRenderDevice->getDependentContent()->terminate();

			return true;
		}
	}
}