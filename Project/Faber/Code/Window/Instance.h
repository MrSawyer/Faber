#pragma once

#include <Windows.h>
#include "../Config.h"

namespace Faber
{
	namespace Window
	{
		FaberEngine void		setLibraryInstanceHandle(HINSTANCE hInstance);
		FaberEngine HINSTANCE	getLibraryInstanceHandle();
	}
}