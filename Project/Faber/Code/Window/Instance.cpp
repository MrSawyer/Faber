#include "Instance.h"

namespace Faber
{
	namespace Window
	{
		HINSTANCE InstanceHandle;

		void setLibraryInstanceHandle(HINSTANCE hInstance)
		{
			InstanceHandle = hInstance;
		}

		HINSTANCE getLibraryInstanceHandle()
		{
			return InstanceHandle;
		}
	}
}