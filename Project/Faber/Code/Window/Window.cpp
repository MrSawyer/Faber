#include "Window.h"
#include "Instance.h"
#include "Procedure.h"

#include <fstream>

namespace Faber
{
	namespace Window
	{
		HWND				WindowHandle = NULL;
		sWindowDescription	WindowDescription;

		bool create(sWindowDescription &WindowDesc)
		{
			WindowDescription = WindowDesc;

			WNDCLASSEX WindowClass;
			ZeroMemory(&WindowClass, sizeof(WNDCLASSEX));
			WindowClass.cbSize = sizeof(WNDCLASSEX);
			WindowClass.cbClsExtra = 0;
			WindowClass.cbWndExtra = 0;
			WindowClass.hInstance = getLibraryInstanceHandle();
			WindowClass.hCursor = LoadCursor(NULL, IDC_ARROW);
			WindowClass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
			WindowClass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
			WindowClass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
			WindowClass.lpfnWndProc = windowProc;
			WindowClass.lpszMenuName = NULL;
			WindowClass.lpszClassName = "FABERWNDCLASS";
			WindowClass.style = CS_OWNDC | CS_VREDRAW | CS_HREDRAW;

			if (RegisterClassEx(&WindowClass) == FALSE)
			{
				MessageBox(NULL, "Registering Faber window class failed!", "Error", MB_ICONERROR | MB_OK);
				return false;
			}

			WindowHandle = CreateWindow("FABERWNDCLASS", WindowDescription.Title.c_str(), WindowDescription.Style | WindowDescription.State, WindowDescription.PositionX, WindowDescription.PositionY, WindowDescription.SizeX, WindowDescription.SizeY, NULL, NULL, getLibraryInstanceHandle(), NULL);
			if (WindowHandle == NULL)
			{
				MessageBox(NULL, "Creating Faber window failed!", "Error", MB_ICONERROR | MB_OK);
				return false;
			}

			RAWINPUTDEVICE RawInputDevice[2];
			ZeroMemory(&RawInputDevice[0], sizeof(RAWINPUTDEVICE));
			ZeroMemory(&RawInputDevice[1], sizeof(RAWINPUTDEVICE));
			RawInputDevice[0].usUsagePage = 0x01;
			RawInputDevice[0].usUsage = 0x02;
			RawInputDevice[0].dwFlags = 0;
			RawInputDevice[0].hwndTarget = WindowHandle;
			RawInputDevice[1].usUsagePage = 0x01;
			RawInputDevice[1].usUsage = 0x06;
			RawInputDevice[1].dwFlags = 0;
			RawInputDevice[1].hwndTarget = WindowHandle;

			if (!RegisterRawInputDevices(RawInputDevice, 2, sizeof(RAWINPUTDEVICE)))
			{
				MessageBox(WindowHandle, "Registering Faber raw input devices failed!", "Error", MB_ICONERROR | MB_OK);
				return false;
			}

			UpdateWindow(WindowHandle);

			return true;
		}

		bool destroy()
		{
			Window::clearRenderDevices();

			if (UnregisterClass("FABERWNDCLASS", getLibraryInstanceHandle()) == FALSE)
			{
				MessageBox(NULL, "Unregistering Faber window class failed!", "Error", MB_ICONERROR | MB_OK);
				return false;
			}

			return true;
		}

		void setTitle(std::string Title)
		{
			LPCSTR WindowTitle = Title.c_str();
			SetWindowText(WindowHandle, WindowTitle);
		}

		void setPosition(unsigned int PositionX, unsigned int PositionY)
		{
			SetWindowPos(WindowHandle, NULL, (int)PositionX, (int)PositionY, NULL, NULL, SWP_NOZORDER | SWP_NOSIZE);
		}

		void setPositionInTheMiddle()
		{

		}

		void setSize(unsigned int SizeX, unsigned int SizeY)
		{
			SetWindowPos(WindowHandle, NULL, NULL, NULL, (int)SizeX, (int)SizeY, SWP_NOZORDER | SWP_NOMOVE);
		}

		std::string getTitle()
		{
			const int TitleLength = GetWindowTextLength(WindowHandle);
			char *WindowTitle = new char[(unsigned)TitleLength];
			if (WindowTitle == NULL)
			{
				return std::string();
			}
			GetWindowText(WindowHandle, WindowTitle, TitleLength);
			std::string Result = WindowTitle;
			delete[] WindowTitle;
			return Result;
		}

		unsigned int getPositionX()
		{
			RECT WindowRect;
			GetWindowRect(WindowHandle, &WindowRect);

			return (unsigned int)WindowRect.left;
		}

		unsigned int getPositionY()
		{
			RECT WindowRect;
			GetWindowRect(WindowHandle, &WindowRect);

			return (unsigned int)WindowRect.top;
		}

		unsigned int getSizeX()
		{
			RECT WindowRect;
			GetWindowRect(WindowHandle, &WindowRect);

			return (unsigned int)(WindowRect.right - WindowRect.left);
		}

		unsigned int getSizeY()
		{
			RECT WindowRect;
			GetWindowRect(WindowHandle, &WindowRect);

			return (unsigned int)(WindowRect.bottom - WindowRect.top);
		}

		HINSTANCE getInstanceHandle()
		{
			return getLibraryInstanceHandle();
		}

		HWND getNativeHandle()
		{
			return WindowHandle;
		}
	}
}