#pragma once

#include <Windows.h>
#include <string>
#include "../Config.h"

namespace Faber
{
	namespace Window
	{
		enum eWindowStyles
		{
			STYLE_EMPTY = WS_POPUP,
			STYLE_FRAME = WS_BORDER,
			STYLE_TITLE = WS_CAPTION,
			STYLE_CONTROLS = WS_SYSMENU,
			STYLE_RESIZABLE = WS_THICKFRAME,
			STYLE_MINIMIZABLE = WS_MINIMIZEBOX,
			STYLE_MAXIMIZABLE = WS_MAXIMIZEBOX
		};

		enum eWindowStates
		{
			STATE_HIDDEN = NULL,
			STATE_SHOWN = WS_VISIBLE,
			STATE_MINIMIZED = WS_MINIMIZE,
			STATE_MAXIMIZED = WS_MAXIMIZE
		};

		struct sWindowDescription
		{
			std::string		Title = "Unnamed";
			unsigned int	PositionX = 0;
			unsigned int	PositionY = 0;
			unsigned int	SizeX = 300;
			unsigned int	SizeY = 300;
			int				Style = STYLE_FRAME | STYLE_TITLE | STYLE_CONTROLS | STYLE_RESIZABLE | STYLE_MINIMIZABLE | STYLE_MAXIMIZABLE;
			int				State = STATE_SHOWN;
		};

		FaberEngine bool			create(sWindowDescription &WindowDesc);
		FaberEngine bool			destroy();

		FaberEngine void			setTitle(std::string Title);
		FaberEngine void			setPosition(unsigned int PositionX, unsigned int PositionY);
		FaberEngine void			setPositionInTheMiddle();
		FaberEngine void			setSize(unsigned int SizeX, unsigned int SizeY);
		FaberEngine void			setStyle(int Style);
		FaberEngine void			setState(int State);

		FaberEngine std::string		getTitle();
		FaberEngine unsigned int	getPositionX();
		FaberEngine unsigned int	getPositionY();
		FaberEngine unsigned int	getSizeX();
		FaberEngine unsigned int	getSizeY();
		FaberEngine int				getStyle();
		FaberEngine int				getState();

		FaberEngine HINSTANCE		getInstanceHandle();
		FaberEngine HWND			getNativeHandle();
	}
}