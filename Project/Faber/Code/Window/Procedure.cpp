#include "Procedure.h"
#include "../Graphics/RenderDevice.h"
#include "../Input/NativeInput.h"
#include <map>

namespace Faber
{
	namespace Window
	{
		std::map <HWND, Graphics::cRenderDevice*> RenderDevices;

		bool attachRenderDevice(HWND Owner, void **Device)
		{
			if (Device == NULL)
			{
				return false;
			}
			if (*Device == NULL)
			{
				return false;
			}

			std::pair <std::map <HWND, Graphics::cRenderDevice*>::iterator, bool> Result = RenderDevices.emplace(Owner, (Graphics::cRenderDevice*)(*Device));
			if (Result.second == false)
			{
				return false;
			}
			if (Result.first == RenderDevices.end())
			{
				return false;
			}

			return true;
		}

		void detachRenderDevice(HWND Owner)
		{
			std::map <HWND, Graphics::cRenderDevice*>::iterator it = RenderDevices.find(Owner);
			if (it != RenderDevices.end())
			{
				RenderDevices.erase(it);
			}
		}

		void clearRenderDevices()
		{
			RenderDevices.clear();
		}

		HRESULT CALLBACK windowProc(HWND hWnd, UINT Msg, WPARAM wParam, LPARAM lParam)
		{
			switch (Msg)
			{
			case WM_INPUT:
			{
				UINT dwSize;
				GetRawInputData((HRAWINPUT)lParam, RID_INPUT, NULL, &dwSize, sizeof(RAWINPUTHEADER));

				LPBYTE lpByte = NULL;
				lpByte = new BYTE[dwSize];
				if (lpByte == NULL)
					break;
				GetRawInputData((HRAWINPUT)lParam, RID_INPUT, lpByte, &dwSize, sizeof(RAWINPUTHEADER));

				RAWINPUT *RawInput = (RAWINPUT*)lpByte;

				if (RawInput->header.dwType == RIM_TYPEKEYBOARD)
				{
					if (RawInput->data.keyboard.Message == WM_KEYDOWN || RawInput->data.keyboard.Message == WM_SYSKEYDOWN)
						Input::setKeyboardButtonState(RawInput->data.keyboard.VKey, true);
					else if (RawInput->data.keyboard.Message == WM_KEYUP || RawInput->data.keyboard.Message == WM_SYSKEYUP)
						Input::setKeyboardButtonState(RawInput->data.keyboard.VKey, false);
				}

				if (RawInput->header.dwType == RIM_TYPEMOUSE)
				{
					POINT position;
					GetCursorPos(&position);
					ScreenToClient(hWnd, &position);

					RECT clientdim;
					GetClientRect(hWnd, &clientdim);

					Input::setMousePosition(position.x - (clientdim.right - clientdim.left) / 2,
											(clientdim.bottom - clientdim.top) / 2 - position.y);

					if (RawInput->data.mouse.usButtonFlags == RI_MOUSE_LEFT_BUTTON_DOWN)
						Input::setMouseButtonState(0, true);
					else if (RawInput->data.mouse.usButtonFlags == RI_MOUSE_LEFT_BUTTON_UP)
						Input::setMouseButtonState(0, false);

					if (RawInput->data.mouse.usButtonFlags == RI_MOUSE_RIGHT_BUTTON_DOWN)
						Input::setMouseButtonState(1, true);
					else if (RawInput->data.mouse.usButtonFlags == RI_MOUSE_RIGHT_BUTTON_UP)
						Input::setMouseButtonState(1, false);
				}

				delete[] lpByte;

				break;
			}

			case WM_SIZE:
			{
				RECT ClientSize;
				GetClientRect(hWnd, &ClientSize);

				std::map <HWND, Graphics::cRenderDevice*>::iterator it = RenderDevices.find(hWnd);
				if (it != RenderDevices.end())
				{
					it._Ptr->_Myval.second->setWindowClientSize
					(
						(float)(ClientSize.right - ClientSize.left),
						(float)(ClientSize.bottom - ClientSize.top)
					);
					it._Ptr->_Myval.second->render();
				}

				break;
			}
				
			case WM_SYSCOMMAND:
			{
				switch (wParam)
				{
				case SC_SCREENSAVE:
					return 0;

				case SC_MONITORPOWER:
					return 0;
				}

				break;
			}

			case WM_CLOSE:
				DestroyWindow(hWnd);
				break;

			case WM_DESTROY:
				PostQuitMessage(0);
				break;
			}

			return DefWindowProc(hWnd, Msg, wParam, lParam);
		}
	}
}