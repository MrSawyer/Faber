#pragma once

#include <Windows.h>
#include "../Config.h"

namespace Faber
{
	namespace Window
	{
		FaberEngine HRESULT CALLBACK windowProc(HWND hWnd, UINT Msg, WPARAM wParam, LPARAM lParam);
		FaberEngine bool attachRenderDevice(HWND Owner, void **Device);
		FaberEngine void detachRenderDevice(HWND Owner);
		FaberEngine void clearRenderDevices();
	}
}