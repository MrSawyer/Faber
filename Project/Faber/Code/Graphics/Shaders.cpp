#include "Shaders.h"

namespace Faber
{
	namespace Graphics
	{
		cShaders::cShaders()
		{
			this->FirstGeometryPassStatic = NULL;
			this->FGPS_MMatrixLocation = NULL;
			this->FGPS_VPMatrixLocation = NULL;
			this->FGPS_InvTransMMatrixLocation = NULL;
			this->FGPS_ColorVLocation = NULL;
			this->FGPS_ColorTLocation = NULL;
			this->FGPS_UVsColorArrayIDLocation = NULL;
			this->FGPS_NumColorsLocation = NULL;
			this->FGPS_NormalTLocation = NULL;
			this->FGPS_UVsNormalArrayIDLocation = NULL;
			this->FGPS_NumNormalsLocation = NULL;
			this->FGPS_SpecularVLocation = NULL;
			this->FGPS_SpecularTLocation = NULL;
			this->FGPS_UVsSpecularArrayIDLocation = NULL;
			this->FGPS_NumSpecularsLocation = NULL;
			
			this->FirstGeometryPassAnimated = NULL;
			this->FGPA_MVPMatrixLocation = NULL;

			this->DefferedLightingPass = NULL;
			this->DLP_TransVLocation = NULL;
			this->DLP_ScaleVLocation = NULL;
			this->DLP_CameraVLocation = NULL;
			this->DLP_FGPositionTLocation = NULL;
			this->DLP_FGColorTLocation = NULL;
			this->DLP_FGNormalTLocation = NULL;
			this->DLP_FGMaterialTLocation = NULL;
			
			this->Postprocessing = NULL;
			this->P_TransVLocation = NULL;
			this->P_ScaleVLocation = NULL;
			this->P_DLFragColorLocation = NULL;
		}

		cShaders::~cShaders() {}

		bool buildProgram(const char *PathV, const char *PathF, GLuint &ProgramID)
		{
			ProgramID = glCreateProgram();
			if (ProgramID == NULL)
			{
				return false;
			}

			int		CompilationSuccess;
			char	CompilationLog[512];

			std::ifstream vShaderFile;
			vShaderFile.open(PathV);
			if (!vShaderFile.good())
			{
				vShaderFile.close();
				glDeleteProgram(ProgramID);
				ProgramID = NULL;
				return false;
			}

			std::stringstream vShaderStream;
			vShaderStream << vShaderFile.rdbuf();
			vShaderFile.close();

			GLuint VertexShader = glCreateShader(GL_VERTEX_SHADER);
			if (VertexShader == NULL)
			{
				glDeleteShader(VertexShader);
				glDeleteProgram(ProgramID);
				ProgramID = NULL;
				return false;
			}
			std::string vShaderString = vShaderStream.str();
			const GLchar *vShaderCode = vShaderString.c_str();

			glShaderSource(VertexShader, 1, &vShaderCode, NULL);

			glCompileShader(VertexShader);
			glGetShaderiv(VertexShader, GL_COMPILE_STATUS, &CompilationSuccess);
			if (!CompilationSuccess)
			{
				glGetShaderInfoLog(VertexShader, 512, NULL, CompilationLog);
				MessageBox(NULL, CompilationLog, "Error", MB_ICONERROR | MB_OK);
				glDeleteShader(VertexShader);
				glDeleteProgram(ProgramID);
				ProgramID = NULL;
				return false;
			}

			glAttachShader(ProgramID, VertexShader);
			glDeleteShader(VertexShader);

			std::ifstream fShaderFile;
			fShaderFile.open(PathF);
			if (!fShaderFile.good())
			{
				fShaderFile.close();
				glDeleteProgram(ProgramID);
				ProgramID = NULL;
				return false;
			}

			std::stringstream fShaderStream;
			fShaderStream << fShaderFile.rdbuf();
			fShaderFile.close();

			GLuint FragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
			if (FragmentShader == NULL)
			{
				glDeleteShader(FragmentShader);
				glDeleteProgram(ProgramID);
				ProgramID = NULL;
				return false;
			}
			std::string fShaderString = fShaderStream.str();
			const GLchar *fShaderCode = fShaderString.c_str();

			glShaderSource(FragmentShader, 1, &fShaderCode, NULL);

			glCompileShader(FragmentShader);
			glGetShaderiv(FragmentShader, GL_COMPILE_STATUS, &CompilationSuccess);
			if (!CompilationSuccess)
			{
				glGetShaderInfoLog(FragmentShader, 512, NULL, CompilationLog);
				MessageBox(NULL, CompilationLog, "Error", MB_ICONERROR | MB_OK);
				glDeleteShader(FragmentShader);
				glDeleteProgram(ProgramID);
				ProgramID = NULL;
				return false;
			}

			glAttachShader(ProgramID, FragmentShader);
			glDeleteShader(FragmentShader);

			glLinkProgram(ProgramID);
			glGetProgramiv(ProgramID, GL_LINK_STATUS, &CompilationSuccess);
			if (!CompilationSuccess)
			{
				glGetProgramInfoLog(ProgramID, 512, NULL, CompilationLog);
				MessageBox(NULL, CompilationLog, "Error", MB_ICONERROR | MB_OK);
				glDeleteProgram(ProgramID);
				ProgramID = NULL;
				return false;
			}

			return true;
		}

		bool cShaders::initialize()
		{
			bool Result = true;

			Result = buildProgram("Content/Shaders/FirstGeometryPassStatic.vert", "Content/Shaders/FirstGeometryPassStatic.frag", this->FirstGeometryPassStatic);
			if (Result == false)
			{
				return false;
			}
			this->FGPS_MMatrixLocation = glGetUniformLocation(this->FirstGeometryPassStatic, "MMatrix");
			this->FGPS_VPMatrixLocation = glGetUniformLocation(this->FirstGeometryPassStatic, "VPMatrix");
			this->FGPS_InvTransMMatrixLocation = glGetUniformLocation(this->FirstGeometryPassStatic, "InvTransMMatrix");
			this->FGPS_ColorVLocation = glGetUniformLocation(this->FirstGeometryPassStatic, "ColorV");
			this->FGPS_ColorTLocation = glGetUniformLocation(this->FirstGeometryPassStatic, "ColorT");
			this->FGPS_UVsColorArrayIDLocation = glGetUniformLocation(this->FirstGeometryPassStatic, "UVsColorArrayID");
			this->FGPS_NumColorsLocation = glGetUniformLocation(this->FirstGeometryPassStatic, "NumColors");
			this->FGPS_NormalTLocation = glGetUniformLocation(this->FirstGeometryPassStatic, "NormalT");
			this->FGPS_UVsNormalArrayIDLocation = glGetUniformLocation(this->FirstGeometryPassStatic, "UVsNormalArrayID");
			this->FGPS_NumNormalsLocation = glGetUniformLocation(this->FirstGeometryPassStatic, "NumNormals");
			this->FGPS_SpecularVLocation = glGetUniformLocation(this->FirstGeometryPassStatic, "SpecularV");
			this->FGPS_SpecularTLocation = glGetUniformLocation(this->FirstGeometryPassStatic, "SpecularT");
			this->FGPS_UVsSpecularArrayIDLocation = glGetUniformLocation(this->FirstGeometryPassStatic, "UVsSpecularArrayID");
			this->FGPS_NumSpecularsLocation = glGetUniformLocation(this->FirstGeometryPassStatic, "NumSpeculars");

			Result = buildProgram("Content/Shaders/DefferedLightingPass.vert", "Content/Shaders/DefferedLightingPass.frag", this->DefferedLightingPass);
			if (Result == false)
			{
				return false;
			}
			this->DLP_TransVLocation = glGetUniformLocation(this->DefferedLightingPass, "TransV");
			this->DLP_ScaleVLocation = glGetUniformLocation(this->DefferedLightingPass, "ScaleV");
			this->DLP_CameraVLocation = glGetUniformLocation(this->DefferedLightingPass, "CameraV");
			this->DLP_FGPositionTLocation = glGetUniformLocation(this->DefferedLightingPass, "FGPositionT");
			this->DLP_FGColorTLocation = glGetUniformLocation(this->DefferedLightingPass, "FGColorT");
			this->DLP_FGNormalTLocation = glGetUniformLocation(this->DefferedLightingPass, "FGNormalT");
			this->DLP_FGMaterialTLocation = glGetUniformLocation(this->DefferedLightingPass, "FGMaterialT");

			Result = buildProgram("Content/Shaders/Postprocessing.vert", "Content/Shaders/Postprocessing.frag", this->Postprocessing);
			if (Result == false)
			{
				return false;
			}
			this->P_TransVLocation = glGetUniformLocation(this->Postprocessing, "TransV");
			this->P_ScaleVLocation = glGetUniformLocation(this->Postprocessing, "ScaleV");
			this->P_DLFragColorLocation = glGetUniformLocation(this->Postprocessing, "DLFragColor");

			return true;
		}

		void cShaders::terminate()
		{
			if (this->FirstGeometryPassStatic != NULL)
			{
				glDeleteProgram(this->FirstGeometryPassStatic);
				this->FirstGeometryPassStatic = NULL;
				this->FGPS_MMatrixLocation = NULL;
				this->FGPS_VPMatrixLocation = NULL;
				this->FGPS_InvTransMMatrixLocation = NULL;
				this->FGPS_ColorVLocation = NULL;
				this->FGPS_ColorTLocation = NULL;
				this->FGPS_UVsColorArrayIDLocation = NULL;
				this->FGPS_NumColorsLocation = NULL;
				this->FGPS_NormalTLocation = NULL;
				this->FGPS_UVsNormalArrayIDLocation = NULL;
				this->FGPS_NumNormalsLocation = NULL;
				this->FGPS_SpecularVLocation = NULL;
				this->FGPS_SpecularTLocation = NULL;
				this->FGPS_UVsSpecularArrayIDLocation = NULL;
				this->FGPS_NumSpecularsLocation = NULL;
			}

			if (this->FirstGeometryPassAnimated != NULL)
			{
				glDeleteProgram(this->FirstGeometryPassAnimated);
				this->FirstGeometryPassAnimated = NULL;
				this->FGPA_MVPMatrixLocation = NULL;
			}

			if (this->DefferedLightingPass != NULL)
			{
				glDeleteProgram(this->DefferedLightingPass);
				this->DefferedLightingPass = NULL;
				this->DLP_TransVLocation = NULL;
				this->DLP_ScaleVLocation = NULL;
				this->DLP_CameraVLocation = NULL;
				this->DLP_FGPositionTLocation = NULL;
				this->DLP_FGColorTLocation = NULL;
				this->DLP_FGNormalTLocation = NULL;
				this->DLP_FGMaterialTLocation = NULL;
			}
				
			if (this->Postprocessing != NULL)
			{
				glDeleteProgram(this->Postprocessing);
				this->Postprocessing = NULL;
				this->P_TransVLocation = NULL;
				this->P_ScaleVLocation = NULL;
				this->P_DLFragColorLocation = NULL;
			}
		}

		void cShaders::launchFGPS()
		{
			glUseProgram(this->FirstGeometryPassStatic);
		}

		void cShaders::launchFGPA()
		{
			glUseProgram(this->FirstGeometryPassAnimated);
		}

		void cShaders::launchDLP()
		{
			glUseProgram(this->DefferedLightingPass);
		}

		void cShaders::launchP()
		{
			glUseProgram(this->Postprocessing);
		}

		GLuint cShaders::getFGPS_ID()
		{
			return this->FirstGeometryPassStatic;
		}

		GLuint cShaders::getFGPA_ID()
		{
			return this->FirstGeometryPassAnimated;
		}

		GLuint cShaders::getDLP_ID()
		{
			return this->DefferedLightingPass;
		}

		GLuint cShaders::getP_ID()
		{
			return this->Postprocessing;
		}

		GLuint cShaders::getFGPS_MMatrixLoc()
		{
			return this->FGPS_MMatrixLocation;
		}

		GLuint cShaders::getFGPS_VPMatrixLoc()
		{
			return this->FGPS_VPMatrixLocation;
		}
		
		GLuint cShaders::getFGPS_InvTransMMatrixLoc()
		{
			return this->FGPS_InvTransMMatrixLocation;
		}

		GLuint cShaders::getFGPS_ColorVLoc()
		{
			return this->FGPS_ColorVLocation;
		}

		GLuint cShaders::getFGPS_ColorTLoc()
		{
			return this->FGPS_ColorTLocation;
		}

		GLuint cShaders::getFGPS_UVsColorArrayIDLoc()
		{
			return this->FGPS_UVsColorArrayIDLocation;
		}

		GLuint cShaders::getFGPS_NumColorsLoc()
		{
			return this->FGPS_NumColorsLocation;
		}

		GLuint cShaders::getFGPS_NormalTLoc()
		{
			return this->FGPS_NormalTLocation;
		}

		GLuint cShaders::getFGPS_UVsNormalArrayIDLoc()
		{
			return this->FGPS_UVsNormalArrayIDLocation;
		}

		GLuint cShaders::getFGPS_NumNormalsLoc()
		{
			return this->FGPS_NumNormalsLocation;
		}

		GLuint cShaders::getFGPS_SpecularVLoc()
		{
			return this->FGPS_SpecularVLocation;
		}

		GLuint cShaders::getFGPS_SpecularTLoc()
		{
			return this->FGPS_SpecularTLocation;
		}

		GLuint cShaders::getFGPS_UVsSpecularArrayIDLoc()
		{
			return this->FGPS_UVsSpecularArrayIDLocation;
		}

		GLuint cShaders::getFGPS_NumSpecularsLoc()
		{
			return this->FGPS_NumSpecularsLocation;
		}

		GLuint cShaders::getFGPA_MVPMatrixLoc()
		{
			return this->FGPA_MVPMatrixLocation;
		}

		GLuint cShaders::getDLP_TransVLoc()
		{
			return this->DLP_TransVLocation;
		}

		GLuint cShaders::getDLP_ScaleVLoc()
		{
			return this->DLP_ScaleVLocation;
		}

		GLuint cShaders::getDLP_CameraVLoc()
		{
			return this->DLP_CameraVLocation;
		}

		GLuint cShaders::getDLP_FGPositionTLoc()
		{
			return this->DLP_FGPositionTLocation;
		}

		GLuint cShaders::getDLP_FGColorTLoc()
		{
			return this->DLP_FGColorTLocation;
		}

		GLuint cShaders::getDLP_FGNormalTLoc()
		{
			return this->DLP_FGNormalTLocation;
		}

		GLuint cShaders::getDLP_FGMaterialTLoc()
		{
			return this->DLP_FGMaterialTLocation;
		}

		GLuint cShaders::getP_TransVLoc()
		{
			return this->P_TransVLocation;
		}

		GLuint cShaders::getP_ScaleVLoc()
		{
			return this->P_ScaleVLocation;
		}

		GLuint cShaders::getP_DLFragColorLoc()
		{
			return this->P_DLFragColorLocation;
		}
	}
}