#include "ClientRectangle.h"
#include <vector>

namespace Faber
{
	namespace Graphics
	{
		namespace ClientRectangle
		{
			GLuint VAO = NULL;
			GLuint VBO = NULL;

			bool initialize()
			{
				std::vector <glm::vec2> Vertices;
				std::vector <glm::vec2> UVs;

				Vertices.push_back(glm::vec2(-1.0f, -1.0f));
				Vertices.push_back(glm::vec2(-1.0f, 1.0f));
				Vertices.push_back(glm::vec2(1.0f, -1.0f));
				Vertices.push_back(glm::vec2(1.0f, 1.0f));

				UVs.push_back(glm::vec2(0.0f, 0.0f));
				UVs.push_back(glm::vec2(0.0f, 1.0f));
				UVs.push_back(glm::vec2(1.0f, 0.0f));
				UVs.push_back(glm::vec2(1.0f, 1.0f));

				glGenVertexArrays(1, &VAO);
				if (VAO == NULL)
				{
					return false;
				}
				glBindVertexArray(VAO);

				glGenBuffers(1, &VBO);
				if (VBO == NULL)
				{
					return false;
				}
				glBindBuffer(GL_ARRAY_BUFFER, VBO);

				glBufferData(GL_ARRAY_BUFFER, Vertices.size() * sizeof(glm::vec2) + UVs.size() * sizeof(glm::vec2), (void*)0, GL_STATIC_DRAW);
				glBufferSubData(GL_ARRAY_BUFFER, 0, Vertices.size() * sizeof(glm::vec2), (void*)&Vertices[0]);
				glBufferSubData(GL_ARRAY_BUFFER, Vertices.size() * sizeof(glm::vec2), UVs.size() * sizeof(glm::vec2), (void*)&UVs[0]);

				glEnableVertexAttribArray(0);
				glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 2 * sizeof(GLfloat), (void*)0);

				glEnableVertexAttribArray(1);
				glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 2 * sizeof(GLfloat), (void*)(Vertices.size() * sizeof(glm::vec2)));

				glBindBuffer(GL_ARRAY_BUFFER, NULL);

				glBindVertexArray(NULL);

				Vertices.clear();
				UVs.clear();

				return true;
			}

			void terminate()
			{
				if (VAO != NULL)
				{
					glDeleteVertexArrays(1, &VAO);
					VAO = NULL;
				}

				if (VBO != NULL)
				{
					glDeleteBuffers(1, &VBO);
					VBO = NULL;
				}
			}

			void render()
			{
				glBindVertexArray(VAO);
				glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
				glBindVertexArray(NULL);
			}
		}
	}
}