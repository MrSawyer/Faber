#pragma once

#include "RenderDevice.h"
#include "../Config.h"

namespace Faber
{
	namespace Graphics
	{
		FaberEngine bool createRenderDevice(sContextDescription &RDContextDesc, cRenderDevice **ppRenderDevice);
		FaberEngine bool destroyRenderDevice(cRenderDevice **ppRenderDevice);
		FaberEngine void terminate();
		FaberEngine void render();
	}
}