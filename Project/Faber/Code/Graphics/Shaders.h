#pragma once

#include <Windows.h>
#include <GL/glew.h>
#include <fstream>
#include <sstream>
#include <string>
#include "../Config.h"

namespace Faber
{
	namespace Graphics
	{
		class cShaders
		{
		private:
			GLuint FirstGeometryPassStatic;
			GLuint FGPS_MMatrixLocation;
			GLuint FGPS_VPMatrixLocation;
			GLuint FGPS_InvTransMMatrixLocation;
			GLuint FGPS_ColorVLocation;
			GLuint FGPS_ColorTLocation;
			GLuint FGPS_UVsColorArrayIDLocation;
			GLuint FGPS_NumColorsLocation;
			GLuint FGPS_NormalTLocation;
			GLuint FGPS_UVsNormalArrayIDLocation;
			GLuint FGPS_NumNormalsLocation;
			GLuint FGPS_SpecularVLocation;
			GLuint FGPS_SpecularTLocation;
			GLuint FGPS_UVsSpecularArrayIDLocation;
			GLuint FGPS_NumSpecularsLocation;

			GLuint FirstGeometryPassAnimated;
			GLuint FGPA_MVPMatrixLocation;

			GLuint DefferedLightingPass;
			GLuint DLP_TransVLocation;
			GLuint DLP_ScaleVLocation;
			GLuint DLP_CameraVLocation;
			GLuint DLP_FGPositionTLocation;
			GLuint DLP_FGColorTLocation;
			GLuint DLP_FGNormalTLocation;
			GLuint DLP_FGMaterialTLocation;

			GLuint Postprocessing;
			GLuint P_TransVLocation;
			GLuint P_ScaleVLocation;
			GLuint P_DLFragColorLocation;

		public:
			FaberEngine cShaders();
			FaberEngine ~cShaders();

			FaberEngine bool initialize();
			FaberEngine void terminate();

			FaberEngine void launchFGPS();
			FaberEngine void launchFGPA();
			FaberEngine void launchDLP();
			FaberEngine void launchP();

			FaberEngine GLuint getFGPS_ID();
			FaberEngine GLuint getFGPA_ID();
			FaberEngine GLuint getDLP_ID();
			FaberEngine GLuint getP_ID();

			FaberEngine GLuint getFGPS_MMatrixLoc();
			FaberEngine GLuint getFGPS_VPMatrixLoc();
			FaberEngine GLuint getFGPS_InvTransMMatrixLoc();
			FaberEngine GLuint getFGPS_ColorVLoc();
			FaberEngine GLuint getFGPS_ColorTLoc();
			FaberEngine GLuint getFGPS_UVsColorArrayIDLoc();
			FaberEngine GLuint getFGPS_NumColorsLoc();
			FaberEngine GLuint getFGPS_NormalTLoc();
			FaberEngine GLuint getFGPS_UVsNormalArrayIDLoc();
			FaberEngine GLuint getFGPS_NumNormalsLoc();
			FaberEngine GLuint getFGPS_SpecularVLoc();
			FaberEngine GLuint getFGPS_SpecularTLoc();
			FaberEngine GLuint getFGPS_UVsSpecularArrayIDLoc();
			FaberEngine GLuint getFGPS_NumSpecularsLoc();

			FaberEngine GLuint getFGPA_MVPMatrixLoc();

			FaberEngine GLuint getDLP_TransVLoc();
			FaberEngine GLuint getDLP_ScaleVLoc();
			FaberEngine GLuint getDLP_CameraVLoc();
			FaberEngine GLuint getDLP_FGPositionTLoc();
			FaberEngine GLuint getDLP_FGColorTLoc();
			FaberEngine GLuint getDLP_FGNormalTLoc();
			FaberEngine GLuint getDLP_FGMaterialTLoc();

			FaberEngine GLuint getP_TransVLoc();
			FaberEngine GLuint getP_ScaleVLoc();
			FaberEngine GLuint getP_DLFragColorLoc();
		};
	}
}