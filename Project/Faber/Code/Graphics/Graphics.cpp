#include "Graphics.h"
#include "../Window/Window.h"
#include "../Window/Instance.h"
#include "../Window/Procedure.h"
#include <set>

namespace Faber
{
	namespace Graphics
	{
		std::set <cRenderDevice*> RenderDevices;

		bool createRenderDevice(sContextDescription &RDContextDesc, cRenderDevice **ppRenderDevice)
		{
			if (ppRenderDevice == NULL)
			{
				return false;
			}

			*ppRenderDevice = new cRenderDevice();
			if (*ppRenderDevice == NULL)
			{
				return false;
			}

			if (RDContextDesc.TargetWindow == NULL)
			{
				if (Window::getNativeHandle() == NULL)
				{
					return false;
				}

				RDContextDesc.TargetWindow = Window::getNativeHandle();

				if (RDContextDesc.ResolutionX == 0 || RDContextDesc.ResolutionY == 0)
				{
					RDContextDesc.ResolutionX = Window::getSizeX();
					RDContextDesc.ResolutionY = Window::getSizeY();
				}
			}
			else if (RDContextDesc.ResolutionX == 0 || RDContextDesc.ResolutionY == 0)
			{
				MessageBox(Window::getNativeHandle(), "Framebuffer resolution cannot be zero!", "Error", MB_ICONERROR | MB_OK);
				return false;
			}

			if ((cRenderDevice*)(*ppRenderDevice)->initializeOpenGL(RDContextDesc) == false)
			{
				return false;
			}
			if ((cRenderDevice*)(*ppRenderDevice)->initialize() == false)
			{
				return false;
			}

			std::pair <std::set <cRenderDevice*>::iterator, bool> Result = RenderDevices.emplace(*ppRenderDevice);
			if (Result.second == false)
			{
				return false;
			}

			if (RDContextDesc.TargetWindow == Window::getNativeHandle())
			{
				if (!Window::attachRenderDevice(RDContextDesc.TargetWindow, (void**)ppRenderDevice))
				{
					return false;
				}
			}

			return true;
		}

		bool destroyRenderDevice(cRenderDevice **ppRenderDevice)
		{
			if (ppRenderDevice == NULL)
			{
				return false;
			}
			if (*ppRenderDevice == NULL)
			{
				return false;
			}

			std::set <cRenderDevice*>::iterator it = RenderDevices.find(*ppRenderDevice);
			if (it == RenderDevices.end())
			{
				return false;
			}
			Window::detachRenderDevice(it._Ptr->_Myval->getRDContextDescription().TargetWindow);
			it._Ptr->_Myval->terminate();
			it._Ptr->_Myval->terminateOpenGL();
			delete it._Ptr->_Myval;

			RenderDevices.erase(it);

			return true;
		}

		void terminate()
		{
			for (std::set <cRenderDevice*>::iterator it = RenderDevices.begin(); it != RenderDevices.end(); ++it)
			{
				it._Ptr->_Myval->terminate();
				it._Ptr->_Myval->terminateOpenGL();
				delete it._Ptr->_Myval;
			}
			RenderDevices.clear();
		}

		void render()
		{
			for (std::set <cRenderDevice*>::iterator it = RenderDevices.begin(); it != RenderDevices.end(); ++it)
			{
				it._Ptr->_Myval->render();
			}
		}
	}
}