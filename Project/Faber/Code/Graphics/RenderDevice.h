#pragma once

#include <Windows.h>
#include <GL/glew.h>
#include <GL/wglew.h>
#include <vector>
#include <set>
#include <map>

#include <GLM/glm.hpp>
#include <GLM/gtc/matrix_transform.hpp>
#include <GLM/gtc/type_ptr.hpp>

#include "Shaders.h"
#include "Framebuffers.h"

#include "../Content/DependentContent.h"
#include "../Content/Scene/Viewport.h"

#include "../Config.h"

namespace Faber
{
	namespace Graphics
	{
		struct sContextDescription
		{
			HWND			TargetWindow = NULL;

			unsigned int	ResolutionX = 0;
			unsigned int	ResolutionY = 0;

			unsigned int	ColorBits = 24;
			unsigned int	AlphaBits = 8;
			unsigned int	DepthBits = 24;
			unsigned int	StencilBits = 8;
		};

		class cRenderDevice
		{
		private:
			float				WindowClientX;
			float				WindowClientY;

			sContextDescription RDContextDesc;
			HDC					DeviceContextHandle;
			HGLRC				RenderContextHandle;

			Content::Dependent::cDependentContent *DependentContent;

			cShaders			*Shaders;
			cFramebuffers		*Framebuffers;

			glm::mat4			PMatrix;
			glm::mat4			VMatrix;

			std::set <Content::cViewport*> Viewports;

			FaberEngine void updateViewportsParameters();

		public:
			FaberEngine cRenderDevice();
			FaberEngine ~cRenderDevice();

			FaberEngine bool makeContextCurrent();
			FaberEngine Content::Dependent::cDependentContent *getDependentContent();

			FaberEngine void				setWindowClientSize(float SizeX, float SizeY);
			FaberEngine sContextDescription getRDContextDescription();

			FaberEngine bool addViewport(Content::cViewport *Viewport);
			FaberEngine void removeViewport(Content::cViewport *Viewport);

			FaberEngine bool initializeOpenGL(sContextDescription &RDContextDesc);
			FaberEngine void terminateOpenGL();
			FaberEngine bool initialize();
			FaberEngine void terminate();
			FaberEngine void render();
		};
	}
}