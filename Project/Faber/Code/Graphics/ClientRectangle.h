#pragma once

#include <Windows.h>
#include <GL/glew.h>
#include <GLM/glm.hpp>
#include "../Config.h"

namespace Faber
{
	namespace Graphics
	{
		namespace ClientRectangle
		{
			FaberEngine bool initialize();
			FaberEngine void terminate();
			FaberEngine void render();
		}
	}
}