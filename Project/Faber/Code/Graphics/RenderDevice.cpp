#include "RenderDevice.h"
#include "ClientRectangle.h"
#include "../Window/Window.h"
#include "../Window/Instance.h"
#include "../Window/Procedure.h"

namespace Faber
{
	namespace Graphics
	{
		cRenderDevice::cRenderDevice()
		{
			this->DeviceContextHandle = NULL;
			this->RenderContextHandle = NULL;
			this->DependentContent = NULL;
			this->Shaders = NULL;
			this->Framebuffers = NULL;
		}

		cRenderDevice::~cRenderDevice() {}

		void cRenderDevice::updateViewportsParameters()
		{
			for (std::set <Content::cViewport*>::iterator it = this->Viewports.begin(); it != this->Viewports.end(); ++it)
			{
				it._Ptr->_Myval->setViewport
				(
					it._Ptr->_Myval->getPosition().x * (float)this->RDContextDesc.ResolutionX,
					it._Ptr->_Myval->getPosition().y * (float)this->RDContextDesc.ResolutionY,
					it._Ptr->_Myval->getScale().x * (float)this->RDContextDesc.ResolutionX,
					it._Ptr->_Myval->getScale().y * (float)this->RDContextDesc.ResolutionY
				);

				it._Ptr->_Myval->setAspect
				(
					(this->WindowClientX * it._Ptr->_Myval->getScale().x) /
					(this->WindowClientY * it._Ptr->_Myval->getScale().y)
				);
			}
		}

		bool cRenderDevice::makeContextCurrent()
		{
			return wglMakeCurrent(this->DeviceContextHandle, this->RenderContextHandle);
		}

		Content::Dependent::cDependentContent *cRenderDevice::getDependentContent()
		{
			return this->DependentContent;
		}

		void cRenderDevice::setWindowClientSize(float SizeX, float SizeY)
		{
			this->WindowClientX = SizeX;
			this->WindowClientY = SizeY;

			this->updateViewportsParameters();
		}

		sContextDescription cRenderDevice::getRDContextDescription()
		{
			return this->RDContextDesc;
		}

		bool cRenderDevice::addViewport(Content::cViewport *Viewport)
		{
			if (Viewport == NULL)
			{
				return false;
			}

			std::pair <std::set <Content::cViewport*>::iterator, bool> Result = this->Viewports.emplace(Viewport);

			if (Result.second == false)
			{
				return false;
			}

			if (Result.first == this->Viewports.end())
			{
				return false;
			}

			this->updateViewportsParameters();

			return true;
		}

		void cRenderDevice::removeViewport(Content::cViewport *Viewport)
		{
			if (Viewport == NULL)
			{
				return;
			}

			std::set <Content::cViewport*>::iterator it = this->Viewports.find(Viewport);

			if (it == this->Viewports.end())
			{
				return;
			}

			this->Viewports.erase(it);

			this->updateViewportsParameters();
		}

		bool cRenderDevice::initializeOpenGL(sContextDescription &RDContextDesc)
		{
			PIXELFORMATDESCRIPTOR PixelFormatDescriptor;
			ZeroMemory(&PixelFormatDescriptor, sizeof(PIXELFORMATDESCRIPTOR));
			PixelFormatDescriptor.nSize = sizeof(PIXELFORMATDESCRIPTOR);
			PixelFormatDescriptor.nVersion = 1;
			PixelFormatDescriptor.dwFlags = PFD_SUPPORT_OPENGL | PFD_DRAW_TO_WINDOW | PFD_DOUBLEBUFFER | PFD_SWAP_EXCHANGE;
			PixelFormatDescriptor.cColorBits = RDContextDesc.ColorBits;
			PixelFormatDescriptor.cAlphaBits = RDContextDesc.AlphaBits;
			PixelFormatDescriptor.cDepthBits = RDContextDesc.DepthBits;
			PixelFormatDescriptor.cStencilBits = RDContextDesc.StencilBits;
			PixelFormatDescriptor.iLayerType = PFD_MAIN_PLANE;
			PixelFormatDescriptor.iPixelType = PFD_TYPE_RGBA;

			if (Window::getNativeHandle() == NULL)
			{
				WNDCLASSEX WindowClass;
				ZeroMemory(&WindowClass, sizeof(WNDCLASSEX));
				WindowClass.cbSize = sizeof(WNDCLASSEX);
				WindowClass.cbClsExtra = 0;
				WindowClass.cbWndExtra = 0;
				WindowClass.hInstance = Window::getLibraryInstanceHandle();
				WindowClass.hCursor = LoadCursor(NULL, IDC_ARROW);
				WindowClass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);
				WindowClass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
				WindowClass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
				WindowClass.lpfnWndProc = Window::windowProc;
				WindowClass.lpszMenuName = NULL;
				WindowClass.lpszClassName = "FABERWNDCLASS";
				WindowClass.style = CS_OWNDC | CS_VREDRAW | CS_HREDRAW;

				if (RegisterClassEx(&WindowClass) == FALSE)
				{
					MessageBox(NULL, "Registering Faber window class failed!", "Error", MB_ICONERROR | MB_OK);
					return false;
				}
			}

			HWND tmpWindowHandle = CreateWindow("FABERWNDCLASS", "", WS_POPUP, 0, 0, 300, 300, NULL, NULL, Window::getLibraryInstanceHandle(), NULL);
			if (tmpWindowHandle == NULL)
			{
				MessageBox(Window::getNativeHandle(), "Creating window failed!", "Error", MB_ICONERROR | MB_OK);
				return false;
			}

			this->DeviceContextHandle = GetDC(tmpWindowHandle);
			if (this->DeviceContextHandle == NULL)
			{
				MessageBox(Window::getNativeHandle(), "Getting device context handle failed!", "Error", MB_ICONERROR | MB_OK);
				return false;
			}

			int PixelFormat = ChoosePixelFormat(this->DeviceContextHandle, &PixelFormatDescriptor);
			if (!SetPixelFormat(this->DeviceContextHandle, PixelFormat, &PixelFormatDescriptor))
			{
				MessageBox(Window::getNativeHandle(), "Setting pixel format failed!", "Error", MB_ICONERROR | MB_OK);
				return false;
			}

			this->RenderContextHandle = wglCreateContext(this->DeviceContextHandle);
			if (this->RenderContextHandle == NULL)
			{
				MessageBox(Window::getNativeHandle(), "Creating render context handle failed!", "Error", MB_ICONERROR | MB_OK);
				return false;
			}

			if (!wglMakeCurrent(this->DeviceContextHandle, this->RenderContextHandle))
			{
				MessageBox(Window::getNativeHandle(), "Making context current failed!", "Error", MB_ICONERROR | MB_OK);
				return false;
			}

			glewExperimental = GL_TRUE;
			if (glewInit() != GLEW_OK)
			{
				MessageBox(Window::getNativeHandle(), "Initializing GLEW library failed!", "Error", MB_ICONERROR | MB_OK);
				return false;
			}

			wglMakeCurrent(NULL, NULL);
			this->RDContextDesc = RDContextDesc;
			this->WindowClientX = (float)Window::getSizeX();
			this->WindowClientY = (float)Window::getSizeY();

			wglDeleteContext(this->RenderContextHandle);
			this->RenderContextHandle = NULL;

			DeleteDC(this->DeviceContextHandle);
			this->DeviceContextHandle = NULL;

			DestroyWindow(tmpWindowHandle);
			if (Window::getNativeHandle() == NULL)
			{
				if (UnregisterClass("FABERWNDCLASS", Window::getLibraryInstanceHandle()) == FALSE)
				{
					MessageBox(NULL, "Unregistering Faber window class failed!", "Error", MB_ICONERROR | MB_OK);
					return false;
				}
			}

			MSG tmpMsg;
			while (PeekMessage(&tmpMsg, NULL, 0, 0, PM_REMOVE))
			{
				if (tmpMsg.message == WM_QUIT)
					break;

				TranslateMessage(&tmpMsg);
				DispatchMessage(&tmpMsg);
			}

			ZeroMemory(&PixelFormatDescriptor, sizeof(PIXELFORMATDESCRIPTOR));

			this->DeviceContextHandle = GetDC(this->RDContextDesc.TargetWindow);
			if (this->DeviceContextHandle == NULL)
			{
				MessageBox(Window::getNativeHandle(), "Getting device context handle failed!", "Error", MB_ICONERROR | MB_OK);
				return false;
			}

			PixelFormat = NULL;

			int PixelFormatAttributes[] =
			{
				WGL_SUPPORT_OPENGL_ARB, GL_TRUE,
				WGL_DRAW_TO_WINDOW_ARB, GL_TRUE,
				WGL_DOUBLE_BUFFER_ARB, GL_TRUE,
				WGL_SWAP_METHOD_ARB, WGL_SWAP_EXCHANGE_ARB,
				WGL_COLOR_BITS_ARB, (int)this->RDContextDesc.ColorBits,
				WGL_ALPHA_BITS_ARB, (int)this->RDContextDesc.AlphaBits,
				WGL_DEPTH_BITS_ARB, (int)this->RDContextDesc.DepthBits,
				WGL_STENCIL_BITS_ARB, (int)this->RDContextDesc.StencilBits,
				WGL_PIXEL_TYPE_ARB, WGL_TYPE_RGBA_ARB,
				0
			};

			int RenderContextAttributes[] =
			{
				WGL_CONTEXT_MAJOR_VERSION_ARB, 3,
				WGL_CONTEXT_MINOR_VERSION_ARB, 3,
				WGL_CONTEXT_PROFILE_MASK_ARB, WGL_CONTEXT_CORE_PROFILE_BIT_ARB,
				0
			};

			int NumFormats;
			wglChoosePixelFormatARB(this->DeviceContextHandle, PixelFormatAttributes, NULL, 1, &PixelFormat, (UINT*)&NumFormats);

			if (!SetPixelFormat(this->DeviceContextHandle, PixelFormat, &PixelFormatDescriptor))
			{
				MessageBox(Window::getNativeHandle(), "Setting pixel format failed!", "Error", MB_ICONERROR | MB_OK);
				return false;
			}

			this->RenderContextHandle = wglCreateContextAttribsARB(this->DeviceContextHandle, NULL, RenderContextAttributes);
			if (this->RenderContextHandle == NULL)
			{
				MessageBox(Window::getNativeHandle(), "OpenGL 3.3v is not supported!", "Error", MB_ICONERROR | MB_OK);
				return false;
			}

			if (!wglMakeCurrent(this->DeviceContextHandle, this->RenderContextHandle))
			{
				MessageBox(Window::getNativeHandle(), "Making context current failed!", "Error", MB_ICONERROR | MB_OK);
				return false;
			}

			RECT ClientSize;
			GetClientRect(Window::getNativeHandle(), &ClientSize);

			int ClientWidth = 2 * (int)Window::getSizeX() - (int)(ClientSize.right - ClientSize.left);
			int ClientHeight = 2 * (int)Window::getSizeY() - (int)(ClientSize.bottom - ClientSize.top);

			SetWindowPos(Window::getNativeHandle(), NULL, NULL, NULL, ClientWidth, ClientHeight, SWP_NOMOVE | SWP_NOZORDER);

			return true;
		}

		void cRenderDevice::terminateOpenGL()
		{
			wglMakeCurrent(NULL, NULL);

			if (this->RenderContextHandle != NULL)
			{
				wglDeleteContext(this->RenderContextHandle);
				this->RenderContextHandle = NULL;
			}

			if (this->DeviceContextHandle != NULL)
			{
				DeleteDC(this->DeviceContextHandle);
				this->DeviceContextHandle = NULL;
			}
		}

		bool cRenderDevice::initialize()
		{
			if (!ClientRectangle::initialize())
			{
				return false;
			}

			this->DependentContent = new Content::Dependent::cDependentContent();
			if (this->DependentContent == NULL)
			{
				return false;
			}

			this->Shaders = new cShaders();
			if (this->Shaders == NULL)
			{
				return false;
			}
			if (!this->Shaders->initialize())
			{
				return false;
			}

			this->Framebuffers = new cFramebuffers();
			if (this->Framebuffers == NULL)
			{
				return false;
			}
			if (!this->Framebuffers->initialize(this->RDContextDesc.ResolutionX, this->RDContextDesc.ResolutionY))
			{
				return false;
			}

			glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
			glClearDepth(1.0f);
			glClearStencil(0);

			return true;
		}

		void cRenderDevice::terminate()
		{
			ClientRectangle::terminate();

			this->Viewports.clear();

			if (this->DependentContent != NULL)
			{
				this->DependentContent->terminate();
				delete this->DependentContent;
				this->DependentContent = NULL;
			}

			if (this->Shaders != NULL)
			{
				this->Shaders->terminate();
				delete this->Shaders;
				this->Shaders = NULL;
			}

			if (this->Framebuffers != NULL)
			{
				this->Framebuffers->terminate();
				delete this->Framebuffers;
				this->Framebuffers = NULL;
			}
		}

		void cRenderDevice::render()
		{
			if (!wglMakeCurrent(this->DeviceContextHandle, this->RenderContextHandle))
			{
				return;
			}

			for (std::set <Content::cViewport*>::iterator it = this->Viewports.begin(); it != this->Viewports.end(); ++it)
			{
				/*	|=================== Obliczenie VP Matrix, dla aktywnego viewportu, perspectywy i kamery ===================|	*/
				glViewport
				(
					(int)it._Ptr->_Myval->getViewportX(),
					(int)it._Ptr->_Myval->getViewportY(),
					(int)it._Ptr->_Myval->getViewportZ(),
					(int)it._Ptr->_Myval->getViewportW()
				);
				this->PMatrix = glm::perspective(45.0f, it._Ptr->_Myval->getAspect(), 0.1f, 1000.0f);
				this->VMatrix = glm::lookAt
				(
					it._Ptr->_Myval->getActiveCamera()->getPosition(),
					it._Ptr->_Myval->getActiveCamera()->getPosition() + it._Ptr->_Myval->getActiveCamera()->getFront(),
					it._Ptr->_Myval->getActiveCamera()->getUp()
				);

				glm::mat4 VPMatrix = this->PMatrix * this->VMatrix;

				/*	|=================== Zebranie static obiektow do wyrenderowania (frustum culling, occlusion culling) ===================|	*/
				// [...]

				Framebuffers->launchFGP();
				glEnable(GL_DEPTH_TEST);
				glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

				/*	|=================== Renderowanie nieprzezroczystych static obiektow sceny ===================|	*/
				Shaders->launchFGPS();
				glUniformMatrix4fv(Shaders->getFGPS_VPMatrixLoc(), 1, GL_FALSE, glm::value_ptr(VPMatrix));
				it._Ptr->_Myval->getViewedScene()->renderStaticObjects(this->Shaders);

				/*	|=================== Renderowanie swiatla na nieprzezroczystych obiektach sceny ===================|	*/
				Framebuffers->launchDLP();
				glDisable(GL_DEPTH_TEST);
				glClear(GL_COLOR_BUFFER_BIT);

				Shaders->launchDLP();

				glUniform2f
				(
					Shaders->getDLP_TransVLoc(),
					it._Ptr->_Myval->getPosition().x,
					it._Ptr->_Myval->getPosition().y
				);

				glUniform2f
				(
					Shaders->getDLP_ScaleVLoc(),
					it._Ptr->_Myval->getScale().x,
					it._Ptr->_Myval->getScale().y
				);

				glActiveTexture(GL_TEXTURE0);
				glBindTexture(GL_TEXTURE_2D, Framebuffers->getFGP_FGPositionTID());
				glUniform1i(Shaders->getDLP_FGPositionTLoc(), 0);

				glActiveTexture(GL_TEXTURE1);
				glBindTexture(GL_TEXTURE_2D, Framebuffers->getFGP_FGColorTID());
				glUniform1i(Shaders->getDLP_FGColorTLoc(), 1);

				glActiveTexture(GL_TEXTURE2);
				glBindTexture(GL_TEXTURE_2D, Framebuffers->getFGP_FGNormalTID());
				glUniform1i(Shaders->getDLP_FGNormalTLoc(), 2);

				glActiveTexture(GL_TEXTURE3);
				glBindTexture(GL_TEXTURE_2D, Framebuffers->getFGP_FGMaterialTID());
				glUniform1i(Shaders->getDLP_FGMaterialTLoc(), 3);

				glUniform3f
				(
					Shaders->getDLP_CameraVLoc(),
					it._Ptr->_Myval->getActiveCamera()->getPosition().x,
					it._Ptr->_Myval->getActiveCamera()->getPosition().y,
					it._Ptr->_Myval->getActiveCamera()->getPosition().z
				);
				
				ClientRectangle::render();

				/*	|=================== Postprocessing ===================|	*/
				glViewport // ustawienie viewportu
				(
					(int)(it._Ptr->_Myval->getPosition().x * this->WindowClientX),
					(int)(it._Ptr->_Myval->getPosition().y * this->WindowClientY),
					(int)(it._Ptr->_Myval->getScale().x * this->WindowClientX),
					(int)(it._Ptr->_Myval->getScale().y * this->WindowClientY)
				);

				Framebuffers->launchP();
				//glClear(GL_COLOR_BUFFER_BIT);

				Shaders->launchP(); // odpalenie shadera postprocessingu

				glUniform2f
				(
					Shaders->getP_TransVLoc(),
					it._Ptr->_Myval->getPosition().x,
					it._Ptr->_Myval->getPosition().y
				);

				glUniform2f
				(
					Shaders->getP_ScaleVLoc(),
					it._Ptr->_Myval->getScale().x,
					it._Ptr->_Myval->getScale().y
				);

				glActiveTexture(GL_TEXTURE0);
				glBindTexture(GL_TEXTURE_2D, Framebuffers->getDLP_DLFragColorID()); // Pobranie tekstury z DLP - Deffered Lighting Pass
				glUniform1i(Shaders->getP_DLFragColorLoc(), 0);

				ClientRectangle::render(); // Render prostokata bez MVP Matrix, na caly VIEWPORT
			}

			/*	|=================== Postprocessing ===================|	*/
			/*glViewport(0, 0, (int)this->WindowClientX, (int)this->WindowClientY);
			Framebuffers->launchP();
			glClear(GL_COLOR_BUFFER_BIT);

			Shaders->launchP();
			glActiveTexture(GL_TEXTURE0);
			glBindTexture(GL_TEXTURE_2D, Framebuffers->getDLP_DLFragColorID());
			glUniform1i(Shaders->getP_DLFragColorLoc(), 0);

			ClientRectangle::render();*/

			SwapBuffers(this->DeviceContextHandle);

			glClear(GL_COLOR_BUFFER_BIT);
		}
	}
}