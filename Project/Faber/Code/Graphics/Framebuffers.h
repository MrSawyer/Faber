#pragma once

#include <Windows.h>
#include <GL/glew.h>
#include "../Config.h"

namespace Faber
{
	namespace Graphics
	{
		class cFramebuffers
		{
		private:
			GLuint FirstGeometryPassFBO;
			GLuint FirstGeometryPassRBO;
			GLuint FGPositionT;
			GLuint FGColorT;
			GLuint FGNormalT;
			GLuint FGMaterialT;

			GLuint DefferedLightingPassFBO;
			GLuint DLFragColor;

		public:
			FaberEngine cFramebuffers();
			FaberEngine ~cFramebuffers();

			FaberEngine bool initialize(unsigned int ResolutionX, unsigned int ResolutionY);
			FaberEngine void terminate();

			FaberEngine void launchFGP();
			FaberEngine void launchDLP();
			FaberEngine void launchP();

			FaberEngine GLuint getFGP_FBOID();
			FaberEngine GLuint getFGP_RBOID();
			FaberEngine GLuint getDLP_FBOID();

			FaberEngine GLuint getFGP_FGPositionTID();
			FaberEngine GLuint getFGP_FGColorTID();
			FaberEngine GLuint getFGP_FGNormalTID();
			FaberEngine GLuint getFGP_FGMaterialTID();
			FaberEngine GLuint getDLP_DLFragColorID();
		};
	}
}