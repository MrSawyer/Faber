#include "Framebuffers.h"

namespace Faber
{
	namespace Graphics
	{
		cFramebuffers::cFramebuffers()
		{
			this->FirstGeometryPassFBO = NULL;
			this->FirstGeometryPassRBO = NULL;
			this->FGPositionT = NULL;
			this->FGColorT = NULL;
			this->FGNormalT = NULL;
			this->FGMaterialT = NULL;

			this->DefferedLightingPassFBO = NULL;
			this->DLFragColor = NULL;
		}

		cFramebuffers::~cFramebuffers() {}

		bool cFramebuffers::initialize(unsigned int ResolutionX, unsigned int ResolutionY)
		{
			glGenFramebuffers(1, &this->FirstGeometryPassFBO);
			glBindFramebuffer(GL_FRAMEBUFFER, this->FirstGeometryPassFBO);

			glGenTextures(1, &this->FGPositionT);
			glBindTexture(GL_TEXTURE_2D, this->FGPositionT);
			glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB32F, ResolutionX, ResolutionY, 0, GL_RGB, GL_FLOAT, NULL);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
			glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, this->FGPositionT, 0);

			glGenTextures(1, &this->FGColorT);
			glBindTexture(GL_TEXTURE_2D, this->FGColorT);
			glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, ResolutionX, ResolutionY, 0, GL_RGB, GL_UNSIGNED_BYTE, NULL);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
			glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT1, GL_TEXTURE_2D, this->FGColorT, 0);

			glGenTextures(1, &this->FGNormalT);
			glBindTexture(GL_TEXTURE_2D, this->FGNormalT);
			glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB32F, ResolutionX, ResolutionY, 0, GL_RGB, GL_FLOAT, NULL);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
			glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT2, GL_TEXTURE_2D, this->FGNormalT, 0);

			glGenTextures(1, &this->FGMaterialT);
			glBindTexture(GL_TEXTURE_2D, this->FGMaterialT);
			glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB32F, ResolutionX, ResolutionY, 0, GL_RGB, GL_FLOAT, NULL);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
			glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT3, GL_TEXTURE_2D, this->FGMaterialT, 0);

			GLuint FGColorAttachments[4] = { GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1, GL_COLOR_ATTACHMENT2, GL_COLOR_ATTACHMENT3 };
			glDrawBuffers(4, FGColorAttachments);

			glGenRenderbuffers(1, &this->FirstGeometryPassRBO);
			glBindRenderbuffer(GL_RENDERBUFFER, this->FirstGeometryPassRBO);
			glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT, ResolutionX, ResolutionY);
			glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, this->FirstGeometryPassRBO);

			if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
			{
				MessageBox(NULL, "First Geometry Pass FBO initializing failed!", "Error", MB_ICONERROR | MB_OK);
			}

			glBindFramebuffer(GL_FRAMEBUFFER, NULL);

			glGenFramebuffers(1, &this->DefferedLightingPassFBO);
			glBindFramebuffer(GL_FRAMEBUFFER, this->DefferedLightingPassFBO);

			glGenTextures(1, &DLFragColor);
			glBindTexture(GL_TEXTURE_2D, DLFragColor);
			glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F, ResolutionX, ResolutionY, 0, GL_RGB, GL_FLOAT, NULL);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
			glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, this->DLFragColor, 0);

			GLuint DLColorAttachments[1] = { GL_COLOR_ATTACHMENT0 };
			glDrawBuffers(1, DLColorAttachments);

			if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
			{
				MessageBox(NULL, "Deffered Lighting Pass FBO initializing failed!", "Error", MB_ICONERROR | MB_OK);
			}

			glBindFramebuffer(GL_FRAMEBUFFER, NULL);

			return true;
		}

		void cFramebuffers::terminate()
		{
			if (this->FirstGeometryPassFBO != NULL)
			{
				glDeleteFramebuffers(1, &this->FirstGeometryPassFBO);
				this->FirstGeometryPassFBO = NULL;
			}
			if (this->FirstGeometryPassRBO != NULL)
			{
				glDeleteRenderbuffers(1, &this->FirstGeometryPassRBO);
				this->FirstGeometryPassRBO = NULL;
			}
			if (this->FGPositionT != NULL)
			{
				glDeleteTextures(1, &this->FGPositionT);
				this->FGPositionT = NULL;
			}
			if (this->FGColorT != NULL)
			{
				glDeleteTextures(1, &this->FGColorT);
				this->FGColorT = NULL;
			}
			if (this->FGNormalT != NULL)
			{
				glDeleteTextures(1, &this->FGNormalT);
				this->FGNormalT = NULL;
			}
			if (this->FGMaterialT != NULL)
			{
				glDeleteTextures(1, &this->FGMaterialT);
				this->FGMaterialT = NULL;
			}

			if (this->DefferedLightingPassFBO != NULL)
			{
				glDeleteFramebuffers(1, &this->DefferedLightingPassFBO);
				this->DefferedLightingPassFBO = NULL;
			}
			if (this->DLFragColor != NULL)
			{
				glDeleteTextures(1, &this->DLFragColor);
				this->DLFragColor = NULL;
			}
		}

		void cFramebuffers::launchFGP()
		{
			glBindFramebuffer(GL_FRAMEBUFFER, this->FirstGeometryPassFBO);
		}

		void cFramebuffers::launchDLP()
		{
			glBindFramebuffer(GL_FRAMEBUFFER, this->DefferedLightingPassFBO);
		}

		void cFramebuffers::launchP()
		{
			glBindFramebuffer(GL_FRAMEBUFFER, NULL);
		}

		GLuint cFramebuffers::getFGP_FBOID()
		{
			return this->FirstGeometryPassFBO;
		}

		GLuint cFramebuffers::getFGP_RBOID()
		{
			return this->FirstGeometryPassRBO;
		}

		GLuint cFramebuffers::getDLP_FBOID()
		{
			return this->DefferedLightingPassFBO;
		}
			
		GLuint cFramebuffers::getFGP_FGPositionTID()
		{
			return this->FGPositionT;
		}

		GLuint cFramebuffers::getFGP_FGColorTID()
		{
			return this->FGColorT;
		}

		GLuint cFramebuffers::getFGP_FGNormalTID()
		{
			return this->FGNormalT;
		}

		GLuint cFramebuffers::getFGP_FGMaterialTID()
		{
			return this->FGMaterialT;
		}

		GLuint cFramebuffers::getDLP_DLFragColorID()
		{
			return this->DLFragColor;
		}
	}
}