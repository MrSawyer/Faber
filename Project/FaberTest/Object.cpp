#include "Object.h"
#include "Device.h"

namespace Testing
{
	cObject::cObject(Content::Predefined::ePredefinedPrimitiveTypes Primitive)
	{
		this->Mesh = nullptr;
		this->Material = nullptr;

		Content::sMaterialTexture *Diffuse = new Content::sMaterialTexture();
		Diffuse->Color = glm::vec4(1.0f);
		Content::sMaterialTexture *Specular = new Content::sMaterialTexture();
		Specular->Color = glm::vec4(1.0f);

		Content::createMaterial((void**)&this->Material);

		this->Material->addColor(Diffuse);
		this->Material->addSpecular(Specular);

		Content::Native::sMeshRawData *MeshData = nullptr;
		Content::createMeshRawData((void**)&MeshData, Primitive);

		getDevice()->getDependentContent()->createMesh((void**)&this->Mesh, MeshData, this->Material);
		this->addMesh(this->Mesh);
	}

	cObject::~cObject()
	{
		;
	}

	void cObject::setColor(glm::vec3 Color)
	{
		this->Material->getColor(0)->Color = glm::vec4(Color, 1.0f);
	}

	glm::vec3 cObject::getColor()
	{
		return this->Material->getColor(0)->Color;
	}
}