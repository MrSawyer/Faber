#include "Testing.h"
#include "../Faber/Code/ErrorHandler/ErrorHandle.h"
#include "../Faber/Code/Physics/Physics.h"

using namespace glm;
using namespace Content::Predefined;

// ============= CONTENT =============
Content::cScene			*Scene;
Content::cViewport		*View;
Content::cCamera		*Camera;

Testing::cObject *BB1;
Testing::cObject *BB2;

Testing::cObject *MyObj1;


Testing::cObject *MyObj2;
Testing::cObject *MyObj3;
Testing::cObject *MyObj4;

Faber::Physics::cWorld PhysicScene;
Faber::Physics::cPhysicBody * Body;


std::vector<Faber::Physics::BodyWatch> Bodies;
std::vector<Testing::cObject  *> Objs;


// -> WYWOLYWANA PRZED PETLA <-
bool Testing::initialize(Content::cScene *inScene, Content::cViewport *inViewport, Content::cCamera *inCamera)
{

	throwError("Errorsoin");

	/*
	throwErrorHWND("eROR", HWND);
	throwWarning("Warninger");
	exitWithError("Error and exit");
	exitWithErrorHWND("eRROR", HWND);

	ORlogicCheckWarningExit("ErroR", VAL > 2, VAL2 < 3, itp itd niesko�czona ilo�c argument�w jak w princie);
	ANDlogicCheckWarningExit("ErroR", VAL > 2, VAL2 < 3, itp itd niesko�czona ilo�c argument�w jak w princie);

	ofc jest tych funkcji du�o wiecej, i znajdziesz je w ErrorHandle.h	
	*/
	Scene = inScene;
	View = inViewport;
	Camera = inCamera;

	//PhysicScene.useSAP();

	Faber::Physics::sPhysicBodyDef NewBodyDef;

	for (int i = 0; i < 100; i++) {
		NewBodyDef.BodyType = Physics::DYNAMIC;
		NewBodyDef.Position = glm::vec3(rand()%10 + 5, rand()%10 + 5, rand()%10 + 5);
		NewBodyDef.Scale = glm::vec3(0.4, 0.4f, 0.4f);

		Faber::Physics::BodyWatch Body = PhysicScene.createBody(NewBodyDef);
		
		Testing::cObject *Obj = createObject(PREDEFINED_PRIMITIVE_SPHERE);;
		Obj->setColor(vec3(1.0f, 1.0f, 1.0f));

		Bodies.push_back(Body);
		Objs.push_back(Obj);
	}

	Camera->setPosition(10.0f, 5.0f, 20.0f);

	return true;
}

// -> WYWOLYWANA PO PETLI <-
bool Testing::terminate()
{
	return true;
}

// -> WYWOLYWANA W PETLI <-
void Testing::launch()
{
	exitWithError("Errorsoin",);

	ErrorHandler::printLogs();


}

Testing::cObject *Testing::createObject(Content::Predefined::ePredefinedPrimitiveTypes Primitive)
{
	cObject *Obj = nullptr;
	Content::createStaticObject((void**)&(Obj = new cObject(Primitive)));
	Scene->addStaticObject(Obj);
	return Obj;
}