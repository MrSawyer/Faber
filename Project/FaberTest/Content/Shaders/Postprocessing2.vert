#version 330 core

layout (location = 0) in vec2 Vertices;
layout (location = 1) in vec2 UVs;

out vec2 OutUVs;

void main()
{
	OutUVs = UVs;
	gl_Position = vec4(Vertices, 0.0, 1.0);
}