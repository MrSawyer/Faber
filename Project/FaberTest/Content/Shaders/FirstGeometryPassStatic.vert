#version 330 core

layout (location = 0) in vec3 Vertices;
layout (location = 1) in vec3 Normals;
layout (location = 2) in vec2 UVs;

out vec3 OutVertices;
out vec3 OutNormals;
out vec2 OutUVs;

uniform mat4 MMatrix;
uniform mat4 VPMatrix;
uniform mat3 InvTransMMatrix;

void main()
{
	OutVertices = vec3(MMatrix * vec4(Vertices, 1.0));
	OutNormals = InvTransMMatrix * Normals;
	OutUVs = UVs;
	
	gl_Position = VPMatrix * vec4(OutVertices, 1.0);
}