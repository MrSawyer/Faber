#version 330 core

layout (location = 0) out vec3 FGPositionT;
layout (location = 1) out vec3 FGColorT;
layout (location = 2) out vec3 FGNormalT;
layout (location = 3) out vec3 FGMaterialT;

in vec3 OutVertices;
in vec3 OutNormals;
in vec2 OutUVs;

uniform vec4		ColorV[4];
uniform sampler2D	ColorT[4];
uniform int			NumColors;

uniform sampler2D	NormalT[4];
uniform int			NumNormals;

uniform vec4		SpecularV[4];
uniform sampler2D 	SpecularT[4];
uniform int			NumSpeculars;

vec3 getNormalFromMap()
{
	vec3 tangentNormal = texture(NormalT[0], OutUVs).xyz * 2.0 - 1.0;
	
	vec3 Q1 = dFdx(OutVertices);
	vec3 Q2 = dFdy(OutVertices);
	vec2 st1 = dFdx(OutUVs);
	vec2 st2 = dFdy(OutUVs);
	
	vec3 N = normalize(OutNormals);
	vec3 T = normalize(Q1 * st2.t - Q2 * st1.t);
	vec3 B = -normalize(cross(N, T));
	
	mat3 TBN = mat3(T, B, N);
	
	return normalize(TBN * tangentNormal);
}

void main()
{
	FGPositionT = OutVertices;
	FGColorT = ColorV[0].rgb;
	FGNormalT = normalize(OutNormals);
	FGMaterialT = SpecularV[0].rgb;
}