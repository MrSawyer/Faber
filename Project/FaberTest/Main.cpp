#include <Windows.h>
#include "Testing.h"
#include "Device.h"

#include <Input\NativeInput.h>
#include <Content\PredefinedContent.h>

bool InitializeConsole();
bool ReleaseConsole();

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
{
	if (InitializeConsole() == false)
	{
		return -1;
	}

	Input::zeroParameters();

	Window::sWindowDescription WindowDesc;
	WindowDesc.Title = "Faber Testing";
	WindowDesc.PositionX = 0;
	WindowDesc.PositionY = 0;
	WindowDesc.SizeX = 1200;
	WindowDesc.SizeY = 600;

	Window::create(WindowDesc);

	Graphics::sContextDescription ContextDesc;
	ContextDesc.ColorBits = 24;
	ContextDesc.AlphaBits = 8;
	ContextDesc.DepthBits = 24;
	ContextDesc.StencilBits = 8;
	
	Graphics::cRenderDevice *Device = nullptr;
	Graphics::createRenderDevice(ContextDesc, &Device);

	Testing::setDevice(Device);

	Content::cViewport *Viewport = nullptr;
	Content::createViewport((void**)&Viewport);

	Content::cCamera *Camera = nullptr;
	Content::createCamera((void**)&Camera);
	Camera->setPosition(0.0f, 0.0f, 0.0f);
	Camera->setFront(0.0f, 0.0f, -1.0f);
	Camera->setUp(0.0f, 1.0f, 0.0f);

	Content::cScene *Scene = nullptr;
	Content::createScene((void**)&Scene);

	Viewport->setViewedScene(Scene);
	Viewport->setActiveCamera(Camera);

	Device->addViewport(Viewport);

	if (!Testing::initialize(Scene, Viewport, Camera))
	{
		SendMessage(Window::getNativeHandle(), WM_CLOSE, 0, 0);
	}

	MSG Msg;
	while (true)
	{
		if (PeekMessage(&Msg, NULL, 0, 0, PM_REMOVE))
		{
			if (Msg.message == WM_QUIT)
				break;

			TranslateMessage(&Msg);
			DispatchMessage(&Msg);
		}
		else
		{
			Testing::launch();
			Device->render();
		}
	}

	Testing::terminate();

	Content::terminate();
	Graphics::terminate();
	Window::destroy();

	if (ReleaseConsole() == false)
	{
		exitWithError("Deleting console failed!", -1);
	}
	return 0;
}

bool InitializeConsole()
{
	if (AllocConsole() == FALSE)
	{
		MessageBox(NULL, "Creating console failed!", "Error", MB_ICONERROR | MB_OK);
		return false;
	}
	if (freopen_s((FILE**)stdout, "CON", "w", stdout) == -1)
	{
		MessageBox(NULL, "Creating console failed!", "Error", MB_ICONERROR | MB_OK);
		return false;
	}
	if (freopen_s((FILE**)stdin, "CON", "r", stdin) == -1)
	{
		MessageBox(NULL, "Creating console failed!", "Error", MB_ICONERROR | MB_OK);
		return false;
	}

	return true;
}

bool ReleaseConsole()
{
	std::cout << "\n\n";
	system("pause");

	if (FreeConsole() == FALSE)
	exitWithError("Deleting console failed!", false);
	
	return true;
}