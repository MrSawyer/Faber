#pragma once

#include <Faber.h>

namespace Testing
{
	void setDevice(Faber::Graphics::cRenderDevice *Dev);
	Faber::Graphics::cRenderDevice *getDevice();
}