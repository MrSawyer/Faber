#include "Device.h"

namespace Testing
{
	Faber::Graphics::cRenderDevice *Device;

	void setDevice(Faber::Graphics::cRenderDevice *Dev)
	{
		Device = Dev;
	}

	Faber::Graphics::cRenderDevice *getDevice()
	{
		return Device;
	}
}