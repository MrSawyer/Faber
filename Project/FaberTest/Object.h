#pragma once

#include <Faber.h>

using namespace Faber;

namespace Testing
{
	class cObject : public Content::cStaticObject
	{
	private:
		Content::Native::cMesh *Mesh;
		Content::cMaterial *Material;

	public:
		cObject(Content::Predefined::ePredefinedPrimitiveTypes Primitive);
		~cObject();

		void setColor(glm::vec3 Color);
		glm::vec3 getColor();
	};
}