#pragma once

#include <Faber.h>
#include <iostream>
#include <time.h>
#include <math.h>
#include <string>
#include <vector>

#include "Object.h"

using namespace Faber;
using namespace std;

namespace Testing
{
	cObject *createObject(Content::Predefined::ePredefinedPrimitiveTypes Primitive);

	bool initialize(Content::cScene *inScene, Content::cViewport *inViewport, Content::cCamera *inCamera);
	bool terminate();
	void launch();
}
